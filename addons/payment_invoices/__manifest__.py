# -*- coding: utf-8 -*-

{
    'name': 'TNC Invoice Payment',
    'version': '12.0.1.3',
    'category': 'account',
    'sequence': 50,
    'author': 'Tarik TNC',
    'website': "bitbucket",
    
    'summary': "Payer plusieurs factures en utilisant l'ecran de paiement avec n'importe quel montant defini par l'utilisateur",
    'description': """ 
            Payer plusieurs factures
            Payer plusieurs factures fournisseurs
            Payer plusieurs notes de crédit / debit
            
            Paiement  partiel des factures,
            Paiement de la facture complete,
            
            
            paiement par lots
            Paiement de plusieurs factures fournisseur ,
            Paiement de plusieurs notes de crédit ,
            Paiement de plusieursnotes de débit multiples,
            Paiement de plusieurs factures,
            paiement multiple,

     """,

    'depends': ['account'],
    
    'data': [
           'views/account_payment_inehrit.xml',
           'security/ir.model.access.csv',
             ],
    "images":['static/description/banner.jpg'],

    'installable': True,
    'application': True,
    'auto_install': False,
    
   
    
}

