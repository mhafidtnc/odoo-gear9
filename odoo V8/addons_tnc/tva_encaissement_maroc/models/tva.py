from openerp import models, fields, api
from openerp.exceptions import ValidationError
from lxml import etree
import base64
from datetime import datetime
from xlrd import open_workbook
import xlrd
from os import path
import os

class tva_line(models.Model):
    _name = "tva.line"
    _description = "Lignes Tva"

    tva_declaration_id = fields.Many2one('tva.declaration', 'Declaration TVA', ondelete='cascade')
    tax_id = fields.Float('TVA')
    amount_ht = fields.Float('Base TVA')
    amount = fields.Float('Montant TVA')
    type = fields.Selection([
        ('r', 'Fournisseur'),
        ('p', 'Client')])

class tva_declaration_line(models.Model):
    _name = "tva.declaration.line"
    _description = "Ligne Declaration Tva"
    _order = 'sequence, id'

    tva_declaration_id = fields.Many2one('tva.declaration', 'Declaration TVA', ondelete='cascade')
    sequence = fields.Integer(string='Sequence')
    invoice_id = fields.Many2one('account.invoice', 'Facture',readonly=True)
    invoice_number = fields.Char('N Facture')
    description = fields.Char('Description')
    amount_ht = fields.Float('Montant HT')
    amount_tva = fields.Float('Montant TVA')
    amount_ttc = fields.Float('Montant TTC')
    invoice_date = fields.Date('Date facture')
    paiement_date = fields.Date('Date de paiament')
    paiement_type_char = fields.Char(string=u'Methode de paiament')
    tax_rate = fields.Float('Taux de TVA',digits=(16,2))
    invoice_tax_id = fields.Many2one('account.invoice.tax', 'Taxe facture')
    type = fields.Boolean('Encaissement?')
    partner_id = fields.Many2one('res.partner','Partenaire')
    id_fisc = fields.Char('IF')
    partner_name = fields.Char('Nom FR')
    ice = fields.Char('ICE')


class tva_declaration(models.Model):
    _name = "tva.declaration"
    _description = "Declaration Tva"
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char('Description', required=True)
    period_id = fields.Many2one('account.period',
                               string='Periode')
    company_id = fields.Many2one('res.company','Societe',default=lambda self: self.env.user.company_id)
    line_ids = fields.One2many(comodel_name='tva.declaration.line', inverse_name='tva_declaration_id',
                                  string=u'Lignes',domain=[('type','=',False)])

    payable_tva_ids = fields.One2many(comodel_name='tva.line', inverse_name='tva_declaration_id',
                               string=u'TVA fournisseur', domain=[('type', '=', 'p')])

    state = fields.Selection([
        ('draft', u'Brouillon'),
        ('done', u'Valide')], default='draft', string='Etat', readonly=True, track_visibility='onchange')

    xml_file = fields.Binary('Fichier de Tele-Declaration TVA',attachment=True)
    excel_file = fields.Binary('Fichier de TVA (EXCEL)',attachment=True)
    filename = fields.Char('file name', readonly=True, store=False, compute='get_file_name')
    regime = fields.Char('Regime de la declaration',compute="get_data_tva")
    annee = fields.Char('Annee',compute="get_data_tva")
    period = fields.Char('Periode',compute="get_data_tva")
    tax_account_id = fields.Many2one('account.account',string="Credit de TVA",
                                     default=lambda self: self.env['ir.values'].get_default('account.config.settings',
                                                                              'tax_account_id'))

    payed_tax_account_id = fields.Many2one('account.account', string="Etat TVA due",
                                    default=lambda self: self.env['ir.values'].get_default('account.config.settings',
                                                                              'payed_tax_account_id'))


    date = fields.Date('Date',required=True)

    @api.multi
    @api.depends('period_id')
    def get_data_tva(self):
        for record in self:
            if record.period_id and record.period_id.fiscalyear_id:
                if len(record.period_id.fiscalyear_id.period_ids) == 13:
                    record.regime = "1"
                else:
                    record.regime = "2"
            if record.period_id:
                split_date = record.period_id.date_start.split('-')
                record.annee = split_date[0]
                record.period = split_date[1]

    @api.one
    def get_file_name(self):
        self.filename = 'Fichier XML.xml'

    @api.multi
    def generate_xml_file(self):
        for record in self:
            root = etree.Element("DeclarationReleveDeduction")
            etree.SubElement(root,"identifiantFiscal").text = record.company_id.partner_id.id_fisc
            etree.SubElement(root,"annee").text = record.annee
            etree.SubElement(root,"periode").text = record.period
            etree.SubElement(root,"regime").text = record.regime
            releveDeductions = etree.SubElement(root,"releveDeductions")
            for line in record.line_ids:
                rd = etree.SubElement(releveDeductions,"rd")
                etree.SubElement(rd, "ord").text = str(line.sequence)
                etree.SubElement(rd, "num").text = str(line.invoice_number)
                etree.SubElement(rd, "des").text = str(line.description)
                etree.SubElement(rd, "mht").text = str(line.amount_ht)
                etree.SubElement(rd, "tva").text = str(line.amount_tva)
                etree.SubElement(rd, "ttc").text = str(line.amount_ttc)
                fr = etree.SubElement(rd, "refF")
                etree.SubElement(fr, "if").text = str(line.id_fisc)
                etree.SubElement(fr, "nom").text = str(line.partner_name)
                etree.SubElement(fr, "ice").text = str(line.ice)
                etree.SubElement(rd, "tx").text = str(round(line.tax_rate,2))
                mp = etree.SubElement(rd, "refF")
                etree.SubElement(mp, "id").text = str(line.paiement_type_char)
                etree.SubElement(rd, "dpai").text = str(line.paiement_date)
                etree.SubElement(rd, "dfac").text = str(line.invoice_date)
            tree = etree.ElementTree(root)
            file = open("tva.xml",'w')

            file.write(etree.tostring(root, pretty_print=True))
            file.close()
            xml_file = base64.encodestring(open('tva.xml','rb').read())

            record.write({'xml_file':xml_file})

    @api.multi
    def validate(self):
        for record in self:
            record.state='done'



    @api.multi
    def generate_tva_lines(self):
        tva_obj = self.env['tva.line']
        for record in self:
            data = {}
            for line in record.line_ids:
                if data.get(line.tax_rate,False):
                    data[line.tax_rate]['amount']+=line.amount_tva
                    data[line.tax_rate]['amount_ht']+=line.amount_ht
                else:
                    data[line.tax_rate] = {
                        'tva_declaration_id': record.id,
                        'tax_id': line.tax_rate,
                        'amount': line.amount_tva,
                        'amount_ht': line.amount_ht,
                        'type': 'p'
                    }
            for data_tva in data:
                tva_obj.create(data[data_tva])

    @api.multi
    def generate_data_from_odoo(self):
        for rec in self:
            rec.line_ids.unlink()
            rec.payable_tva_ids.unlink()
            invoice_ids = self.env['account.invoice'].search([('type','=','in_invoice'),('state','=','paid'),('period_id','=',rec.period_id.id)])
            seq = 1
            for invoice in invoice_ids:
                vals={}
                vals['tva_declaration_id']=rec.id
                vals['sequence'] = seq
                vals['invoice_number'] = invoice.number or ''
                vals['partner_name'] = invoice.partner_id.name or ''
                vals['id_fisc'] = invoice.partner_id.id_fisc or ''
                vals['ice'] = invoice.partner_id.ice or ''
                vals['description'] = invoice.number or ''
                vals['amount_ht'] = invoice.amount_untaxed
                vals['amount_tva'] = invoice.amount_tax
                vals['amount_ttc'] = invoice.amount_total
                vals['invoice_date'] = invoice.date_invoice
                vals['paiement_type_char'] = '4'
                if invoice.payment_ids:
                    vals['paiement_date'] = invoice.payment_ids[0].date
                taux_tva = 20
                if invoice.tax_line:
                    vals['tax_rate'] = round((100*invoice.tax_line[0].amount) / (invoice.tax_line[0].base or 1),2)

                self.env['tva.declaration.line'].create(vals)
                seq += 1
            self.generate_tva_lines()

    @api.multi
    def generate_data(self):
        for record in self:
            record.line_ids.unlink()
            record.payable_tva_ids.unlink()
            f = base64.decodestring(record.excel_file)
            book = open_workbook(file_contents=f, encoding_override="utf-8")
            sheet = book.sheet_by_index(0)
            for row_index in range(1, sheet.nrows):
                vals = {}
                vals['sequence'] = float(sheet.cell(row_index,0).value or 0.0)
                vals['invoice_number'] = str(sheet.cell(row_index,1).value or '')
                vals['partner_name'] = str(sheet.cell(row_index,2).value or '')
                vals['id_fisc'] = str(sheet.cell(row_index,3).value or '')
                vals['ice'] = str(sheet.cell(row_index,4).value or '')
                vals['description'] = str(sheet.cell(row_index,5).value or '')
                vals['amount_ht'] = float(sheet.cell(row_index,6).value or 0.0)
                vals['amount_tva'] = float(sheet.cell(row_index,7).value or 0.0)
                vals['amount_ttc'] = float(sheet.cell(row_index,8).value or 0.0)

                year, month, day,hour, minute, second  = xlrd.xldate_as_tuple(sheet.cell(row_index, 9).value,book.datemode)
                invoice_date = datetime(year, month, day)
                vals['invoice_date'] = invoice_date.strftime("%d/%m/%Y")

                year, month, day, hour, minute, second = xlrd.xldate_as_tuple(sheet.cell(row_index, 10).value,
                                                                              book.datemode)
                paiement_date = datetime(year, month, day)
                vals['paiement_date'] = paiement_date.strftime("%d/%m/%Y")

                vals['paiement_type_char'] = str(sheet.cell(row_index, 11).value or 0.0)
                vals['tax_rate'] = float(sheet.cell(row_index, 12).value or 0.0)
                vals['tva_declaration_id']=record.id
                self.env['tva.declaration.line'].create(vals)
        self.generate_tva_lines()

