# -*- encoding: utf-8 -*-

{
    'name': 'TVA encaissement maroc',
    'version': '1.0',
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    "depends": [
        'account','partner_extend'
    ],
    'data': [
        "views/tva.xml"
        ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
