# -*- encoding: utf-8 -*-

from openerp import models,fields, api
from openerp.exceptions import ValidationError

class account_invoice(models.Model):
    _inherit = 'account.invoice'

    objectif_recouvrement = fields.Char(string=u'Objectif recouvrement')
    date_depot = fields.Date(string=u'Date dépôt')
    facture_deposee = fields.Boolean(string=u'Facture déposée',default=False)


    @api.multi
    def write(self, vals):
        for rec in self:
            if vals.get('facture_deposee',"rien") != 'rien' and vals['facture_deposee']:

                if vals.get('date_depot',"rien") != 'rien':
                    new_date = vals['date_depot']
                else:
                    new_date = rec.date_depot

                if rec.payment_term:
                    pterm_list = rec.payment_term.compute(value=1,date_ref=new_date)[0]
                    if pterm_list:
                        vals['date_due'] = max(line[0] for line in pterm_list)
                else:
                    vals['date_due'] = new_date



            return super(account_invoice,self).write(vals)