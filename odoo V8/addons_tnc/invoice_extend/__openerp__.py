# -*- coding: utf-8 -*-

{
    "name": "Invoice Extend",
    "version": "1.1",
    "depends": ['account'],
    "author": "BHECO SERVICES",
    'website': 'http://bhecoservices.com',
    "category": "ACCOUNTING",
    "description": "Ajouter de nouveaux champs : Date dépôt facture, facture déposée, objectif recouvrement",
    "init_xml": [],
    'data': [
        'views/invoice_extend_view.xml',
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
