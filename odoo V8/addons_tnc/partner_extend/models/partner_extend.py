# -*- encoding: utf-8 -*-

from openerp import models,fields, api
from openerp.exceptions import ValidationError

class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    id_fisc = fields.Char(string=u'Identifiant Fiscal')
    patente = fields.Char(string=u'N° de patente')
    rc = fields.Char(string=u'RC')
    cnss = fields.Char(string=u'Numéro de la sécurité sociale')
    capital_social = fields.Char(string=u'Capital social')
    ice = fields.Char(string=u'ICE')

    @api.one
    @api.constrains('rc')
    def _check_unique_constraint(self):
            if len(self.search([('rc', '=', self.rc)])) > 1:
                    raise ValidationError(u"Le champ RC doit être unique !")