# -*- coding: utf-8 -*-

{
    "name": "Partner Extend",
    "version": "1.1",
    "depends": ['base'],
    "author": "BHECO SERVICES",
    'website': 'http://bhecoservices.com',
    "category": "BASE",
    "description": "Ajouter des infos sup",
    "init_xml": [],
    'data': [
        'views/partner_extend_view.xml',
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
