
{
    'name': 'Etats paie marocaine',
    'version': '1.0',
    'category': 'HR',
    'complexity': "normal",
    'description': """
    Etats de la paie marociane : CNSS, IR, AMO, livre de paie
""",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['hr_payroll_ma'],
    'data': [ 'report/reports_view.xml',
             'report/livre_paie.xml',
             'report/rep_cnss.xml',
             'report/rep_amo.xml',
             'report/rep_igr.xml',
             'report/bulletin_spec.xml',
    ],
    'demo': [
        '',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

