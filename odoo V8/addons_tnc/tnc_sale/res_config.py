# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (C) 2004-2012 OpenERP S.A. (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging

from openerp.osv import fields, osv
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)


class sale_configuration(osv.TransientModel):
    _inherit = 'sale.config.settings'

    _columns = {
        'team': fields.boolean('Champ Equipe commerciale Obligatoire'),
    }

                 
    def onchange_team(self, cr, uid, ids, team, context=None):
        var = True
        if not team:
            view_ids = self.pool.get('ir.ui.view').search(cr, uid, [('active','=',False),('name','=','tnc_quotation_object_crm_lead_facultatif')])
            
            view_ids = self.pool.get('ir.ui.view').browse(cr, uid, view_ids, context=context)
            if view_ids :
                view = self.pool.get('ir.ui.view').write(cr,uid, [view_ids[0].id], {'active': True})
        else :
            view_ids = self.pool.get('ir.ui.view').search(cr, uid, [('active','=',True),('name','=','tnc_quotation_object_crm_lead_facultatif')])
            
            view_ids = self.pool.get('ir.ui.view').browse(cr, uid, view_ids, context=context)
            if view_ids :
                view = self.pool.get('ir.ui.view').write(cr,uid, [view_ids[0].id], {'active': False})
            
            
            
            
 
