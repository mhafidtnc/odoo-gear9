# -*- coding: utf-8 -*-
from openerp import api, fields, models, _

class Saletnc(models.Model):
    _inherit = 'crm.lead'

    project_id = fields.Many2one('project.project', string='Project', readonly=True)
    real_revenu = fields.Float(String=u'Reel revenue', compute='_compute_revenue_reel', store=True)
    doc_count = fields.Integer(compute='_compute_attached_docs_count', string="Number of documents attached",)
    purchase_count = fields.Integer(compute='_compute_purchase_order_count', string="Number of purchase order",)

    @api.multi
    def case_add_projet(self):
        for order in self:
            vals = {
                'name': order.name,
                'partner_id': order.partner_id.id,
                'user_id': order.user_id.id,
            }
            res = self.env['project.project'].create(vals)
            order.project_id = res

    @api.depends('devis_opp.amount_untaxed')
    def _compute_revenue_reel(self):
        for lead in self:
            lead.real_revenu = lead.devis_opp.amount_untaxed

    def _compute_attached_docs_count(self):
        Attachment = self.env['ir.attachment']
        for opp in self:
            opp.doc_count = Attachment.search_count([
                ('res_model', '=', 'crm.lead'), ('res_id', '=', opp.id)
            ])

    def _compute_purchase_order_count(self):
        purchase_object = self.env['purchase.order']
        for opp in self:
            opp.purchase_count = purchase_object.search_count([('opportunity_id', '=', opp.id),('state', 'in', ['done','except_picking','except_invoice','approved'])])

    @api.multi
    def attachment_tree_view(self):
        self.ensure_one()
        domain = [('res_model', '=', 'crm.lead'), ('res_id', 'in', self.ids)]
        return {
            'name': _('Attachments'),
            'domain': domain,
            'res_model': 'ir.attachment',
            'type': 'ir.actions.act_window',
            'view_id': False,
            'view_mode': 'kanban,tree,form',
            'view_type': 'form',
            'help': _('''<p class="oe_view_nocontent_create">
                        Documents are attached to your opportunity.</p><p>
                    </p>'''),
            'limit': 80,
            'context': "{'default_res_model': '%s','default_res_id': %d}" % (self._name, self.id)
        }

    @api.multi
    def button_purchases(self):
        # partner_ids = [lead.partner_id.id for lead in self]
        for rec in self:
            order_states = ['done','except_picking','except_invoice','approved']

            orders = self.env['purchase.order'].search([
                ('opportunity_id', '=', rec.id),
                ('state', 'in', order_states),
            ])

            res = {
                'name': 'Purchase order',
                'type': 'ir.actions.act_window',
                'res_model': 'purchase.order',
                'view_type': 'form',
                'context': {'default_opportunity_id': rec.id}
            }

            if len(orders) == 1:
                res['res_id'] = orders[0].id
                res['view_mode'] = 'form'
            else:
                res['domain'] = [
                    ('state', 'in', order_states),
                    ('opportunity_id', '=', rec.id),
                ]
                res['view_mode'] = 'tree,form'

            return res
