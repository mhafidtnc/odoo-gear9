# -*- coding: utf-8 -*-

from openerp import api, fields, models, _

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    opportunity_id = fields.Many2one('crm.lead', domain="[('type','=','opportunity')]", string="Opportunité",
                                        readonly=False)
