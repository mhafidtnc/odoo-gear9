# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP S.A. <http://www.openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'TNC Sale Extension',
    'version': '1.0',
    'category': 'Sale TNC',
    'description': u"""
- Ajout bouton documents sur opportunités
- Devis - Ajout contact chez le client sur le devis
- Opportunité: - Ajout d'un champ CA sur l'opportunite
- Facture Client - renseigner automatiquement le champ opportunité depuis le bdc
- Devis - ajout du champ objet sur devis et facture
- Champs obligatoires sur opportunité
    """,
    'summary': 'TNC Sale',
    'author': 'KARIZMA CONSEIL',
    'website': 'http://www.karizma-conseil.com',
    'depends': ['sale','sale_crm','project','PAC','Devis_custom','purchase'],
    'data': [
        'views/tnc_sale_views.xml',
        'views/sale_order.xml',
        'views/tnc_report.xml',
        'views/purchase_order_views.xml',
        'views/tnc_sale_views_facultatif.xml',
        'res_config_view.xml',
    ],
    #'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
