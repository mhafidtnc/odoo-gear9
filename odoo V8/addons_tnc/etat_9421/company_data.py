# -*- coding: utf-8 -*-
from openerp import fields, models,api



class ResCompany(models.Model):
    _inherit = 'res.company'

    identifiant_fiscal = fields.Char(string="IF")
    code_commune = fields.Char(string="Commune")
    n_cni = fields.Char(string=u"N° CNI")
    n_cnss = fields.Char(string=u"N° CNSS")
    n_ce = fields.Char(string=u"N° CE")
    n_rc = fields.Char(string=u"N° RC")
    n_tp = fields.Char(string="Identifiant TP")