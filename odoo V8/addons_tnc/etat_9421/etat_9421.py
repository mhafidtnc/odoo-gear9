# -*- coding: utf-8 -*-
from openerp import fields, models,api, osv, _
import base64
import time
from datetime import datetime
import xml.etree.cElementTree as ET

class hr_employee(models.Model):
    _inherit = 'hr.employee'

    otherid = fields.Char('Num CE')

class RecapPaie(models.Model):
        _name = 'etat.9421'

        name = fields.Char(string=u"Intitulé rapport")
        annee = fields.Integer(string=u"Année fiscale")
        etat_line_ids = fields.One2many('etat.9421.line','id_etat')
        file_id = fields.Binary(string=u'Fichier')
        edi_filename = fields.Char(string=_(""), required=False, )

        # Champs calculés
        effectif = fields.Integer(compute='get_effectif',string="Effectif")
        nb_pp = fields.Integer(compute='get_effectif',string="Effectif Permanents")
        nb_po = fields.Integer(compute='get_effectif',string="Effectif Occasionnels")
        nb_stg = fields.Integer(compute='get_effectif',string="Effectif stagiaires")

        # Totaux permanents
        total_sbi_pp = fields.Float(compute='get_totaux', string="totalMtRevenuBrutImposablePP")
        total_sni_pp = fields.Float(compute='get_totaux', string="totalMtRevenuNetImposablePP")
        total_ir_pp = fields.Float(compute='get_totaux', string="totalMtIrPrelevePP")
        total_ded_pp = fields.Float(compute='get_totaux', string="totalMtTotalDeductionPP")

        # Totaux occasionnels
        total_sb_po = fields.Float( string="totalMtBrutSommesPO")
        total_ir_po = fields.Float( string="totalIrPrelevePO")

        # Totaux stagiaires
        total_brut_stg = fields.Float(string="totalMtBrutTraitSalaireSTG")
        total_ind_stg = fields.Float(string="totalMtBrutIndemnitesSTG")
        total_ret_stg = fields.Float(string="totalMtRetenuesSTG")
        total_net_stg = fields.Float(string="totalMtRevenuNetImpSTG")

        # Totaux spéciaux
        somme_rts = fields.Float(string="totalSommePayeRTS")
        tot_rev_annuel = fields.Float(string="totalmtAnuuelRevenuSalarial")
        mt_abondement = fields.Float(string="totalmtAbondement")
        mt_permanent = fields.Float(string="montantPermanent")
        mt_occasionnel = fields.Float(string="montantOccasionnel")
        mt_stagiaire = fields.Float(string="montantStagiaire")
        ref_declaration = fields.Char(string="referenceDeclaration",default='345678')



        @api.multi
        def get_totaux(self):

            tot_sbi = 0
            tot_sni = 0
            tot_ir = 0
            for rec in self:
                for line in rec.etat_line_ids:
                    tot_sbi += line.s_sbi
                    tot_sni += line.s_sni
                    tot_ir += line.s_igr

            rec.total_sbi_pp = tot_sbi
            rec.total_sni_pp = tot_sni
            rec.total_ir_pp = tot_ir


        @api.multi
        def get_effectif(self):
            for rec in self:
                tot=0
                for emp in self.env['hr.employee'].search([('active','=',True)]):
                    tot += 1
                rec.effectif = tot
                rec.nb_pp = tot
                rec.nb_po = 0
                rec.nb_stg = 0


        @api.multi
        def generate_edi_file(self):

            societe = self.env['res.company'].search([('id','=',1)])
            for rec in self:
                niv_1 = ET.Element("TraitementEtSalaire")
                ET.SubElement(niv_1, "identifiantFiscal").text = societe.identifiant_fiscal
                ET.SubElement(niv_1, "nom").text = ""
                ET.SubElement(niv_1, "prenom").text = ""
                ET.SubElement(niv_1, "raisonSociale").text = societe.name
                ET.SubElement(niv_1, "exerciceFiscalDu").text = str(rec.annee)+"-01-01"
                ET.SubElement(niv_1, "exerciceFiscalAu").text = str(rec.annee)+"-12-31"
                ET.SubElement(niv_1, "annee").text = str(rec.annee)
                commune = ET.SubElement(niv_1, "commune")
                ET.SubElement(commune, "code").text = societe.code_commune

                ET.SubElement(niv_1, "numeroCIN").text = societe.n_cni
                ET.SubElement(niv_1, "numeroCNSS").text = societe.n_cnss
                ET.SubElement(niv_1, "numeroCE").text = societe.n_ce
                ET.SubElement(niv_1, "numeroRC").text = societe.n_rc
                ET.SubElement(niv_1, "numeroFax").text = societe.fax
                ET.SubElement(niv_1, "numeroTelephone").text = societe.phone
                ET.SubElement(niv_1, "identifiantTP").text = societe.n_tp
                ET.SubElement(niv_1, "email").text = societe.email

                ET.SubElement(niv_1, "effectifTotal").text = str(rec.effectif)

                ET.SubElement(niv_1, "nbrPersoPermanent").text = str( rec.nb_pp)
                ET.SubElement(niv_1, "nbrPersoOccasionnel").text = str( rec.nb_po)
                ET.SubElement(niv_1, "nbrStagiaires").text = str( rec.nb_stg)

                ET.SubElement(niv_1, "totalMtRevenuBrutImposablePP").text = str( rec.total_sbi_pp)
                ET.SubElement(niv_1, "totalMtRevenuNetImposablePP").text = str( rec.total_sni_pp)
                ET.SubElement(niv_1, "totalMtTotalDeductionPP").text = str( rec.total_ded_pp)
                ET.SubElement(niv_1, "totalMtIrPrelevePP").text = str( rec.total_ir_pp)

                ET.SubElement(niv_1, "totalMtBrutSommesPO").text = str( rec.total_sb_po)
                ET.SubElement(niv_1, "totalIrPrelevePO").text = str( rec.total_ir_po)

                ET.SubElement(niv_1, "totalMtBrutTraitSalaireSTG").text = str( rec.total_brut_stg)
                ET.SubElement(niv_1, "totalMtBrutIndemnitesSTG").text = str( rec.total_ind_stg)
                ET.SubElement(niv_1, "totalMtRetenuesSTG").text = str( rec.total_ret_stg)
                ET.SubElement(niv_1, "totalMtRevenuNetImpSTG").text = str( rec.total_net_stg)

                ET.SubElement(niv_1, "totalSommePayeRTS").text = str( rec.somme_rts)
                ET.SubElement(niv_1, "totalmtAnuuelRevenuSalarial").text = str( rec.tot_rev_annuel)
                ET.SubElement(niv_1, "totalmtAbondement").text = str( rec.mt_abondement)
                ET.SubElement(niv_1, "montantPermanent").text = str(rec.mt_permanent)
                ET.SubElement(niv_1, "montantOccasionnel").text = str( rec.mt_occasionnel)
                ET.SubElement(niv_1, "montantStagiaire").text = str( rec.mt_stagiaire)
                ET.SubElement(niv_1, "referenceDeclaration").text = str( rec.ref_declaration)

                liste_pp1 = ET.SubElement(niv_1, "listPersonnelPermanent")



                for line in rec.etat_line_ids:
                    liste_pp = ET.SubElement(liste_pp1, "PersonnelPermanent")
                    ET.SubElement(liste_pp, "nom").text = str(line.employee_id.lastname)
                    ET.SubElement(liste_pp, "prenom").text = str(line.employee_id.firstname)
                    adresse=""
                    if line.employee_id.address_home:
                        adresse = line.employee_id.address_home.encode('utf8')

                    ET.SubElement(liste_pp, "adressePersonnelle").text = adresse.decode('utf8',errors='xmlcharrefreplace')
                    """
                    print '**sss**',line.employee_id.matricule, line.employee_id.id
                    if line.employee_id.address_home:
                        adresse = line.employee_id.address_home.encode('utf8', 'ignore')
                        print '---sss------faef-----',line.employee_id.address_home.encode('utf8', 'ignore')
                    """
                    # print 'erreur ',line.employee_id.matricule, adresse



                    ET.SubElement(liste_pp, "numCNI").text = str(line.employee_id.cin)
                    ET.SubElement(liste_pp, "numCE").text = str(line.employee_id.otherid)
                    ET.SubElement(liste_pp, "numPPR").text = ""
                    ET.SubElement(liste_pp, "numCNSS").text = str(line.employee_id.ssnid)
                    ET.SubElement(liste_pp, "ifu").text = ""
                    ET.SubElement(liste_pp, "mtBrutTraitementSalaire").text = str(line.s_salaire_brut)
                    ET.SubElement(liste_pp, "periode").text = str(line.s_jrs)
                    ET.SubElement(liste_pp, "mtExonere").text = str(line.s_indemnites)
                    ET.SubElement(liste_pp, "mtEcheances").text = str(0.0)
                    ET.SubElement(liste_pp, "nbrReductions").text = str(line.nbr_reductions)
                    ET.SubElement(liste_pp, "mtIndemnite").text = str(line.s_ind_fp)
                    ET.SubElement(liste_pp, "mtAvantages").text = str(line.s_avantage_nature)
                    ET.SubElement(liste_pp, "mtRevenuBrutImposable").text = str(line.s_sbi)
                    ET.SubElement(liste_pp, "mtFraisProfess").text = str(line.s_frais_pro)
                    ET.SubElement(liste_pp, "mtCotisationAssur").text = str(line.s_cot_ass)
                    ET.SubElement(liste_pp, "mtAutresRetenues").text = str(line.s_autres_ret)
                    ET.SubElement(liste_pp, "mtRevenuNetImposable").text = str(line.s_sni)
                    ET.SubElement(liste_pp, "mtTotalDeduction").text = str(99999)
                    ET.SubElement(liste_pp, "irPreleve").text = str(line.s_igr)
                    ET.SubElement(liste_pp, "casSportif").text = "false"
                    ET.SubElement(liste_pp, "numMatricule").text = str(line.employee_id.matricule)
                    ET.SubElement(liste_pp, "datePermis").text = ""
                    ET.SubElement(liste_pp, "dateAutorisation").text = ""
                    sit_fam = ET.SubElement(liste_pp, "refSituationFamiliale")
                    ET.SubElement(sit_fam, "code").text = str(line.situation)
                    taux = ET.SubElement(liste_pp, "refTaux")
                    ET.SubElement(taux, "code").text = 'TPP.20.2009'

                chemin = '/opt/odoo/odoo/SimplIGR.xml'
                tree = ET.ElementTree(niv_1)
                file = open(chemin,'w+')
                #file.write(ET.tostring(niv_1, pretty_print = True))
                file.write(ET.tostring(niv_1))
                file.close()
                xml_file = base64.encodestring(open(chemin,'rb').read())
                rec.write({'file_id':xml_file})
                self.edi_filename = "TraitementsEtSalaires_TNC_{}.xml".format(self.annee)

        @api.multi
        def generer_etat_9421(self):

            p = self.env['account.period']
            bul = self.env['hr.payroll_ma.bulletin']
            line_etat = self.env['etat.9421.line']


            for res in self:
                res.name = "Etat 9421 - Année : " + str(res.annee)
                # extraction des period_ids incluses dans l'année sélectionnée
                periodes=[]
                for i in range(1,13,1):
                    v_period = str(i).rjust(2,'0')+'/'+str(res.annee)
                    r_period = p.search([('name','=',v_period)])
                    if r_period:
                        periodes.append(r_period.id)

                # Suppression des lignes déjà existantes
                query = " DELETE FROM etat_9421_line WHERE id_etat ="+str(res.id)+". "
                res.env.cr.execute(query)
                res.env.invalidate_all()


                # Identification de tous les employés qui ont été déclarés durant l'année sélectionnée
                r_employe = bul.search([('period_id','in',tuple(periodes))])

                requete = "select distinct(a.employee_id), (select max(period_id) from hr_payroll_ma_bulletin where employee_id = a.employee_id and period_id in "+str(tuple(periodes))+" ) from hr_payroll_ma_bulletin a where period_id in "+str(tuple(periodes))+"  group by employee_id, period_id  order by employee_id"
                self._cr.execute(requete)
                resultat = self._cr.fetchall()

                for j in resultat:
                    # print '---- ', j
                    bulletin = bul.search([('employee_id','=',int(j[0])),('period_id','=',int(j[1]))])
                    line_etat.create({
                        'id_etat':res.id,
                        'employee_id':j[0],
                        'period_id':int(j[1]),
                        's_salaire_brut':bulletin.cumul_sb,
                        's_avantage_nature':0.0,
                        's_ind_fp':bulletin.cumul_indemnites_fp,
                        's_indemnites':bulletin.cumul_avn,
                        'taux_fp':20,
                        's_sbi':bulletin.cumul_sbi,
                        's_frais_pro':bulletin.cumul_fp,
                        's_cot_ass':bulletin.cumul_ee_cotis,
                        's_autres_ret':0.0,
                        's_sni':bulletin.cumul_sni,
                        's_jrs':bulletin.cumul_work_days,
                        's_igr':bulletin.cumul_igr,
                        'nbr_reductions':bulletin.personnes,
                    })






class RecapPaieLine(models.Model):
        _name = 'etat.9421.line'
        _order = 'matricule'


        id_etat = fields.Many2one('etat.9421')

        # détail ligne
        employee_id = fields.Many2one('hr.employee',string=u"Employé")
        matricule = fields.Integer(related='employee_id.matricule', string="Matricule",store=True)
        cin = fields.Char(related='employee_id.cin', string="CIN")
        cnss = fields.Char(related='employee_id.ssnid', string=u"N°CNSS")
        adresse = fields.Char(related='employee_id.address_home', string="Adresse personnelle")
        situation = fields.Char(compute='get_marital_status', string="Situation")
        period_id = fields.Many2one('account.period', string=u"Période")
        # les cumuls
        s_salaire_brut = fields.Float(string="Salaire brut")
        s_avantage_nature = fields.Float(string="avantages en nature")
        s_ind_fp = fields.Float(string=u"Montant ind. frais pro")
        s_indemnites = fields.Float(string=u"Montant exonérations")
        taux_fp = fields.Integer(string="Taux frais professionnels")
        s_sbi = fields.Float(string="Revenue brut imposable")
        s_frais_pro = fields.Float(string="Montant Frais professionnels")
        s_cot_ass = fields.Float(string="Cotisations assurance")
        s_autres_ret = fields.Float(string="Autres retenues")
        s_ech = fields.Float(string=u"Montant échéances")
        s_date_ac = fields.Date(string=u"Date de l'AC")
        s_date_ph = fields.Date(string="Date du PH")
        s_sni = fields.Float(string="Net imposable")
        nbr_reductions = fields.Integer(string=u"Nbr déductions charges de famille")
        s_jrs = fields.Float(string=u"Période en jours")
        s_igr = fields.Float(string=u"I.R prélevé")

        @api.multi
        def get_marital_status(self):

            for res in self:

                if res.employee_id.marital == 'single':
                    res.situation = 'C'
                if res.employee_id.marital == 'married':
                    res.situation = 'M'
                if res.employee_id.marital == 'divorced':
                    res.situation = 'D'
                if res.employee_id.marital == 'widower':
                    res.situation = 'V'
