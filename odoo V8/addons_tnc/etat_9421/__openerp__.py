# -*- coding: utf-8 -*-
{
    'name': 'Etat 9421',
    'version': '1.0',
    'category': 'HR',
    'complexity': "normal",
    'description': """
     Module qui génère l'etat 9421
""",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['hr_payroll_ma'],
    'data': ['etat_9421_view.xml',
             'company_data_view.xml',
             'report/reports_view.xml',
             'report/etat_ir.xml',
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

