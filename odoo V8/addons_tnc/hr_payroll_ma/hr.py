# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
import time


class hr_employee(osv.osv):
    _inherit = 'hr.employee'
    _columns = {
        'matricule' : fields.integer('Matricule'),
        'cin' : fields.char('CIN', size=64),
        'date': fields.date(u"Date entrée", help=u"Cette date est requipe pour le calcul de la prime d'anciennet�"),
        'anciennete': fields.boolean(u"Prime d'ancienneté", help=u"Est ce que cet employé benificie de la prime d'ancienneté"),
        'mode_reglement' : fields.selection([('virement', 'Virement'), ('cheque', u'Chèque'), ('espece', u'Espèce'), ], u'Mode De Réglement'),
        'bank' : fields.char(u'Banque', size=128),
        'compte' : fields.char(u'Compte bancaire', size=128),
        'chargefam' : fields.integer(u'Nombre de personnes à charge'),
        'logement': fields.float('Abattement Fr Logement'), 
        'affilie':fields.boolean(u'Affilié', help='Est ce qu on va calculer les cotisations pour cet employe'),
        'address_home' : fields.char(u'Adresse Personnelle', size=256),
        'address' : fields.char(u'Adresse Professionnelle', size=256),
        'phone_home' : fields.char(u'Télephone Personnel', size=256),
        'email_perso' : fields.char(u'Email perso', size=256),
        'ssnid': fields.char('CNSS',),
            }
    _defaults = {
        'chargefam' : 0,
        'logement' : 0,
        'anciennete' : True,
        'affilie' :True,
        'date' : lambda * a: time.strftime('%Y-%m-%d'),
        'mode_reglement' : 'virement'
                }
hr_employee()


class hr_contract(osv.osv) :
    _inherit = 'hr.contract'
    _description = 'Employee Contract'
    _columns = {
                'working_days_per_month' : fields.integer(u'Jours travaillés par mois'),
                'hour_salary' : fields.float(u'Salaire Heure'),
                'monthly_hour_number' : fields.float(u'Nombre Heures par mois'),
				'ir' : fields.boolean(u'IR'),
                'cotisation':fields.many2one('hr.payroll_ma.cotisation.type', u'Type cotisation', required=True),
                'rubrique_ids': fields.one2many('hr.payroll_ma.ligne_rubrique', 'id_contract', 'Les rubriques'),
                }
    _defaults = {
        'working_days_per_month' : 26,
                }
    def net_to_brute(self, cr, uid, ids, context={}):
        contract = self.pool.get('hr.contract').browse(cr, uid, ids[0])
        salaire_base = contract.wage
        cotisation = contract.cotisation
        personnes = contract.employee_id.chargefam
        params = self.pool.get('hr.payroll_ma.parametres')
        objet_ir = self.pool.get('hr.payroll_ma.ir')
        id_ir = objet_ir.search(cr, uid, [])
        liste = objet_ir.read(cr, uid, id_ir, ['debuttranche', 'fintranche', 'taux', 'somme'])
        ids_params = params.search(cr, uid, [])
        dictionnaire = params.read(cr, uid, ids_params[0])
        abattement = personnes * dictionnaire['charge']
        base = 0
        salaire_brute = salaire_base
        trouve=False
        trouve2=False
        while(trouve == False):
            salaire_net_imposable=0
            cotisations_employee=0
            for cot in cotisation.cotisation_ids :
                if cot.plafonee and salaire_brute >= cot.plafond:
                    base = cot.plafond
                else : base = salaire_brute
                cotisations_employee += base * cot['tauxsalarial'] / 100
            fraispro = salaire_brute * dictionnaire['fraispro'] / 100
            if fraispro < dictionnaire['plafond']:
                salaire_net_imposable = salaire_brute - fraispro - cotisations_employee
            else :
                salaire_net_imposable = salaire_brute - dictionnaire['plafond'] - cotisations_employee
            for tranche in liste:
                if(salaire_net_imposable >= tranche['debuttranche']/12) and (salaire_net_imposable < tranche['fintranche']/12):
                    taux = (tranche['taux'])
                    somme = (tranche['somme']/12) 
            
            ir = (salaire_net_imposable * taux / 100) - somme - abattement
            if(ir < 0):ir = 0
            salaire_net=salaire_brute - cotisations_employee - ir
            if(int(salaire_net)==int(salaire_base) and trouve2==False):
                trouve2=True
                salaire_brute-=1
            if(round(salaire_net,2)==salaire_base):trouve=True
            elif trouve2==False : salaire_brute+=0.5
            elif trouve2==True : salaire_brute+=0.01
            
        self.write(cr, uid, [contract.id], {'wage' : round(salaire_brute,2)})
        return True
hr_contract()

class hr_holidays_status(osv.osv):
    _inherit = "hr.holidays.status"
    _description = 'Holidays'
    _columns = {
        'payed':fields.boolean(u'Payé', required=False),
            }
    _defaults = {
        'payed': True
    }
hr_holidays_status()