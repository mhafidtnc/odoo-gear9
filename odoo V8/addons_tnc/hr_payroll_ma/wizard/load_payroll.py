from osv import osv,fields
from tools.translate import _
import time
import netsvc
import base64

class load_payroll_wizard(osv.osv_memory):
    _name = 'load.payroll.wizard'
    
    def action_load_payroll(self, cr, uid, ids, context=None):
        payroll = self.pool.get('hr.payroll_ma')
        try:
            file_content_decoded = base64.decodestring(self.browse(self, uid, ids[0], context).order_file)
        except IOError, e:
            raise wizard.except_wizard(_('Error !'), _('Merci de donner un chemin valide!'))
        order_data = file_content_decoded.split('\n')
        list_ids = {}
        for line_data in order_data:
            datas = line_data.split(';')
            if (len(datas)>1):
                matricule = int(datas[1].replace('"', '').strip())
                heures = datas[2].replace('"', '').strip().split(':')[0]
                list_ids[matricule]=heures
                #print matricule, heures,list_ids[1], datas[2].replace('"', '').strip().split(':')
        #print list_ids
        for proc_id in list_ids:
            cr.execute("""
            update hr_contract set monthly_hour_number="""+str(list_ids[proc_id])+"""
            where id = (select hc.id from hr_contract hc, hr_employee he ,resource_resource rr 
            where hc.employee_id=he.id and rr.matricule = '"""+str(proc_id)+"""' and rr.id=he.resource_id)
            """)
        pay_data = {
                'date_start' : self.browse(self, uid, ids[0], context).date_start,
                'date_end' : self.browse(self, uid, ids[0], context).date_end,
                'period_id' :self.browse(self, uid, ids[0], context).period_id.id, 
                } 
        pay_id = payroll.create(cr, uid, pay_data)
        #print pay_id
        payroll.generate_employees(cr, uid, [pay_id], context)
        payroll.compute_all_lines(cr, uid, [pay_id], context)    
            
        return True
    
    _columns = {
        'order_file': fields.binary('Fichier de pointage',required = True),
        'date_start': fields.date('Date debut',required = True),
        'date_end': fields.date('Date fin',required = True),
        'period_id': fields.many2one('account.period', 'Periode',required = True),
    }

    
load_payroll_wizard()