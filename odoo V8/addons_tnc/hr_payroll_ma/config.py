# -*- coding: utf-8 -*-

from openerp.osv import osv, fields

class hr_payroll_ma_parametres(osv.osv):
    
    def _get_credit_account(self, cr, uid, context):
        account_obj = self.pool.get('account.account')
        res = account_obj.search(cr, uid, [('code', 'like', '4432%')], limit=1)
        if res:
            return res[0]
        else:
            return False
        
    def _get_debit_account(self, cr, uid, context):
        account_obj = self.pool.get('account.account')
        res = account_obj.search(cr, uid, [('code', 'like', '6171%')], limit=1)
        if res:
            return res[0]
        else:
            return False
        
    def _get_ir_account(self, cr, uid, context):
        account_obj = self.pool.get('account.account')
        res = account_obj.search(cr, uid, [('code', 'like', '44525%')], limit=1)
        if res:
            return res[0]
        else:
            return False
        
    _name = 'hr.payroll_ma.parametres'
    _description = 'Parametres'
    _columns = {
        'smig' : fields.float("SMIG"),
		'arrondi' : fields.boolean("Arrondi"),
        'charge' : fields.float("Charges de familles", help="Les charges de famille deduites de IGR"),
        'fraispro' : fields.float("Frais Proffessionnels"),
        'plafond' : fields.float("Plafond"),
        'credit_account_id': fields.many2one('account.account', 'Compte de credit IGR'),
        'partner_id': fields.many2one('res.partner', 'Partenaire', change_default=True, readonly=True,),
        'salary_credit_account_id' : fields.many2one('account.account', 'Compte de credit',),
        'salary_debit_account_id' : fields.many2one('account.account', u'Compte de debit',),
        'analytic_account_id':fields.many2one('account.analytic.account', 'Analytic Account',)
                 } 
    _defaults = {
        'arrondi': True,
        'salary_credit_account_id': _get_credit_account,
        'salary_debit_account_id': _get_debit_account,
        'credit_account_id': _get_ir_account,
    }  
hr_payroll_ma_parametres()

class hr_ir(osv.osv):
    _name = 'hr.payroll_ma.ir'
    _description = 'IR'
    _columns = {
        'debuttranche' : fields.float("Début de Tranche"),
        'fintranche' : fields.float("Fin de tranche"),
        'taux' : fields.float("taux"),
        'somme' : fields.float("Somme a déduire")
                 }   
hr_ir()

class hr_anciennete(osv.osv):
    _name = 'hr.payroll_ma.anciennete'
    _description = "Tranches de la prime d'anciennete"
    _columns = {
        'debuttranche' : fields.float(u"Début Tranche"),
        'fintranche' : fields.float(u"Fin Tranche"),
        'taux' : fields.float(u"taux")
            }
hr_anciennete()


class hr_cotisation(osv.osv):
    _name = 'hr.payroll_ma.cotisation'
    _description = 'Configurer les cotisations'
    _columns = {
        'code' : fields.char("Code", size=64),
        'name' : fields.char("Designation", size=64),
        'tauxsalarial' : fields.float("Taux Salarial"),
        'tauxpatronal' : fields.float("Taux Patronal"),
        'plafonee' : fields.boolean(u'Cotisation plafonée'),
        'plafond' : fields.float(u"Plafond"),
        'credit_account_id': fields.many2one('account.account', 'Compte de credit', ),
        'debit_account_id': fields.many2one('account.account', 'Compte de débit'),
         }
hr_cotisation()

class hr_cotisation_type(osv.osv):

    _name = 'hr.payroll_ma.cotisation.type'
    _description = 'Configurer les types de cotisation'
    _columns = {
        'name' : fields.char("Designation", size=64),
        'cotisation_ids' : fields.many2many('hr.payroll_ma.cotisation', 'salary_cotisation', 'cotisation_id', 'cotisation_type_id', 'Cotisations'),
}
hr_cotisation_type()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: