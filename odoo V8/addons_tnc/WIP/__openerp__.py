
{
    'name': 'CRM: Work In Progress',
    'version': '1.0',
    'category': 'CRM',
    'complexity': "normal",
    'description': """
   Module générant le WIP d'un manager: Opportunités qui ne sont pas à l'état "Perdu" et dont le dernier statut n'est pas "Livré"
""",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['base','crm','CRM_custom'],
    'data': ['wip_view.xml',
             'report/reports_view.xml',
             'report/wip.xml',
    ],
    'demo': [
        '',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

