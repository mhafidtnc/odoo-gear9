# -*- coding: utf-8 -*-
from openerp import fields, models,api


class OpportuniteRapport(models.Model):
    _name = 'opportunite.rapport'

    name = fields.Char()
    revenu_estime = fields.Float()
    partner_id= fields.Char()
    statut= fields.Char()
    id_wiz = fields.Many2one('wip.wizard')





# Classe qui représente le wizard pour générer le rapport wip
class WipWizard(models.Model):
        _name = 'wip.wizard'


        account_manager = fields.Many2one('res.users',string="Account manager")
        opp_ids_print = fields.One2many('opportunite.rapport','id_wiz')


        # Méthode qui générer le rapport WIP du manager sélectionné
        @api.multi
        def generate_wip(self):
            v_opp = self.env['crm.lead']
            p_opp = self.env['opportunite.rapport']

            opp_ids=[]
            for rec in self:


                v_opp_res = v_opp.search([('user_id','=',rec.account_manager.id),('stage_id.name','!=','Perdu')])

                for y in v_opp_res:

                    # On vérifie si le dernier statut de l'opportunité n'est pas "Livré" ou "Annulé"
                    if y.status_opportunity:

                        v_status = y.status_opportunity[0].op_statut
                        if v_status not in ['livre','annule']:
                            opp_ids.append(y)
                    else:
                        opp_ids.append(y)


                rec.opp_ids_print.unlink()
                for z in opp_ids:
                    p_opp.create({'id_wiz':rec.id,'name':z.name,'revenu_estime':z.planned_revenue,'partner_id':z.partner_id.name,'statut':z.stage_id.name})


                return self.env['report'].get_action(self,'WIP.wip_report')







