
{
    'name': 'Devis custom',
    'version': '1.0',
    'category': 'CRM',
    'complexity': "normal",
    'description': """
    Customization du devis: Ajout de l'opportunité
""",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['crm','sale','purchase'],
    'data': [
        'devis_custom_view.xml',
        'demande_prix_custom_view.xml',
    ],
    'demo': [
        '',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

