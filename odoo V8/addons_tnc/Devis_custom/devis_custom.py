# -*- coding: utf-8 -*-
from openerp import fields, models,api


class sale_order_opportunity(models.Model):
    _inherit='sale.order'

    devis_opportunity = fields.Many2one('crm.lead',domain="[('type','=','opportunity')]",string="Opportunité",readonly=True)


    @api.multi
    def write(self, vals):
        for rec in self:
            op = self.env['crm.lead']
            print 'testxxxx'
            if vals.get('devis_opportunity',"rien") != 'rien':
                # on met à jour le champ "devis_opp" de l'ancienne opportunité à False
                op.search([('id','=',rec.devis_opportunity.id)]).write({'devis_opp':False})

                # On affecte le devis en cours à l'opportunité sélectionnée
                op.search([('id','=',vals['devis_opportunity'])]).write({'devis_opp':rec.id})

        return super(sale_order_opportunity,self).write(vals)


    @api.model
    def create(self, vals):

            op = self.env['crm.lead']
            print 'vals ', vals

            res = super(sale_order_opportunity,self).create(vals)

            if vals.get('devis_opportunity',"rien") != 'rien':
                op.search([('id','=',vals['devis_opportunity'])]).write({'devis_opp':res.id})

            return res