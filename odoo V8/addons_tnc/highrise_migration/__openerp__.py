# -*- coding: utf-8 -*-

{
    'name': 'Highrise ODOO deals migration',
    'version': '1.0',
    'category': 'CRM',
    'complexity': 'normal',
    'description': '''
        Highrise ODOO deals migration
    ''',
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['crm','base'],
    'data': [
        'wizard/highrise_deals.xml',
    ],
    'demo': [],
    'test':[],
    'installable': True,
    'auto_install': False,
    'application': False,
}
