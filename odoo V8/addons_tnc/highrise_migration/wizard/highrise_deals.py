from openerp import models, fields, api
from openerp.exceptions import except_orm
from highton import Highton

class highrise_deals(models.TransientModel):
    _name = "highrise.deals"

    date = fields.Date(string="Date", required=True)

    @api.multi
    def import_deals(self):
        user_obj = self.env['res.users']
        categ_obj = self.env['crm.case.categ']
        stage_obj = self.env['crm.case.stage']
        patner_obj = self.env['res.partner']
        lead_obj = self.env['crm.lead']
        high = Highton(
            api_key = '387611db71ab98b1344b2d75e0ed432d',
            user ='thenextclic' #'hrouissi@thenextclic.com'
        )

        users={}


        #people = high.get_users()
       # for p in people:
        #    print p.name, p.highrise_id

        people = high.get_users()
        for p in people:
            user = user_obj.search([('name','=',p.name)])
            if not user:
                raise except_orm(('Attention!'), (u"L'utilisateur %s n'existe pas!" % (p.name,)))
                #user = user_obj.create({'name':p.name,'login':p.name})
            users[p.highrise_id]=user.id

        #print "users", users

        categs  ={}

        Categ = high.get_deal_categories()
        for c in Categ:
            categ = categ_obj.search([('name','=',c.name)])
            #print "categ categ categ categ categ categ"
            #print categ, categ[0].name, c.name
            if not categ:
                obj_id =False
                object = self.env['ir.model'].search([('name','=','crm.lead')])
                if object:
                    obj_id = object.id
                categ = categ_obj.create({'name':c.name,'object_id':obj_id})
            if len(categ) > 1:
                categs[c.highrise_id]=categ[0].id
            else:
                categs[c.highrise_id]=categ.id

        for wizard in self:
            date = wizard.date

            date_deals  = date.replace("-", "")+"000000"

            print date_deals

            #deals = high.get_deals_since(date_deals)
            deals = high.get_deals()


            new_leads=[]
            print "len(deals)" ,len(deals)
            for deal in deals:
                print deal.highrise_id, deal.name
                lead_id = lead_obj.search([('highrise_id','=',deal.highrise_id)])

                if not lead_id:

                    partner_id = False
                    partner_name =""
                    if hasattr(deal, 'party'):
                        clas = str(deal.party)
                        if "Company" in clas:
                            partner = patner_obj.search([('name','=',deal.party.name)])
                            if partner:
                                partner_id = partner.id
                            else:
                                partner_id = patner_obj.create({'name':deal.party.name})
                                partner_id = partner_id.id
                            partner_name = deal.party.name
                        if "Person" in clas:
                            partner = patner_obj.search([('name','=',deal.party.company_name)])
                            if partner:
                                partner_id = partner.id
                            else:
                                partner_id = patner_obj.create({'name': deal.party.company_name})
                                partner_id = partner_id.id
                            partner_name = deal.party.company_name

                    stage  = self.env.ref('crm.stage_lead1')
                    if deal.status == 'won':
                        stage  = self.env.ref('crm.stage_lead6')
                    if deal.status == 'lost':
                        stage  = self.env.ref('crm.stage_lead7')

                    data = {
                        'highrise_id':deal.highrise_id,
                        'type':'opportunity',
                        'description':deal.background,
                        'currency':deal.currency,
                        'name': deal.name,
                        'planned_revenue':deal.price,
                        'user_id': users.get(deal.responsible_party_id,False),
                        'stage_id':stage.id,
                        'write_date': deal.updated_at or False,
                        'partner_id': partner_id,
                        'partner_name':partner_name,
                    }

                    #print "status_changed_on", deal.status_changed_on
                    #print "deal.updated_at", deal.updated_at

                    if deal.status_changed_on:
                        data['date_last_stage_update']=deal.status_changed_on

                    if categs.get(deal.category_id,False):
                        data['categ_ids']=[(6, False,[categs.get(deal.category_id,False)])]

                    #print "data data data data data data"
                    #print data

                    new_leads.append(lead_obj.create(data))

            for deal in deals:
                lead=lead_obj.search([('highrise_id','=',deal.highrise_id)])
                #print lead, new_leads
                #print "lead, new_leads"
                if lead in new_leads:
                    #print "lead, new_leads 2"
                    notes = high.get_deal_notes(deal.highrise_id)
                    if notes:
                            for n in notes:
                                post_msg=''
                                post_msg+=n.body+'\n'
                                if n.attachments:
                                    for att in n.attachments:
                                        post_msg+=att.name+': '+att.url+'\n'
                                lead.message_post(post_msg)
                                #print "message notes:", post_msg

                    emails = high.get_deal_emails(deal.highrise_id)
                    if emails:
                            for e in emails:
                                post_msg=''
                                post_msg+=e.body+'\n'
                                if e.attachments:
                                    for att in e.attachments:
                                        post_msg+=att.name+': '+att.url+'\n'
                                lead.message_post(post_msg)
                                #print "email:", post_msg
        return {'type': 'ir.actions.act_window_close'}