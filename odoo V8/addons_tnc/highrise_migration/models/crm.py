from openerp import models,fields,api

class crm(models.Model):
    _inherit = 'crm.lead'

    highrise_id = fields.Integer(string="Highrise ID",required=False)
    currency  = fields.Char(string="Devise", required=False)
