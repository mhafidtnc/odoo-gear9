# -*- coding: utf-8 -*-
{
    'name': 'e-BDS : DAMANCOM',
    'version': '1.0',
    'category': 'hr',
    'description': "Le système de Télédéclaration et de Télépaiement de la caisse Nationale de la sécurité Sociale",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'depends': ["base","hr_payroll_ma"],
    'init_xml': [
    ],
    'update_xml': [
               'wizard/e_bds_wizard_view.xml',
               'e_bds_view.xml'  
    ],
    'demo_xml': [
    ],
    'test': [
             ],
    'installable': True,
    'active': False,
}

