# -*- coding: utf-8 -*-
from openerp import fields, models,api
import convertion

class AccountInvoice(models.Model):
    _inherit='account.invoice'

    opportunity_id = fields.Many2one('crm.lead',domain="[('type','=','opportunity')]",string="Opportunité")
    amount_text=fields.Char('amount_text',compute='change_amount')

    @api.depends('amount_total')
    def change_amount(self):
        self.amount_text = convertion.trad(self.amount_total, 'DH').upper()
        return True

