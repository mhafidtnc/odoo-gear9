
{
    'name': 'Ajout de l opportunité à la facture',
    'version': '1.0',
    'category': 'CRM',
    'complexity': "normal",
    'description': """
    Customization de la facture client: Ajout de l'opportunité
""",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['crm','sale','account'],
    'data': ['add_opportunity_invoice_view.xml',
    ],
    'demo': [
        '',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

