# -*- coding: utf-8 -*-

{
    'name': 'Leads link to sale order',
    'version': '1.0',
    'category': 'CRM',
    'complexity': 'normal',
    'description': '''
        Leads link to sale order
    ''',
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['crm','sale','sale_crm'],
    'data': [

    ],
    'demo': [],
    'test':[],
    'installable': True,
    'auto_install': False,
    'application': False,
}
