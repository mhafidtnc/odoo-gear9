# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import api, fields, models, _

class AccountInvoiceTnc(models.Model):
    _inherit = 'account.invoice'

    # @api.multi
    # def update_due_date(self,):
    #     for inv in self:
    #         inv.signal_workflow('invoice_cancel')
    #         inv.action_cancel_draft()
    #         val = inv.onchange_payment_term_date_invoice(inv.payment_term.id, inv.date_depot)
    #         inv.signal_workflow('invoice_open')

    @api.multi
    def update_due_date(self,):
        for inv in self:
            for ml in inv.move_id.line_id:
                if ml.date_maturity:
                    ml.date_maturity = inv.date_depot
