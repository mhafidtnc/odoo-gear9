# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'tnc accounting',
    'version': '1.0',
    'author': 'KARIZMA CONSEIL',
    'website': 'https://www.karizma-conseil.com',
    'category': 'tnc accounting',
    'sequence': 8,
    'summary': 'accounting extention for TNC',
    'depends': [
        'account_payment','invoice_extend'
    ],
    'description': u"""
- Ordres de paiement: Ajout Réf paiement dans le rapport imprimable
- Date de dépot: Cette fonctionnalité est basée sur le module 'invoice_extend' qui ajoute la date de dépot sur la facture avec mise à jour de la date d'écheance.
                 Ce module ajout un bouton qui permet de forcer la date d'échéance sur les écritures comptables générées par la facture.
- Ajout du processus des cheques emis / remis                 
    """,
    'data': [
        'views/tnc_payment_order_report.xml',
        'views/tnc_invoice_view.xml',
        'views/tnc_account_voucher_view.xml',
        'views/tnc_payment_order_view.xml',
    ],
#    'demo': [],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
