# -*- coding: utf-8 -*-
from openerp import api, fields, models, _

class PaymentOrder(models.Model):
    _inherit = 'payment.order'

    partner = fields.Many2one('res.partner', string="Partenaire", related='line_ids.partner_id', store="True", readonly="True")
