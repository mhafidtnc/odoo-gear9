# -*- coding: utf-8 -*-
from openerp import fields, models,api

class Status_op(models.Model):
    _name='opportunity.statut'
    _order = 'id desc'

    op_statut = fields.Selection(selection='get_status',string="Status projet")
    op_statut_obj = fields.Many2one('status.objet',string="Status projet objet")
    status_ = fields.Char(string="Status ", compute='get_status_adequat',store=True)
    etape = fields.Many2one('crm.case.stage',related='opportunity_id.stage_id',string="Etape ",store=True)
    op_note_client = fields.Text(string="Note au client")
    op_next_livrable = fields.Text(string="Prochain livrable")
    op_date_livraison = fields.Date(string="Date livraison prévue")
    op_date_dernier_statut = fields.Datetime(related='opportunity_id.op_date_last_status',string="Date MAJ statut")
    op_date_create_dernier_statut = fields.Datetime(related='opportunity_id.op_date_create_last_status',string="Date création statut")

    opportunity_id = fields.Many2one('crm.lead',string='Opportunite')
    sequence = fields.Integer('Sequence',default=0)
    resp_id = fields.Many2one('res.users',related='opportunity_id.user_id',store=True)

    @api.multi
    @api.depends('op_statut','op_statut_obj')
    def get_status_adequat(self):

        for res in self:
            if res.op_statut:
                res.status_ = res.op_statut
            else:
                res.status_ = res.op_statut_obj.name


    def get_status(self):
        return (
            ('encours','En cours'),
            ('livre','Livré'),
            ('annule','Annulé'))

class Opportunity_stat(models.Model):
    _inherit='crm.lead'

    status_opportunity = fields.One2many('opportunity.statut','opportunity_id')
    op_date_last_maj = fields.Datetime(string="Date dernière MAJ")
    opp_date_cloture = fields.Datetime(string="Date clôture")
    op_date_last_status = fields.Datetime(string="Dernier MAJ statut")
    #op_date_last_status = fields.Datetime(string="Dernier MAJ statut", compute='get_last_status_date')
    op_date_create_last_status = fields.Datetime(string="Date création statut")
    #op_date_create_last_status = fields.Datetime(string="Date création statut", compute='get_last_status_date')
    op_last_status = fields.Char(string="Dernier statut", compute='get_last_status_date',store=True)


    @api.multi
    @api.depends('status_opportunity')
    def get_last_status_date(self):
        for res in self:
            i=0
            for status in res.status_opportunity.sorted(key=lambda r: r.id, reverse=True):

                if i==0:
                    res.op_date_last_status = status.write_date
                    res.op_date_create_last_status = status.create_date
                    res.op_last_status = status.status_

                i +=1

    @api.one
    def write(self,values):
        if self.type == 'opportunity':
           if 'stage_id' in values:
               if values['stage_id']:
                   states = self.env['crm.case.stage'].search([('id','=',values['stage_id'])])
                   if states.name in (u'Gagné','Perdu'):
                       values['opp_date_cloture']= fields.Date.context_today(self)
        values['op_date_last_maj'] = fields.Date.context_today(self)
        res = super(Opportunity_stat, self).write(values)
        if len(self.status_opportunity) > 1:
            i=len(self.status_opportunity)-1
            for  opp_stat in self.status_opportunity.sorted(key=lambda r: r.create_date):
                opp_stat.write({'sequence':i})
                i-=1


        return res





