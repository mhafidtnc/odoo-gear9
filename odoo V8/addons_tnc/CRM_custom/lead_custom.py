# -*- coding: utf-8 -*-
from openerp import fields, models,api


class CrmLead(models.Model):
    _inherit='crm.lead'

    contact_prenom = fields.Char()

class CrmLead2opportunityPartner(models.Model):
    _inherit = 'crm.lead2opportunity.partner'

    @api.multi
    def action_apply(self):

        resultat = super(CrmLead2opportunityPartner,self).action_apply()
        resultat['domain']="[('type', '=', 'lead')]"
        resultat['res_id']= False
        resultat['views']= False
        resultat['view_type']= "tree"
        resultat['view_mode']= "tree"
        print '/////', resultat

        return resultat
