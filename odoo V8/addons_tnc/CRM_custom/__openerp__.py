
{
    'name': 'CRM custom',
    'version': '1.0',
    'category': 'CRM',
    'complexity': "normal",
    'description': """
    Customization du module CRM
""",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['crm','sale'],
    'data': ['crm_view.xml',
             'res_partner_view.xml',
             'status_objet_view.xml',
    ],
    'demo': [
        '',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

