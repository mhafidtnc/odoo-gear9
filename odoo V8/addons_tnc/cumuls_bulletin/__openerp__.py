
{
    'name': 'Ajout des cumuls dans le bulletin de paie',
    'version': '1.0',
    'category': 'HR',
    'complexity': "normal",
    'description': """
     Ajout des cumuls dans le bulletin de paie: Jours travaillés, SBI, IR, ...
""",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['hr_payroll_ma'],
    'data': ['add_cumul_form_view.xml',
    ],
    'demo': [
        '',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

