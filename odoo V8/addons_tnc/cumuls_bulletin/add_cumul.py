# -*- coding: utf-8 -*-
from openerp import fields, models,api, osv
import base64
import time
from datetime import datetime



class HrPayroll_maBulletin(models.Model):
        _inherit = 'hr.payroll_ma.bulletin'


        cumul_work_days=fields.Float(compute='get_cumuls', string='Cumul Jrs travaillés', digits=(6,2))
        cumul_sbi= fields.Float(compute='get_cumuls', string='Cumul SBI', digits=(6,2))
        cumul_sb= fields.Float(compute='get_cumuls', string='Cumul SB', digits=(6,2))
        cumul_sni= fields.Float(compute='get_cumuls', string='Cumul SNI', digits=(6,2))
        cumul_igr= fields.Float(compute='get_cumuls', string='Cumul IR', digits=(6,2))
        cumul_ee_cotis = fields.Float(compute='get_cumuls', string='Cumul Cotis employé', digits=(6,2))
        cumul_er_cotis = fields.Float(compute='get_cumuls', string='Cumul Cotis employeur', digits=(6,2))
        cumul_fp = fields.Float(compute='get_cumuls', string='Cumul frais professionnels', digits=(6,2))
        cumul_avn = fields.Float(compute='get_cumuls', string='Cumul Avantages en nature', digits=(6,2))


        @api.multi
        def get_bulletin_cumuls(self,mois, annee, employe):

            ligne_bul_paie = self.env['hr.payroll_ma.bulletin.line']
            acct_period = self.env['account.period']
            bul = self.env['hr.payroll_ma.bulletin']
            cumuls = {}

            for res in self:
                v_period = str(mois).rjust(2,'0') + "/" + str(annee)
                period = acct_period.search([('code','=',v_period)])
                bulletin = bul.search([('period_id','=',period.id),('employee_id','=',employe)])

                if bulletin:
                    cumuls['working_days'] = bulletin.working_days
                    cumuls['salaire_brut_imposable'] = bulletin.salaire_brute_imposable
                    cumuls['salaire_brut'] = bulletin.salaire_brute
                    cumuls['salaire_net_imposable'] = bulletin.salaire_net_imposable
                    cumuls['igr'] = bulletin.igr
                    cumuls['cotisations_employee'] = bulletin.cotisations_employee
                    cumuls['cotisations_employer'] = bulletin.cotisations_employer
                    cumuls['avn'] = bulletin.indemnite
                    if bulletin.frais_pro < 2500:
                        cumuls['fp'] = bulletin.frais_pro
                    else:
                        cumuls['fp'] = 2500.0


            return cumuls

        @api.multi
        def get_cumuls(self):

            i=1

            for res in self:

                periode = res.period_id.name.split('/')

                mois = periode[0]
                annee = periode[1]

                somme_wd = 0.0
                somme_sbi = 0.0
                somme_sb = 0.0
                somme_sni = 0.0
                somme_igr = 0.0
                somme_cot_ee = 0.0
                somme_cot_er = 0.0
                somme_fp = 0.0
                somme_avn = 0.0
                for j in range(1,int(mois)+1,1):


                    valeur_mois = res.get_bulletin_cumuls(j,annee,res.employee_id.id)

                    if valeur_mois:

                        somme_wd += valeur_mois['working_days']
                        somme_sbi += valeur_mois['salaire_brut_imposable']
                        somme_sb += valeur_mois['salaire_brut']
                        somme_sni += valeur_mois['salaire_net_imposable']
                        somme_igr += valeur_mois['igr']
                        somme_cot_ee += valeur_mois['cotisations_employee']
                        somme_cot_er += valeur_mois['cotisations_employer']
                        somme_fp += valeur_mois['fp']
                        somme_avn += valeur_mois['avn']


                res.cumul_work_days = somme_wd
                res.cumul_sbi = somme_sbi
                res.cumul_sb = somme_sb
                res.cumul_sni = somme_sni
                res.cumul_igr = somme_igr
                res.cumul_ee_cotis = somme_cot_ee
                res.cumul_er_cotis = somme_cot_er
                res.cumul_fp = somme_fp
                res.cumul_avn = somme_avn