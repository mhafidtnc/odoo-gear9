
{
    'name': 'Reporting bulletins de paie',
    'version': '1.0',
    'category': 'HR',
    'complexity': "normal",
    'description': """
    Reporting bulletins de paie
""",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['base','hr_payroll_ma'],
    'data': [
             'report/bi_report_view.xml',
             'security/bi_rh_security.xml',
             'security/ir.model.access.csv',

    ],
    'demo': [

    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

