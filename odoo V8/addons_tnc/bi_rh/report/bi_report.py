# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from openerp.osv import fields,osv

class BpReport(osv.osv):
    _name = "bp.report"
    _description = "Rapport bulletin de paie"
    _auto = False
    _columns = {
        'employe': fields.char('Employee'),
        'department': fields.many2one('hr.department','Departement'),
        'date_recrutement': fields.date('Date de recrutement'),
        'periode': fields.many2one('account.period', 'Période'),
        'salaire_net': fields.float('Net à payer'),

        'mode_reglement':fields.char('Mode règlement'),

        'igr': fields.float('IGR'),

        'sbi': fields.float('Brut Imposable'),
        'sb': fields.float('Brut'),
        'cotis_employer': fields.float('Cotisations employeur'),
        'cotis_employe': fields.float('Cotisations employé'),
        'masse_salariale': fields.float('Masse salariale'),
        'job': fields.many2one('hr.job','Fonction'),

        }

    def init(self, cr):
        tools.drop_view_if_exists(cr, 'bp_report')
        cr.execute("""
            CREATE or REPLACE view bp_report as (
                 SELECT
                    min(bp.id) as id,
                    date as date_recrutement,
                    e.name_related as employe,
		    e.department_id as department,	
                    bp.period_id as periode,
                    salaire_net_a_payer as salaire_net,
                    e.mode_reglement as mode_reglement,
                    bp.igr as igr,
                    bp.salaire_brute_imposable as sbi,
                    bp.salaire_brute as sb,
                    bp.cotisations_employer as cotis_employer,
                    bp.cotisations_employee as cotis_employe,
                    bp.cotisations_employer + bp.salaire_brute  as masse_salariale,
                    e.job_id as job


                FROM
                    hr_payroll_ma_bulletin as bp, account_period as a, hr_employee as e,
                    hr_payroll_ma_bulletin_line as l_bp
                WHERE
                    bp.period_id = a.id
                    and bp.employee_id = e.id
                    and bp.id = l_bp.id_bulletin

                GROUP BY
                    date,e.name_related,e.department_id,bp.period_id,salaire_net_a_payer,bp.igr,bp.salaire_brute_imposable,e.job_id,
                    bp.cotisations_employer,bp.salaire_brute,bp.cotisations_employee,e.mode_reglement




            )
        """)


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
