# -*- coding: utf-8 -*-

from openerp import api, fields, models
import datetime
from openerp.tools.float_utils import float_round
from openerp.exceptions import except_orm, Warning, RedirectWarning

class stock_picking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def do_enter_transfer_details(self):



        return super(stock_picking,self).do_enter_transfer_details()


class sale_order_(models.Model):
    _inherit = 'sale.order'


    facture_ids = fields.One2many('facture.client','order_id')


class facture_client(models.Model):
    _name = 'facture.client'

    order_id = fields.Many2one('sale.order')
    name = fields.Char(string="Description")
    pourcentage = fields.Float(string="Pourcentage")
    montant = fields.Float(string="Montant ", compute='get_montant')
    tax_id = fields.Many2many('account.tax','rel_bc_tax','facture_id','tax_id',string="Taxes",domain="[('parent_id','=',False),('type_tax_use','!=','purchase')]")
    tax_amount = fields.Float(string="Montant taxe",compute='calc_totals')
    amount_ttc = fields.Float(string="Montant taxe",compute='calc_totals')
    invoiced = fields.Boolean(string=u"Facturé",default=False)
    invoice_id = fields.Many2one('account.invoice',string="Facture")


    # Fonction qui calcule le montant HT de la ligne
    @api.multi
    @api.depends('pourcentage','order_id.amount_untaxed')
    def get_montant(self):
        for rec in self:
            rec.montant = (rec.pourcentage/100) * rec.order_id.amount_untaxed


    # Redéfinition de la méthode create
    @api.model
    def create(self,vals):

        # On met par défaut la TVA à 20%
        tax = self.env['account.tax'].search([('type_tax_use','=','sale'),('amount','=',0.2)])

        somme_pourcentage=0
        for ligne in self.env['sale.order'].search([('id','=',vals['order_id'])]).facture_ids:
            somme_pourcentage += ligne.pourcentage

        somme_pourcentage+= vals['pourcentage']


        if somme_pourcentage > 100:
            raise except_orm(('Attention'), (u"La somme des pourcentages de facturation ne doit pas dépasser les 100% !!!"))

        id_lp = super(facture_client,self).create(vals)
        id_lp.tax_id = [(6,0,[tax.id])]

        return id_lp


    # Redéfinition de la méthode write
    @api.multi
    def write(self,vals):

        for rec in self:
            somme_pourcentage=0
            id_lp = super(facture_client,self).write(vals)
            for ligne in rec.order_id.facture_ids:
                somme_pourcentage += ligne.pourcentage

            if somme_pourcentage > 100:
                raise except_orm(('Attention'), (u"La somme des pourcentages de facturation ne doit pas dépasser les 100% !!!"))


            return id_lp

    # Fonction qui crée la facture
    @api.multi
    def facturer_ligne(self):

        for rec in self:

            invoice_id = self.env['account.invoice'].create({
                'partner_id':rec.order_id.partner_id.id,
                'date_invoice':fields.Date.context_today(self),
                'opportunity_id':rec.order_id.devis_opportunity.id,
                'origin':rec.order_id.client_order_ref,
                'account_id':rec.order_id.partner_id.property_account_receivable.id
            })

            if invoice_id:


                # On insére les lignes du bon de commande d'origine
                for ligne in rec.order_id.order_line:

                    if not ligne.tax_id:
                        dict = {
                                        'invoice_id':invoice_id.id,
                                        'product_id':ligne.product_id.id or False,
                                        'name':ligne.name,
                                        'quantity':ligne.product_uom_qty,
                                        'price_unit':0,

                                        }
                    else:
                        dict= {
                                        'invoice_id':invoice_id.id,
                                        'product_id':ligne.product_id.id or False,
                                        'name':ligne.name,
                                        'quantity':ligne.product_uom_qty,
                                        'price_unit':0,
                                        'invoice_line_tax_id':[(6,0,[ligne.tax_id.id or False])]
                                        }

                    self.env['account.invoice.line'].create(dict)


                self.env['account.invoice.line'].create({
                'invoice_id':invoice_id.id,
                'name':rec.name,
                'quantity':1.0,
                'price_unit':rec.montant,
                'invoice_line_tax_id':[(6,0,[rec.tax_id.id])]
                })
                rec.invoiced=True
                rec.invoice_id = invoice_id



    # Fonction qui calcule le montant de la taxe
    @api.multi
    @api.depends('tax_id','montant')
    def calc_totals(self):

        for rec in self:
            somme = 0
            for line in rec.tax_id:
                somme += (line.amount * rec.montant)

            rec.tax_amount = somme
            rec.amount_ttc = rec.montant + somme

