# -*- coding: utf-8 -*-

{
    "name": "TNC: Facturation client",
    "version": "1.1",
    "depends": ['sale','account'],
    "author": "BHECO SERVICES",
    'website': 'http://bhecoservices.com',
    "category": "ACCOUNTING",
    "description": "Facturation client",
                        
    "init_xml": [],
    'data': [
        'views/customer_invoicing_view.xml',
        'security/customer_invoicing_security.xml',
        'security/ir.model.access.csv',
    ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
