# -*- coding: utf-8 -*-
from openerp import fields, models,api


class Status_op(models.Model):
    _inherit='opportunity.statut'

    pac_id = fields.Many2one('plan.action.opportunity',string='PAC')
    client_id = fields.Char(string='Client',compute='get_client',store=True)
    montant = fields.Float(string='Montant',compute='get_montant',store=True)


    @api.one
    @api.depends('pac_id', 'opportunity_id')
    def get_client(self):
        if self.opportunity_id:
            self.client_id=self.opportunity_id.partner_id.name
        if self.pac_id:
            self.client_id=self.pac_id.client.name

    @api.one
    @api.depends('pac_id', 'opportunity_id')
    def get_montant(self):
        if self.opportunity_id:
            self.montant = self.opportunity_id.planned_revenue
        if self.pac_id:
            self.montant =self.pac_id.total_revenu_estime

class crm_op(models.Model):
    _inherit='crm.lead'

    id_pac = fields.Many2one('plan.action.opportunity',string="Plan d'action")
    devis_opp = fields.Many2one('sale.order',string='Devis')
    montant_devis = fields.Float(related='devis_opp.amount_total',string="Montant devis")


    @api.multi
    def get_devis(self):
        devis = self.env['sale.order']

        for rec in self:
            dev= devis.search([('devis_opportunity','=',rec.id)])
            if dev:
                rec.devis_opp = dev.id
        return True


class PAC(models.Model):
    _name='plan.action.opportunity'
    _inherit = 'mail.thread'

    name = fields.Char(string="Titre",required=True)
    client = fields.Many2one('res.partner',string="Client",domain="[('customer','=',True)]",required=True)
    responsable = fields.Many2one('res.users',string="Responsable")
    date_creation  =fields.Date(string='Date création')
    budget = fields.Float(string="Budget")
    total_revenu_estime = fields.Float(string=u"Revenu estimé",compute='calcul_revenu_estime')
    CA_realise = fields.Float(string="CA réalisé",compute='calcul_ca_realise')
    opportunity_ids = fields.One2many('crm.lead','id_pac',string="Opportunsité(s)")


    status_opportunity = fields.One2many('opportunity.statut','pac_id')

    @api.one
    def write(self,values):
        res = super(PAC, self).write(values)
        if len(self.status_opportunity) > 1:
            i=len(self.status_opportunity)-1
            for  opp_stat in self.status_opportunity.sorted(key=lambda r: r.create_date):
                opp_stat.write({'sequence':i})
                i-=1
        return res

    # Fonction qui calcule le budget du PAC
    @api.multi
    @api.depends('opportunity_ids')
    def calcul_revenu_estime(self):

        for rec in self:
            somme = 0
            for l in rec.opportunity_ids:
                somme += l.planned_revenue

            rec.total_revenu_estime = somme



    # Fonction qui calcule le chiffre d'affaires réalisé
    @api.multi
    @api.depends('opportunity_ids')
    def calcul_ca_realise(self):
        devis = self.env['sale.order']
        for rec in self:
            somme = 0
            for l in rec.opportunity_ids:
                d= devis.search([('devis_opportunity','=',int(l.id)),('state','in',('manual','progress','done'))])
                if d:
                    #print 'montant', d.amount_total,',nacme',rec.name
                    for x in d:
                        somme += x.amount_total
            rec.CA_realise = somme


