
{
    'name': 'Plan d action : PAC',
    'version': '1.0',
    'category': 'CRM',
    'complexity': "normal",
    'description': """
    PAC: Plan d'action regroupant plusieurs opportunités liées à un client (gestion du budget, gestion du CA réalisé,...)
""",
    'author': 'BHECO SERVICES',
    'website': 'http://www.bhecoservices.com',
    'images': [],
    'depends': ['crm','sale','CRM_custom'],
    'data': ['PAC_view.xml',
    ],
    'demo': [
        '',
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

