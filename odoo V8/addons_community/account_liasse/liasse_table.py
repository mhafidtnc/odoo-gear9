# -*- encoding: utf-8 -*-
from openerp.osv import fields, osv
from openerp import tools
import base64
import csv
import itertools
import datetime as dt
from lxml import etree

class credit_bail(osv.osv):
    _name = "credi.bail.erp"
    _columns = {
            'rubrique':fields.char('Rubriques'),
            'date_first_ech':fields.date('Date de la 1ere echeance'),
            'duree_contrat':fields.integer('Duree du contrat en mois'),
            'val_estime':fields.float('Valeur estimee du bien a la date du contrat'),
            'duree_theo':fields.integer('Duree theorique d amortissement du bien '),
            'cumul_prec':fields.float('Cumul des exercices precedents des redevances'),
            'montant_rev':fields.float('Montant de l exercice des redevances'),
            'rev_moins':fields.float('redevances restant a payer (A moins d un an)'),
            'rev_plus':fields.float('redevances restant a payer (A plus d un an)'),
            'prix_achat':fields.float('Prix d achat residuel en fin de contrat'),
            'observation':fields.char('Observations'),
            'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True)

    }
    
class pm_value(osv.osv):
        _name = "pm.value.erp"
        _columns = {
            'date_cession':fields.date('Date de cession ou de retrait'),
            'compte_princ':fields.float('Compte principal'),
            'montant_brut':fields.float('Montant brut'),
            'amort_cumul':fields.float('Amortissements cumules'),
            'val_net_amort':fields.float('Valeur nette d\'amortissements'),
            'prod_cess':fields.float('Produit de cession'),
            'plus_value':fields.float('Plus values'),
            'moins_value':fields.float('Moins values'),
            'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True)

    }
        
class titre_particip(osv.osv):
        _name = "titre.particip.erp"
        _columns = {
            'raison_soc':fields.char('Raison sociale de la société émettrice'),
            'sect_activity':fields.char('Secteur d\'activité'),
            'capit_social':fields.float('Capital social'),
            'particip_cap':fields.float('Participation au capital %'),
            'prix_global':fields.float('Prix d\'acquisition global '),
            'val_compt':fields.float('Valeur comptable nette'),
            'extr_date':fields.date('Extrait des derniers états de synthèse de la société émettrice(Date de clôture)'),
            'extr_situation':fields.float('Extrait des derniers états de synthèse de la société émettrice(Situation nette)'),
            'extr_resultat':fields.float('Extrait des derniers états de synthèse de la société émettrice(résultat net)'),
            'prod_inscrit':fields.float('Produits inscrits au C.P.C de l\'exercice '),
            'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True)

    }
        
class repart_cs(osv.osv):
        _name = "repart.cs.erp"
        _columns = {
            'name':fields.char('Nom, prenom ou raison sociale des principaux associes'),
            'id_fisc':fields.integer('IF'),
            'cin':fields.char('CIN'),
            'adress':fields.char('Adresse'),
            'number_prec':fields.integer('NOMBRE DE TITRES (Exercice precedent)'),
            'number_actual':fields.integer('NOMBRE DE TITRES (Exercice actual)'),
            'val_nom':fields.float('Valeur nominale de chaque action ou part sociale'),
            'val_sousc':fields.float('MONTANT DU CAPITAL(Souscrit)'),
            'val_appele':fields.float('MONTANT DU CAPITAL(Appele)'),
            'val_lib':fields.float('MONTANT DU CAPITAL(libere)'),
            'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True)

    }
            
class interets(osv.osv):
        _name = "interets.erp"
        _columns = {
            'name':fields.char('Nom, prenom ou raison sociale des principaux associes'),
            'adress':fields.char('Adresse'),
            'cin':fields.char('N C.I.N. ou Article I.S.'),
            'mont_pretl':fields.float('Montant du pret'),
            'date_pret':fields.date('Date du  pret '),
            'duree_mois':fields.integer('Duree du pret (en mois)'),
            'taux_inter':fields.float('Taux d\'interet'),
            'charge_global':fields.float('Charge financiere globale'),
            'remb_princ':fields.float('Remboursement exercices anterieurs(Principal)'),
            'remb_inter':fields.float('Remboursement exercices anterieurs(Interet)'),
            'remb_actual_princ':fields.float('Remboursement exercice actuel(Principal)'),
            'remb_actual_inter':fields.float('Remboursement exercice actuel(Interet)'),
            'observation':fields.char('Observations'),
            'type':fields.selection([('0','Associe'),('1','Tier')], 'A/T'),
            'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),

    } 
        
"""        
class interets_tier(osv.osv):
        _name = "interets.tier"
        _columns = {
            'name':fields.char('Nom, prénom ou raison sociale des principaux associés'),
            'adress':fields.char('Adresse'),
            'cin':fields.char('N° C.I.N. ou Article I.S.'),
            'mont_pretl':fields.float('Montant du prêt'),
            'date_pret':fields.date('Date du  prêt '),
            'duree_mois':fields.integer('Durée du prêt (en mois)'),
            'taux_inter':fields.float('Taux d\'intérêt'),
            'charge_global':fields.float('Charge financière globale'),
            'remb_princ':fields.float('Remboursement exercices antérieurs(Principal)'),
            'remb_inter':fields.float('Remboursement exercices antérieurs(Intérêt)'),
            'remb_actual_princ':fields.float('Remboursement exercice actuel(Principal)'),
            'remb_actual_inter':fields.float('Remboursement exercice actuel(Intérêt)'),
            'observation':fields.char('Observations'),
            'balance_id': fields.many2one('liasse.balance', 'liasse conf', ondelete='cascade', select=True),

    } 
 """       
class beaux(osv.osv):
        _name = "beaux.erp"
        _columns = {
            'nature':fields.char('Nature du bien loué'),
            'lieu':fields.char('Lieu de situation'),
            'name':fields.char('Nom et prénoms ou Raison sociale et adresse du propriétaire'),
            'date_loc':fields.date('Date de conclusion de l\'acte de location'),
            'mont_annuel':fields.float('Montant annuel de location'),
            'mont_loyer':fields.float('Montant du loyer compris dans les charges de l\'exercice'),
            'nature_bail':fields.char('Nature du contrat Bail-ordinaire'),
            'nature_periode':fields.char('Nature du contrat(Nème période)'),
            'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True)

    }
        
class passage(osv.osv):
        _name = 'passage.erp'
        _columns = {
            'name':fields.char(u'intitulé'),
            'montant1':fields.float('Montant +'),
            'montant2':fields.float('Montant -'),
            'type':fields.selection([('0','REINTEGRATIONS FISCALES COURANTES'),('1','REINTEGRATIONS FISCALES NON COURANTES'),
                                     ('2','DEDUCTIONS FISCALES COURANTES'),('3','DEDUCTIONS FISCALES NON COURANTES ')], 'Type'),
            'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),

    }
        
class dotation_amort(osv.osv):
        _name = "dotation.amort.erp"
        _columns = {
            'code0':fields.char('Immobilisations concernees'),
            'code1':fields.date('Date entree'),
            'code2':fields.float('Valeur a amortir (Prix dacquisition)'),
            'code3':fields.float(u'Valeur a amortir - Valeur comptable apres reevaluation'),
            'code4':fields.float('Amortissements anterieurs'),
            'code5':fields.float('Amortissements deduits du Benefice brut de l exercice (Taux)'),
            'code6':fields.integer('Amortissements deduits du Benefice brut de l exercice Duree'),
            'code7':fields.float('Amortissements deduits du Benefice brut de l exercice Amortissements normaux ou acceleres de l exercice'),
            'code8':fields.float('Total des amortissements a la fin de l exercice'),
            'code9':fields.char('observations'),
            'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True)

    }
        
class declaration(osv.osv):
    _name = 'declarationrvt.declaration.erp'
 
    _columns={
        'id_declar':fields.char('Nom de la declaration RVT:',required=True),
      #  'date_debut': fields.date('Date debut:',required=True),
       # 'date_fin': fields.date('Date fin:',required=True),
        'output':fields.binary('Declaration_xml:',readonly=True),
        'output_nom':fields.char('File name'),
        'ben_ids':fields.one2many('declarationrvt.beneficiaire.erp','ben_id','Beneficiaires',required=True),
        'date_id':fields.many2one('liasse.fiche.signalitique.erp','Fiche signalitique',required=True),    
              }
    _defaults = {
        'output_nom': 'declaration.xml',
    }
    _rec_name="id_declar"
    def generatexml(self,cr,uid,ids,context=None):
        for rec in self.browse(cr, uid, ids, context=context):
            id_fiscal = rec.id_declar
            # date_deb = rec.date_debut
            # date_f = rec.date_fin
            benids=rec.ben_ids
            dateid=rec.date_id
       
            doc = etree.Element( "DeclarationRVT", nsmap={})
            id = etree.SubElement( doc, "identifiantFiscal" )
            id.text = str(id_fiscal)
           
            dated = etree.SubElement( doc, "exerciceFiscalDu" )
            dated.text = str(dateid.date_start)
            datef = etree.SubElement( doc, "exerciceFiscalAu" )
            datef.text = str(dateid.date_end)
            
            for ben in benids:
                id_fis = ben.idfiscal
                honorair=ben.honoraires
                commission=ben.commissions
                rab=ben.rab
                r_soc=ben.raison_sociale
                adres=ben.adress
                numtp=ben.numtaxe
                numcnss=ben.num_cnss
                vv=ben.villeben
                prof=ben.professionben
                natio=ben.nationaliteben
                sommes=  etree.SubElement( doc, "sommesAllouees" )
                somme=  etree.SubElement( sommes, "sommesAllouee" )
                honoraire = etree.SubElement(somme, "honoraires" )
                honoraire.text  = str(honorair)
                commis = etree.SubElement(somme, "commissions" )
                commis.text  = str(commission)
                rabais = etree.SubElement(somme, "rabais" )
                rabais.text  = str(rab)
                benef= etree.SubElement(somme, "beneficiaire" )
                id_fisc= etree.SubElement(benef, "identifiantFiscal" )
                id_fisc.text = str(id_fis)
                rais_soc= etree.SubElement(benef, "raisonSociale" )
                rais_soc.text = str(r_soc)
                adr=etree.SubElement(benef, "adresse" )
                adr.text = str(adres)
                ntp=etree.SubElement(benef, "numeroTP" )
                ntp.text = str(numtp)
                ncnss=etree.SubElement(benef, "numCNSS" )
                ncnss.text = str(numcnss)
                v=etree.SubElement(benef, "ville" )
                v.text = str(vv)
                pro=etree.SubElement(benef, "profession" )
                pro.text = str(prof)
                nat=etree.SubElement(benef, "nationalite" )
                nat.text = str(natio)
               
               
        xml_data = "%s" % (
            etree.tostring(doc, pretty_print = True, xml_declaration = True, encoding='UTF-8')
                )
                 
        self.write(cr, uid, ids, {
            'output': base64.b64encode(xml_data.encode('utf8')),}, context=context) 
        return True 


class beneficiaire (osv.osv):
    _name = 'declarationrvt.beneficiaire.erp'

    _columns={
           
            'raison_sociale':fields.char('Nom et prénom ou raison_sociale',required=True),
            'idfiscal':fields.char('Identifiant fiscal',required=True),
            'numtaxe':fields.char('Numéro de la taxe professionnelle',required=True),
            'num_registre':fields.char('Numéro du registre de commerce'),
            'num_cnss':fields.char('Numéro affiliation CNSS',required=True),
            'adress':fields.char(' ',required=True),
            'villeben':fields.char('Ville',required=True),
            'telephone':fields.char('Téléphone'),
            'fax':fields.char('Fax'),
            'email':fields.char('Email'),
            'forme_juridique':fields.char('Forme juridique'),
            'professionben':fields.char('Profession ou activités exercées',required=True),              
            'honoraires':fields.float('honoraires',required=True),
            'commissions':fields.float('commissions',required=True),
            'rab':fields.float('rabais',required=True),  
            'nationaliteben':fields.char('Nationalité',required=True),
            'ben_id':fields. many2one('declarationrvt.declaration.erp','ben_ids','',required=True),
           
             }    
    
class actif(osv.osv):
    _name = "bilan.actif.fiscale.erp"
    _columns = {
        'lib':fields.char('Libelle', required=True),
        'code1':fields.float('Brut',required=True),
        'code2':fields.float('Amortissement et provisions',required=True),
        'code3':fields.float('Net',required=True),
        'code4':fields.float('Net Precedent',required=True),
        'code0':fields.many2one('liasse.bilan.actif.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', u'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }
    _defaults = {
        'type': '0',
    } 
    
class passif(osv.osv):
    _name = "bilan.passif.fiscale.erp"
    _columns = {
        'lib':fields.char('Libelle', required=True),
        'code1':fields.float('Exercice',required=True),
        'code2':fields.float('Exercice precedent',required=True),
        'code0':fields.many2one('liasse.bilan.passif.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', 'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }

    _defaults = {
        'type': '0',
    }
    
class cpc(osv.osv):
    _name = "cpc.fiscale.erp"
    _columns = {
        'lib':fields.char('Nature', required=True),
        'code1':fields.float('Operations propres à l\'exercice ',required=True),
        'code2':fields.float('Operations concernant les exercices precedents',required=True),
        'code3':fields.float('TOTAUX DE L\'EXERCICE ',required=True),
        'code4':fields.float('TOTAUX DE L\'EXERCICE PRECEDENT',required=True),
        'code0':fields.many2one('liasse.cpc.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', 'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }

    _defaults = {
        'type': '0',
    }
    
class det_cpc(osv.osv):
    _name = "det.cpc.fiscale.erp"
    _columns = {
        'poste':fields.char('Poste'),
        'lib':fields.char('Libelle', required=True),
        'code1':fields.float('Exercice',required=True),
        'code2':fields.float('Exercice precedent',required=True),
        'code0':fields.many2one('liasse.det.cpc.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', 'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }

    _defaults = {
        'type': '0',
    }
    
class tfr(osv.osv):
    _name = "tfr.fiscale.erp"
    _columns = {
        'lettre':fields.char('Lettre'),
        'num':fields.char('Num'),
        'op':fields.char('Operateur'),
        'lib':fields.char('Libelle', required=True),
        'code1':fields.float('Exercice',required=True),
        'code2':fields.float('Exercice precedent',required=True),
        'code0':fields.many2one('liasse.tfr.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', 'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }

    _defaults = {
        'type': '0',
    }
    
class caf(osv.osv):
    _name = "caf.fiscale.erp"
    _columns = {
        'lettre':fields.char('Lettre'),
        'num':fields.char('Num'),
        'op':fields.char('Operateur'),
        'lib':fields.char('Libelle', required=True),
        'code1':fields.float('Exercice',required=True),
        'code2':fields.float('Exercice precedent',required=True),
        'code0':fields.many2one('liasse.caf.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', 'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }

    _defaults = {
        'type': '0',
    }
    
class amort(osv.osv):
    _name = "amort.fiscale.erp"
    _columns = {
        'lib':fields.char('Nature', required=True),
        'code1':fields.float('Cumul debut',required=True),
        'code2':fields.float('Dotation',required=True),
        'code3':fields.float('Amortissement',required=True),
        'code4':fields.float('Cumul d amortissement',required=True),
        'code0':fields.many2one('liasse.amort.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', 'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }

    _defaults = {
        'type': '0',
    } 
   
class provision(osv.osv):
    _name = "provision.fiscale.erp"
    _columns = {
        'lib':fields.char('Nature', required=True),
        'code1':fields.float('Montant debut',required=True),
        'code2':fields.float('Dot exp',required=True),
        'code3':fields.float('Dot fin',required=True),
        'code4':fields.float('Dot nc',required=True),
        'code5':fields.float('Rep exp',required=True),
        'code6':fields.float('Rep fin',required=True),
        'code7':fields.float('Rep nc',required=True),
        'code8':fields.float('Montant fin',required=True),
        'code0':fields.many2one('liasse.provision.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', 'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }

    _defaults = {
        'type': '0',
    }

class liasse_stock(osv.osv):
    _name = "stock.fiscale.erp"
    _columns = {
        'lib':fields.char('STOCKS', required=True),
        'code1':fields.float('S.F. Montant Brut',required=True),
        'code2':fields.float(u'S.F. Provision pour depreciation',required=True),
        'code3':fields.float('S.F. Montant net',required=True),
        'code4':fields.float(' S.I. Montant brut',required=True),
        'code5':fields.float('S.I.Provision pour depreciation',required=True),
        'code6':fields.float('S.I.Montant net',required=True),
        'code0':fields.many2one('liasse.stock.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'code7':fields.float('Variation de stock en valeur',required=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', 'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }

    _defaults = {
        'type': '0',
    }
    
class fusion(osv.osv):
    _name = "fusion.fiscale.erp"
    _columns = {
        'lib':fields.char('Elements', required=True),
        'code1':fields.float('Valeur d aport',required=True),
        'code2':fields.float('Valeur nette comptable',required=True),
        'code3':fields.float('Plus-value constatee et differee',required=True),
        'code4':fields.float('Fraction de la plus-value rapportee aux l exercices anterieurs (cumul) en %',required=True),
        'code5':fields.float('Fraction de la plus-value rapportee a l exercices actuel en %',required=True),
        'code6':fields.float('Cumul des plus-values rapportees',required=True),
        'code7':fields.float('Solde des plus-values non imputees',required=True),
        'code8':fields.char('Observations'),
        'code0':fields.many2one('liasse.fusion.erp','codification'),
        'balance_id': fields.many2one('liasse.balance.erp', 'liasse conf', ondelete='cascade', select=True),
        'type':fields.selection([
            ('0', 'Normal'),
            ('1', 'Entete'),
            ('2', u'Total'),
            ],
            'Type',required=True),
        'sequence':fields.integer('Sequence',required=True),
    }

    _defaults = {
        'type': '0',
    }
    
    def onchange_code1(self, cr, uid, ids, code1,code0,context=None):
        fusion = self.pool.get('liasse.fusion.erp')
        code_conf = self.pool.get('liasse.code.erp')
        fusion_ids = fusion.browse(cr,uid,[code0])
        for row in fusion_ids:
            code_conf.write(cr,uid,row.code1.id,{'valeur':code1})
        return True
    
    def onchange_code2(self, cr, uid, ids, code2,code0,context=None):
        fusion = self.pool.get('liasse.fusion.erp')
        code_conf = self.pool.get('liasse.code.erp')
        fusion_ids = fusion.browse(cr,uid,[code0])
        for row in fusion_ids:
            code_conf.write(cr,uid,row.code2.id,{'valeur':code2})
        return True
    
    def onchange_code3(self, cr, uid, ids, code3,code0,context=None):
        fusion = self.pool.get('liasse.fusion.erp')
        code_conf = self.pool.get('liasse.code.erp')
        fusion_ids = fusion.browse(cr,uid,[code0])
        for row in fusion_ids:
            code_conf.write(cr,uid,row.code3.id,{'valeur':code3})
        return True
    
    def onchange_code4(self, cr, uid, ids, code4,code0,context=None):
        fusion = self.pool.get('liasse.fusion.erp')
        code_conf = self.pool.get('liasse.code.erp')
        fusion_ids = fusion.browse(cr,uid,[code0])
        for row in fusion_ids:
            code_conf.write(cr,uid,row.code4.id,{'valeur':code4})
        return True
    
    def onchange_code5(self, cr, uid, ids, code5,code0,context=None):
        fusion = self.pool.get('liasse.fusion.erp')
        code_conf = self.pool.get('liasse.code.erp')
        fusion_ids = fusion.browse(cr,uid,[code0])
        for row in fusion_ids:
            code_conf.write(cr,uid,row.code5.id,{'valeur':code5})
        return True
    
    def onchange_code6(self, cr, uid, ids, code6,code0,context=None):
        fusion = self.pool.get('liasse.fusion.erp')
        code_conf = self.pool.get('liasse.code.erp')
        fusion_ids = fusion.browse(cr,uid,[code0])
        for row in fusion_ids:
            code_conf.write(cr,uid,row.code6.id,{'valeur':code6})
        return True
    
    def onchange_code7(self, cr, uid, ids, code7,code0,context=None):
        fusion = self.pool.get('liasse.fusion.erp')
        code_conf = self.pool.get('liasse.code.erp')
        fusion_ids = fusion.browse(cr,uid,[code0])
        for row in fusion_ids:
            code_conf.write(cr,uid,row.code7.id,{'valeur':code7})
        return True
    
    def onchange_code8(self, cr, uid, ids, code8,code0,context=None):
        fusion = self.pool.get('liasse.fusion.erp')
        code_conf = self.pool.get('liasse.code.erp')
        fusion_ids = fusion.browse(cr,uid,[code0])
        for row in fusion_ids:
            code_conf.write(cr,uid,row.code8.id,{'valeur':code8})
        return True
    