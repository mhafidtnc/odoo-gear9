from openerp.osv import fields, osv

class check_list(osv.osv):
    
    def _is_errone(self, cr, uid, ids, field_name, arg, context=None):
        res={} 
        for record in self.browse(cr, uid, ids, context=context):
            res[record.id] = True
            if record.exercice == 0:
                res[record.id] = False
        return res
    
    def _is_erronep(self, cr, uid, ids, field_name, arg, context=None):
        res={} 
        for record in self.browse(cr, uid, ids, context=context):
            res[record.id] = True
            if record.exercicep == 0:
                res[record.id] = False
        return res     
    
    _name = "check.list.erp"
    _columns = {
        'etat':fields.char('Etat'),
        'name':fields.char('Nom'),
        'exercice':fields.float('Exercice N'),
        'exercice_errone':fields.function(_is_errone, string='Compte errone',type="boolean"),
        'exercicep':fields.float('Exercice N-1'),
        'exercicep_errone':fields.function(_is_erronep, string='Compte errone',type="boolean"),
    }
    
class check_rubrique(osv.osv):
    
    def _is_errone(self, cr, uid, ids, field_name, arg, context=None):
        res={} 
        for record in self.browse(cr, uid, ids, context=context):
            res[record.id] = True
            if (record.valeurbal - record.valeurres) == 0:
                res[record.id] = False
        return res 
    
    _name = "check.rubrique.erp"
    _columns = {
        'name':fields.char('Rubrique'),
        'compte':fields.char('Compte'),
        'valeurbal':fields.float('Balance'),
        'valeurres':fields.float('Resultat'),
        'errone':fields.function(_is_errone, string='Etat',type="boolean"),      
                }