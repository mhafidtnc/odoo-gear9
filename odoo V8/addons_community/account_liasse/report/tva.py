# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv
from openerp.addons.report_xls.utils import rowcol_to_cell


class tva(osv.osv_memory):
    
    _name = "tva.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'],num_format_str = '#,##0.00')
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [38,19,19,19,19]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        c_specs_list = []
        cell_style = xlwt.easyxf(self.xls_styles['xls_title']) 
        report_name =  'DETAIL DE LA TAXE SUR LA VALEUR AJOUTEE'
        c_specs = [
          ('1', 1, 0, 'text', data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'IF: '+data["if"] or ''),('report_name', 1, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', None)
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'Tableau n° 12'),('3', 1, 0, 'text', None),
            ('4', 1, 0, 'text',None),
            ('5', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','NATURE', None, self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'Solde au début de l\'exercice', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'Opérations comptables de l\'exercice', None, self.cell_style_header_tab2),
            ('3', 1, 0, 'text', 'Declarations T.V.A de l\'exercice', None, self.cell_style_header_tab2),
            ('4', 1, 0, 'text', 'Solde fin d\'exercice', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid, data):
        encour = self.pool.get('liasse.tva.erp')
        encour_edi_ids = encour.search(cr,uid,[],limit=1)
        encour_edi_obj = encour.browse(cr,uid,encour_edi_ids)
        #liasse_conf = self.pool.get('liasse.configuration')
        code_conf = self.pool.get('liasse.code.erp')
        balance = self.pool.get('liasse.balance.erp')
        balance_ids = balance.search(cr, uid, [('id','=',data["id"])])
        balance_obj = balance.browse(cr, uid, balance_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number
        for code in encour_edi_obj:
            if balance_obj:  
                c_specs = [
                           ('1', 1, 0, 'text','A. T.V.A. Facturée', None,self.cell_style_header),  
                           ('2', 1, 0, 'number', balance_obj[0].tva_facsd,None,self.cell_style_number_header),
                           ('3', 1, 0, 'number', balance_obj[0].tva_faco,None,self.cell_style_number_header),  
                           ('4', 1, 0, 'number', balance_obj[0].tva_facd,None,self.cell_style_number_header), 
                           ('5', 1, 0, 'number', balance_obj[0].tva_facsf,None,self.cell_style_number_header),                    
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','B. T.V.A. Récupérable', None,self.cell_style_header),  
                           ('2', 1, 0, 'number', balance_obj[0].tva_recsd,None,self.cell_style_number_header),
                           ('3', 1, 0, 'number', balance_obj[0].tva_reco,None,self.cell_style_number_header),  
                           ('4', 1, 0, 'number', balance_obj[0].tva_recd,None,self.cell_style_number_header), 
                           ('5', 1, 0, 'number', balance_obj[0].tva_recsf,None,self.cell_style_number_header),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Sur charges', None,self.cell_style_normal),  
                           ('2', 1, 0, 'number', balance_obj[0].tva_charsd,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].tva_charo,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].tva_chard,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].tva_charsf,None,self.cell_style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Sur immobilisations', None,self.cell_style_normal),  
                           ('2', 1, 0, 'number', balance_obj[0].tva_immosd,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].tva_immoo,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].tva_immod,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].tva_immosf,None,self.cell_style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','C. T.V.A. due ou crédit de T.V.A = (A - B )', None,self.cell_style_header), 
                           ('2', 1, 0, 'number', balance_obj[0].tva_totalsd,None,self.cell_style_number_header),
                           ('3', 1, 0, 'number', balance_obj[0].tva_totalo,None,self.cell_style_number_header),  
                           ('4', 1, 0, 'number', balance_obj[0].tva_totald,None,self.cell_style_number_header), 
                           ('5', 1, 0, 'number', balance_obj[0].tva_totalsf,None,self.cell_style_number_header),                        
                           ]
                c_sepcs_list.append(c_specs)                 
            # modification des code EDI
                code_conf.write(cr,uid,code.tva_facsd.id,{'valeur':balance_obj[0].tva_facsd})
                code_conf.write(cr,uid,code.tva_faco.id,{'valeur':balance_obj[0].tva_faco})
                code_conf.write(cr,uid,code.tva_facd.id,{'valeur':balance_obj[0].tva_facd})
                code_conf.write(cr,uid,code.tva_facsf.id,{'valeur':balance_obj[0].tva_facsf})
                code_conf.write(cr,uid,code.tva_recsd.id,{'valeur':balance_obj[0].tva_recsd}) 
                code_conf.write(cr,uid,code.tva_reco.id,{'valeur':balance_obj[0].tva_reco})
                code_conf.write(cr,uid,code.tva_recd.id,{'valeur':balance_obj[0].tva_recd})
                code_conf.write(cr,uid,code.tva_recsf.id,{'valeur':balance_obj[0].tva_recsf})
                code_conf.write(cr,uid,code.tva_charsd.id,{'valeur':balance_obj[0].tva_charsd})
                code_conf.write(cr,uid,code.tva_charo.id,{'valeur':balance_obj[0].tva_charo})             
                code_conf.write(cr,uid,code.tva_chard.id,{'valeur':balance_obj[0].tva_chard})
                code_conf.write(cr,uid,code.tva_charsf.id,{'valeur':balance_obj[0].tva_charsf})
                code_conf.write(cr,uid,code.tva_immosd.id,{'valeur':balance_obj[0].tva_immosd})                                  
                code_conf.write(cr,uid,code.tva_immoo.id,{'valeur':balance_obj[0].tva_immoo})
                code_conf.write(cr,uid,code.tva_immod.id,{'valeur':balance_obj[0].tva_immod})
                code_conf.write(cr,uid,code.tva_immosf.id,{'valeur':balance_obj[0].tva_immosf})
                code_conf.write(cr,uid,code.tva_totalsd.id,{'valeur':balance_obj[0].tva_totalsd})
                code_conf.write(cr,uid,code.tva_totalo.id,{'valeur':balance_obj[0].tva_totalo}) 
                code_conf.write(cr,uid,code.tva_totald.id,{'valeur':balance_obj[0].tva_totald})
                code_conf.write(cr,uid,code.tva_totalsf.id,{'valeur':balance_obj[0].tva_totalsf})
                
        return c_sepcs_list  
    