# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv


class dotation(osv.osv_memory):
    
    _name = "dotation.report.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'xls_neant': 'font: bold true, height 800;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top']
    cell_style_neant = xlwt.easyxf(xls_styles['xls_neant']+ xls_styles['center'] + xls_styles['borders_all'])
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'],num_format_str = '#,##0.00')
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [40,14,14,16,16,14,14,14,14,14]

    def generate_title(self,data):
        c_specs_list = []
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        cell_style = xlwt.easyxf(self.xls_styles['xls_title']) 
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        report_name =  'ETAT DES DOTATIONS AUX AMORTISSEMENTS RELATIFS AUX IMMOBILISATIONS'
        c_specs = [
           ('1', 1, 0, 'text', data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'IF: '+data["if"] or '')
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', None),('report_name', 7, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', None),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', None),('report_name', 1, 0, 'text', 'Montant',None,cell_style),
            ('2', 1, 0, 'number', data["montant_dot"],None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
                   ('0', 1, 0, 'text', 'Tableau n° 16'),('9', 1, 0, 'text', None),('2', 1, 0, 'text', None),('4', 1, 0, 'text', None),
                   ('5', 1, 0, 'text', None),('6', 1, 0, 'text', None),('7', 1, 0, 'text', None),('8', 1, 0, 'text', None),('1', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','Immobilisations concernées',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'Date d\'entrée', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'Valeur à amortir (Prix d\'acquisition)', None, self.cell_style_header_tab2),
            ('3', 1, 0, 'text', 'Valeur à amortir - Valeur comptable après réévaluation', None, self.cell_style_header_tab2),
            ('4', 1, 0, 'text', 'Amortissements antérieurs', None, self.cell_style_header_tab2),
            ('5', 1, 0, 'text', 'Amortissements déduits du Bénéfice brut de l\'exercice (Taux)', None, self.cell_style_header_tab2),
            ('6', 1, 0, 'text', 'Amortissements déduits du Bénéfice brut de l\'exercice Durée ', None, self.cell_style_header_tab2),
            ('7', 1, 0, 'text', 'Amortissements déduits du Bénéfice brut de l\'exercice Amortissements normaux ou accélérés de l\'exercice', None, self.cell_style_header_tab2),
            ('8', 1, 0, 'text', 'Total des amortissements à la fin de l\'exercice ', None, self.cell_style_header_tab2),
            ('9', 1, 0, 'text', 'Observations', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid,databal):
        pm_value_data = self.pool.get('dotation.amort.erp')
        pm_value_edi = self.pool.get('liasse.dotation.erp')
        liasse_balance = self.pool.get('liasse.balance.erp')
        liasse_balance_ids = liasse_balance.search(cr,uid,[("id","=",databal["id"])])
        liasse_balance_obj = liasse_balance.browse(cr,uid,liasse_balance_ids)
        pm_value_data_ids = pm_value_data.search(cr,uid,[('balance_id','=',databal["id"])])
        pm_value_edi_ids = pm_value_edi.search(cr,uid,[],limit=1)
        pm_value_data_obj = pm_value_data.browse(cr,uid,pm_value_data_ids)
        pm_value_edi_obj = pm_value_edi.browse(cr,uid,pm_value_edi_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number
        if pm_value_edi_obj:
            if pm_value_edi_obj.dotation_line_ids:
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code0.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code1.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code2.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code3.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code4.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code5.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code6.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code7.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code8.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(pm_value_edi_obj.dotation_line_ids[0].code9.id))
                       
        if not pm_value_data_obj:
            c_specs = [
                           ('1', 10, 0, 'text','NEANT', None,self.cell_style_neant),
                           ]
            c_sepcs_list.append(c_specs) 
        for data in pm_value_data_obj:
            data1=data.code0
            data2=data.code1
            data3=data.code2
            data4=data.code3
            data5=data.code4
            data6=data.code5
            data7=data.code6
            data8=data.code7
            data9=data.code8
            data10=data.code9                                                            
            c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'text', data2 ,None,style_text),
                           ('3', 1, 0, 'number', data3,None,style_number),
                           ('4', 1, 0, 'number', data4, None, style_number),
                           ('5', 1, 0, 'number', data5, None,style_number),
                           ('6', 1, 0, 'number', data6, None ,style_number),
                           ('7', 1, 0, 'number',data7, None,style_number),
                           ('8', 1, 0, 'number', data8 ,None,style_number),
                           ('9', 1, 0, 'number', data9 ,None,style_number),
                           ('10', 1, 0, 'text', data10 ,None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
        if liasse_balance_obj:
            if liasse_balance_obj.dotation_amort:
                c_specs = [
                           ('0', 1, 0, 'text','Total', None,self.cell_style_header),
                           ('01', 1, 0, 'text',None, None,self.cell_style_header),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].val_acq, None,self.cell_style_number_header),
                           ('2', 1, 0, 'number', liasse_balance_obj[0].val_compt ,None,self.cell_style_number_header),
                           ('3', 1, 0, 'number', liasse_balance_obj[0].amort_ant,None,self.cell_style_number_header),
                           ('4', 1, 0, 'number', liasse_balance_obj[0].amort_ded_et, None, self.cell_style_number_header),
                           ('02', 1, 0, 'text',None, None,self.cell_style_header),
                           ('5', 1, 0, 'number', liasse_balance_obj[0].amort_ded_e, None,self.cell_style_number_header),
                           ('6', 1, 0, 'number', liasse_balance_obj[0].amort_fe, None ,self.cell_style_number_header),
                           ]
                c_sepcs_list.append(c_specs)        
                                
        return c_sepcs_list  
    