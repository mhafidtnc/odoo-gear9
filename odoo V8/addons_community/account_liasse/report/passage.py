# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv


class interet(osv.osv_memory):
    
    _name = "passage.report.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'],num_format_str = '#,##0.00')
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [52,20,20]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style = xlwt.easyxf(self.xls_styles['xls_title'] +self.xls_styles['center']) 
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        report_name =  'PASSAGE DU RESULTAT NET COMPTABLE AU RESULTAT NET FISCAL'

        c_specs = [
           ('1', 1, 0, 'text', data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'IF: '+data["if"] or '')
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('report_name', 3, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text',None),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
                   ('0', 1, 0, 'text', 'Tableau n° 3'),('1', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','INTITULE',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'MONTANT', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'MONTANT', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid,data):
        ps_data = self.pool.get('passage.erp')
        liasse_balance = self.pool.get('liasse.balance.erp')
        liasse_balance_ids = liasse_balance.search(cr,uid,[('id','=',data["id"])])
        liasse_balance_obj = liasse_balance.browse(cr,uid,liasse_balance_ids)
        psrfc_data_ids = ps_data.search(cr,uid,[("type","=","0"),('balance_id','=',data["id"])])
        psrfnc_data_ids = ps_data.search(cr,uid,[("type","=","1"),('balance_id','=',data["id"])])
        psdfc_data_ids = ps_data.search(cr,uid,[("type","=","2"),('balance_id','=',data["id"])])
        psdfnc_data_ids = ps_data.search(cr,uid,[("type","=","3"),('balance_id','=',data["id"])])
        psrfc_data_obj = ps_data.browse(cr,uid,psrfc_data_ids)
        psrfnc_data_obj = ps_data.browse(cr,uid,psrfnc_data_ids)
        psdfc_data_obj = ps_data.browse(cr,uid,psdfc_data_ids)
        psdfnc_data_obj = ps_data.browse(cr,uid,psdfnc_data_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number

        if liasse_balance_obj:
            
            c_specs = [
                           ('1', 3, 0, 'text','RESULTAT NET COMPTABLE', None,self.cell_style_header),                           
                           ]
            c_sepcs_list.append(c_specs)  
            
            c_specs = [
                           ('0', 1, 0, 'text','Bénéfice net', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_benifice_p, None,style_number),
                           ('2', 1, 0, 'number',liasse_balance_obj[0].p_benifice_m, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            c_specs = [
                           ('0', 1, 0, 'text','Perte nette ', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_perte_p, None,style_number),
                           ('2', 1, 0, 'number',liasse_balance_obj[0].p_perte_m, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            

        if psrfc_data_obj:  
            c_specs = [
                           ('1', 3, 0, 'text','REINTEGRATIONS FISCALES COURANTES', None,self.cell_style_header),                           
                           ]             

        c_sepcs_list.append(c_specs) 
        for data in psrfc_data_obj:
            data1=data.name
            data2=data.montant1
            data3=data.montant2
                                                                         
            c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'number', data2 ,None,style_number),
                           ('3', 1, 0, 'number', data3,None,style_number),                          
                           ]
            c_sepcs_list.append(c_specs)
            
        if psrfnc_data_obj:
            c_specs = [
                           ('1',3 , 0, 'text','REINTEGRATIONS FISCALES NON COURANTES', None,self.cell_style_header),                           
                           ]   
            
            c_sepcs_list.append(c_specs)
            for data in psrfnc_data_obj:
                data1=data.name
                data2=data.montant1
                data3=data.montant2
                                                      
                c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'number', data2 ,None,style_number),
                           ('3', 1, 0, 'number', data3,None,style_number),                           
                           ]
                c_sepcs_list.append(c_specs)           
    
        if psdfc_data_obj:
            c_specs = [
                           ('1', 3, 0, 'text','DEDUCTIONS FISCALES COURANTES', None,self.cell_style_header),                           
                           ]

            c_sepcs_list.append(c_specs)
            for data in psdfc_data_obj:
                data1=data.name
                data2=data.montant1
                data3=data.montant2
                                                                              
                c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'number', data2 ,None,style_number),
                           ('3', 1, 0, 'number', data3,None,style_number),                           
                           ]
                c_sepcs_list.append(c_specs)           

        
        if psdfnc_data_obj:   
            c_specs = [
                           ('1', 3, 0, 'text','DEDUCTIONS FISCALES NON COURANTES', None,self.cell_style_header),                           
                           ]
            c_sepcs_list.append(c_specs) 
            for data in psdfnc_data_obj:
                data1=data.name
                data2=data.montant1
                data3=data.montant2
                                                                                 
                c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'number', data2 ,None,style_number),
                           ('3', 1, 0, 'number', data3,None,style_number),                           
                           ]
                c_sepcs_list.append(c_specs)           

            
        if liasse_balance_obj:
            c_specs = [
                           ('0', 1, 0, 'text','Total', None,self.cell_style_header),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_total_montantp, None,self.cell_style_number_header),
                           ('2', 1, 0, 'number',liasse_balance_obj[0].p_total_montantm, None,self.cell_style_number_header),
                           ]
            c_sepcs_list.append(c_specs)

            c_specs = [
                           ('1', 3, 0, 'text','RESULTAT BRUT FISCAL', None,self.cell_style_header),                           
                           ]
            c_sepcs_list.append(c_specs) 
            
            c_specs = [
                           ('0', 1, 0, 'text','Bénéfice brut si T1> T2 (A)', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_benificebp, None,style_number),
                           ('2', 1, 0, 'number',liasse_balance_obj[0].p_benificebm, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)

            c_specs = [
                           ('0', 1, 0, 'text','Déficit brut fiscal si T2> T1 (B)', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_deficitfp, None,style_number),
                           ('2', 1, 0, 'number',liasse_balance_obj[0].p_deficitfm, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('1', 3, 0, 'text','REPORTS DEFICITAIRES IMPUTES', None,self.cell_style_header),                           
                           ]
            c_sepcs_list.append(c_specs) 
            
            c_specs = [
                           ('0', 1, 0, 'text','Exercice n-4', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_exo4p, None,style_number),
                           ('2', 1, 0, 'text',None, None,style_number),
                           #('2', 1, 0, 'number',liasse_balance_obj[0].p_exo4m, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('0', 1, 0, 'text','Exercice n-3', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_exo3p, None,style_number),
                           ('2', 1, 0, 'text',None, None,style_number),
                           #('2', 1, 0, 'number',liasse_balance_obj[0].p_exo3m, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('0', 1, 0, 'text','Exercice n-2', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_exo2p, None,style_number),
                           ('2', 1, 0, 'text',None, None,style_number),
                           #('2', 1, 0, 'number',liasse_balance_obj[0].p_exo2m, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('0', 1, 0, 'text','Exercice n-1', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_exo1p, None,style_number),
                           ('2', 1, 0, 'text',None, None,style_number),
                           #('2', 1, 0, 'number',liasse_balance_obj[0].p_exo1m, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('1', 3, 0, 'text','RESULTAT NET FISCAL', None,self.cell_style_header),                           
                           ]
            c_sepcs_list.append(c_specs)

            c_specs = [
                           ('0', 1, 0, 'text','Bénéfice net fiscal (A-C)', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_benificenfp, None,style_number),
                           ('2', 1, 0, 'number',liasse_balance_obj[0].p_benificenfm, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('0', 1, 0, 'text','ou déficit net fiscal (B)', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_deficitnfp, None,style_number),
                           ('2', 1, 0, 'number',liasse_balance_obj[0].p_deficitnfm, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('1', 3, 0, 'text','CUMUL DES AMORTISSEMENTS FISCALEMENT DIFFERES', None,self.cell_style_header),                           
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('0', 1, 0, 'text','CUMUL DES AMORTISSEMENTS FISCALEMENT DIFFERES', None,style_text),
                           ('1', 1, 0, 'text',None, None,style_number),
                           ('2', 1, 0, 'number',liasse_balance_obj[0].p_cumulafdm, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('1', 3, 0, 'text','CUMUL DES DEFICITS FISCAUX RESTANT A REPORTER', None,self.cell_style_header),                           
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('0', 1, 0, 'text',' Exercice n-4', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_exo4cumulp, None,style_number),
                           ('2', 1, 0, 'text',None, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)

            c_specs = [
                           ('0', 1, 0, 'text',' Exercice n-3', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_exo3cumulp, None,style_number),
                           ('2', 1, 0, 'text',None, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('0', 1, 0, 'text',' Exercice n-2', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_exo2cumulp, None,style_number),
                           ('2', 1, 0, 'text',None, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
            c_specs = [
                           ('0', 1, 0, 'text',' Exercice n-1', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].p_exo1cumulp, None,style_number),
                           ('2', 1, 0, 'text',None, None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
        return c_sepcs_list  
    