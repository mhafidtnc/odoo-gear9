# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2013 Noviat nv/sa (www.noviat.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import xlwt
from openerp import pooler
import time
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls

class liasse_normal_xls_parser(report_sxw.rml_parse):

    def __init__(self, cursor, uid, name, context):
        super(liasse_normal_xls_parser, self).__init__(cursor, uid, name, context=context)
        self.pool = pooler.get_pool(self.cr.dbname)
        self.cursor = self.cr

        company = self.pool.get('res.users').browse(self.cr, uid, uid, context=context).company_id

        self.localcontext.update({
            'cr': cursor,
            'uid': uid,
            'report_name': 'Bilan actif',
            
        })

    def set_context(self, objects, data, ids, report_type=None):
        return super(liasse_normal_xls_parser, self).set_context(objects, data, ids,
                                                            report_type=report_type)

report_sxw.report_sxw('report.liasse.normal.erp', 'liasse.balance.erp',
    'addons/liasse_normal/report/bilan_actif.rml',
    parser=liasse_normal_xls_parser, header=False)
        
class liasse_normal_xls(report_xls):
    
         
    def generate_xls_report(self, _p, _xs, data, objects, wb):

            #""" Actiiiif """
        ac = self.pool.get('actif.erp')
        ws,row_pos = self.write_sheet(wb, ' T1 Actif', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" Passif """
        ac = self.pool.get('passif.erp')
        ws,row_pos = self.write_sheet(wb, 'T1 Passif', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" cpc """
        ac = self.pool.get('cpc.report.erp')
        ws,row_pos = self.write_sheet(wb, 'T2 CPC', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
        #passage
        ac = self.pool.get('passage.report.erp')
        ws,row_pos = self.write_sheet(wb, 'T3 Passage', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" immo """
        ac = self.pool.get('immo.erp')
        ws,row_pos = self.write_sheet(wb, 'T4 Immo', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" affect """
        ac = self.pool.get('affect.erp')
        ws,row_pos = self.write_sheet(wb, 'T14 AFFECT', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" esg """
        """ TFR """
        ac = self.pool.get('tfr.erp')
        ws,row_pos = self.write_sheet(wb, 'T5 ESG', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        row_pos +=1
        """ CAF """
        ac = self.pool.get('caf.erp')
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" det cpc """
        ac = self.pool.get('det.cpc.erp')
        ws,row_pos = self.write_sheet(wb, 'T6 DET CPC', ac.column_sizes)
        c_specs_list = ac.generate_title(_p.cr,_p.uid,data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" Credit bail """
        ac = self.pool.get('credit.bail.report.erp')
        ws,row_pos = self.write_sheet(wb, 'T7 CREDITBAIL', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" amort """
        ac = self.pool.get('amort.erp')
        ws,row_pos = self.write_sheet(wb, 'T8 AMT', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" Provision """
        ac = self.pool.get('prov.erp')
        ws,row_pos = self.write_sheet(wb, 'T9 PROV', ac.column_sizes)
        c_specs_list = ac.generate_title(_p.cr,_p.uid,data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" Pm value  """
        ac = self.pool.get('pm.value.report.erp')
        ws,row_pos = self.write_sheet(wb, 'T10 PM VALUE', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" titre particip  """
        ac = self.pool.get('titre.particip.report.erp')
        ws,row_pos = self.write_sheet(wb, 'T11 PARTICP', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" tva """
        ac = self.pool.get('tva.erp')
        ws,row_pos = self.write_sheet(wb, 'T12 TVA', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" repart cs  """
        ac = self.pool.get('repart.cs.report.erp')
        ws,row_pos = self.write_sheet(wb, 'T13 REPART CS', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header(_p.cr, _p.uid,data)
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" encouragement """
        ac = self.pool.get('encouragement.erp')
        ws,row_pos = self.write_sheet(wb, 'T15 ENCOURG', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" dotation """
        ac = self.pool.get('dotation.report.erp')
        ws,row_pos = self.write_sheet(wb, 'T16 Dotation', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" fusion """
        ac = self.pool.get('fusion.erp')
        ws,row_pos = self.write_sheet(wb, 'T17 FUSION', ac.column_sizes)
        c_specs_list = ac.generate_title(_p.cr,_p.uid,data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" interet  """
        ac = self.pool.get('interet.report.erp')
        ws,row_pos = self.write_sheet(wb, 'T18 Interet', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
        
            #beaux  
        ac = self.pool.get('beaux.report.erp')
        ws,row_pos = self.write_sheet(wb, 'T19 BEAUX', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            #""" stock """
        ac = self.pool.get('stock.erp')
        ws,row_pos = self.write_sheet(wb, 'T20 STOCK', ac.column_sizes)
        c_specs_list = ac.generate_title(data)
        row_pos = self.write_title(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list = ac.generate_header()
        row_pos = self.write_header(ws, _xs, _p, data, row_pos, c_specs_list)
        c_specs_list=ac.generate_body(_p.cr, _p.uid,data)
        row_pos = self.write_body(ws, _xs, _p, data, row_pos, c_specs_list)
        
            
   
    ## write sheet     
    def write_sheet(self,wb, name, c_sizes):
        ws = wb.add_sheet(name)
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0 # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0
        
        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']
        c_specs = [('empty%s'%i, 1, c_sizes[i], 'text', None) for i in range(0,len(c_sizes))]
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, set_column_size=True)
        return ws,row_pos
        
    
    ## write Title
    def write_title(self,ws, _xs, _p, data, row_pos,c_specs_list):
        cell_format = _xs['borders_all'] + _xs['wrap'] + _xs['top']
        cell_style = xlwt.easyxf(cell_format)
        for c_specs in c_specs_list:
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style)
        return row_pos
    
    ## Write header
    def write_header(self,ws, _xs, _p, data, row_pos,c_specs_list):
        cell_format = _xs['borders_all'] + _xs['wrap'] + _xs['top']
        cell_style = xlwt.easyxf(cell_format)
        for c_specs in c_specs_list:
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style)
        return row_pos
    
    ## write body
    def write_body(self,ws, _xs, _p, data, row_pos,c_specs_list):
        cell_format = _xs['borders_all'] + _xs['wrap'] + _xs['top']
        cell_style = xlwt.easyxf(cell_format)
        for c_specs in c_specs_list:
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style)
        return row_pos

liasse_normal_xls('report.liasse.normal.erp.xls', 'liasse.balance.erp',
    parser=liasse_normal_xls_parser)
        