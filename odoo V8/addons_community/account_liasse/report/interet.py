# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv


class interet(osv.osv_memory):
    
    _name = "interet.report.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'xls_neant': 'font: bold true, height 800;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top']
    cell_style_neant = xlwt.easyxf(xls_styles['xls_neant']+ xls_styles['center'] + xls_styles['borders_all']) 
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'],num_format_str = '#,##0.00')
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [15,15,15,15,15,15,15,15,15,15,15,15,15]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        cell_style = xlwt.easyxf(self.xls_styles['xls_title']) 
        report_name =  'ETAT DES INTERETS DES EMPRUNTS CONTRACTES AUPRES DES ASSOCIES '
        report_name1='ET DES TIERS AUTRES QUE LES ORGANISMES DE BANQUE OU DE CREDIT'
        c_specs = [
           ('1', 1, 0, 'text', data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'IF: '+data["if"] or '')
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('0', 1, 0, 'text', None)
        ]   
        c_specs = [
            ('0', 1, 0, 'text', None),('1', 1, 0, 'text', None),('report_name', 8, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('0', 1, 0, 'text', None),('1', 1, 0, 'text', None),('report_name', 8, 0, 'text', report_name1,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text',None),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
                   ('0', 1, 0, 'text', 'Tableau n° 18'),('2', 1, 0, 'text', None),('3', 1, 0, 'text', None),('7', 1, 0, 'text', None),
                   ('4', 1, 0, 'text', None),('5', 1, 0, 'text', None),('6', 1, 0, 'text', None),('10', 1, 0, 'text', None),
                   ('8', 1, 0, 'text',None),('9', 1, 0, 'text', None),('11', 1, 0, 'text', None),('1', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','Nom,  prénomou  raison  sociale',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'Adresse', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'N° C.I.N.  ou article  I.S.', None, self.cell_style_header_tab2),
            ('3', 1, 0, 'text', 'Montant du prêt', None, self.cell_style_header_tab2),
            ('4', 1, 0, 'text', 'Date du  prêt', None, self.cell_style_header_tab2),
            ('5', 1, 0, 'text', 'Durée du prêt en mois', None, self.cell_style_header_tab2),
            ('6', 1, 0, 'text', 'Taux d\'intérêt', None, self.cell_style_header_tab2),
            ('7', 1, 0, 'text', 'Charge financière globale', None, self.cell_style_header_tab2),
            ('8', 2, 0, 'text', 'Remboursement exercices antérieurs', None, self.cell_style_header_tab2),
            ('9', 2, 0, 'text', 'Remboursement exercice actuel', None, self.cell_style_header_tab2),
            ('10', 1, 0, 'text', 'Observations', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        
        c_specs = [
            ('0', 1, 0, 'text',None,None,self.cell_style_header_tab),
            ('1', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('2', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('3', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('4', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('5', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('6', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('7', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('8', 1, 0, 'text', 'Principal', None, self.cell_style_header_tab),
            ('9', 1, 0, 'text', 'Intérêt', None, self.cell_style_header_tab),
            ('10', 1, 0, 'text', 'Principal', None, self.cell_style_header_tab),
            ('11', 1, 0, 'text', 'Intérêt', None, self.cell_style_header_tab),
            ('12', 1, 0, 'text', None, None, self.cell_style_header_tab),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid,data):
        in_data = self.pool.get('interets.erp')
        liasse_balance = self.pool.get('liasse.balance.erp')
        liasse_balance_ids = liasse_balance.search(cr,uid,[('id','=',data["id"])])
        liasse_balance_obj = liasse_balance.browse(cr,uid,liasse_balance_ids)
        ina_data_ids = in_data.search(cr,uid,[("type","=","0"),('balance_id','=',data["id"])])
        int_data_ids = in_data.search(cr,uid,[("type","=","1"),('balance_id','=',data["id"])])
        ina_data_obj = in_data.browse(cr,uid,ina_data_ids)
        int_data_obj = in_data.browse(cr,uid,int_data_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number
        
        c_specs = [
                           ('1', 1, 0, 'text','Associés', None,self.cell_style_header),                           
                           ]
        c_sepcs_list.append(c_specs)  
        if not ina_data_obj:
            c_specs = [
                           ('1', 13, 0, 'text','NEANT', None,self.cell_style_neant),
                           ]
            c_sepcs_list.append(c_specs) 
        for data in ina_data_obj:
            data1=data.name
            data2=data.adress
            data3=data.cin
            data4=data.mont_pretl
            data5=data.date_pret
            data6=data.duree_mois
            data7=data.taux_inter
            data8=data.charge_global
            data9=data.remb_princ
            data10=data.remb_inter
            data11=data.remb_actual_princ
            data12=data.remb_actual_inter
            data13=data.observation  
                                                           
            c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'text', data2 ,None,style_text),
                           ('3', 1, 0, 'text', data3,None,style_text),
                           ('4', 1, 0, 'number', data4, None, style_number),
                           ('5', 1, 0, 'text', data5, None,style_text),
                           ('6', 1, 0, 'number', data6, None ,style_number),
                           ('7', 1, 0, 'number',data7, None,style_number),
                           ('8', 1, 0, 'number', data8 ,None,style_number),
                           ('9', 1, 0, 'number', data9, None ,style_number),
                           ('10', 1, 0, 'number',data10, None,style_number),
                           ('11', 1, 0, 'number', data11 ,None,style_number),
                           ('12', 1, 0, 'number',data12, None,style_number),
                           ('13', 1, 0, 'text', data13 ,None,style_number),                           
                           ]
            c_sepcs_list.append(c_specs)
            
        c_specs = [
                           ('1', 1, 0, 'text','Tiers', None,self.cell_style_header),                           
                           ]
        c_sepcs_list.append(c_specs) 
        if not int_data_obj:
            c_specs = [
                           ('1', 13, 0, 'text','NEANT', None,self.cell_style_neant),
                           ]
            c_sepcs_list.append(c_specs) 

        for data in int_data_obj:
            data1=data.name
            data2=data.adress
            data3=data.cin
            data4=data.mont_pretl
            data5=data.date_pret
            data6=data.duree_mois
            data7=data.taux_inter
            data8=data.charge_global
            data9=data.remb_princ
            data10=data.remb_inter
            data11=data.remb_actual_princ
            data12=data.remb_actual_inter
            data13=data.observation
                                                                 
            c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'text', data2 ,None,style_text),
                           ('3', 1, 0, 'text', data3,None,style_text),
                           ('4', 1, 0, 'number', data4, None, style_number),
                           ('5', 1, 0, 'text', data5, None,style_text),
                           ('6', 1, 0, 'number', data6, None ,style_number),
                           ('7', 1, 0, 'number',data7, None,style_number),
                           ('8', 1, 0, 'number', data8 ,None,style_number),
                           ('9', 1, 0, 'number', data9, None ,style_number),
                           ('10', 1, 0, 'number',data10, None,style_number),
                           ('11', 1, 0, 'number', data11 ,None,style_number),
                           ('12', 1, 0, 'number',data12, None,style_number),
                           ('13', 1, 0, 'text', data13 ,None,style_number),                           
                           ]
            c_sepcs_list.append(c_specs)           

            # modification des code EDI
            
        if liasse_balance_obj:
            if liasse_balance_obj.interets_associe or liasse_balance_obj.interets_tier:
                c_specs = [
                           ('0', 1, 0, 'text','Total', None,style_text),
                           ('1', 1, 0, 'text',None, None,style_text),
                           ('2', 1, 0, 'text',None, None,style_text),
                           ('3', 1, 0, 'number',liasse_balance_obj[0].in_mont_pretl, None,style_number),
                           ('4', 1, 0, 'text',None, None,style_text),
                           ('5', 1, 0, 'text',None, None,style_text),
                           ('6', 1, 0, 'text',None, None,style_text),
                           ('7', 1, 0, 'number', liasse_balance_obj[0].in_charge_global ,None,style_number),
                           ('8', 1, 0, 'number', liasse_balance_obj[0].in_remb_princ,None,style_number),
                           ('9', 1, 0, 'number', liasse_balance_obj[0].in_remb_inter, None, style_number),
                           ('10', 1, 0, 'number', liasse_balance_obj[0].in_remb_actual_princ, None,style_number),
                           ('11', 1, 0, 'number', liasse_balance_obj[0].in_remb_actual_inter, None ,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                                
        return c_sepcs_list  
    