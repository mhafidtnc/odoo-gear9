# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv


class pm_value(osv.osv_memory):
    
    _name = "pm.value.report.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'xls_neant': 'font: bold true, height 800;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_neant = xlwt.easyxf(xls_styles['xls_neant']+ xls_styles['center'] + xls_styles['borders_all'])
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'],num_format_str = '#,##0.00')
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [14,14,14,16,16,14,14,14]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        cell_style = xlwt.easyxf(self.xls_styles['xls_title'] ) 
        report_name =  'TABLEAU DES PLUS OU MOINS VALUES SUR CESSIONS OU RETRAITS'
        c_specs = [
            ('0', 4, 0, 'text',data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('0', 2, 0, 'text', 'IF: '+data["if"] or ''),('1', 1, 0, 'text', None),
        ]
        c_specs_list.append(c_specs)

        c_specs = [
            ('1', 1, 0, 'text', None),('report_name', 1, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        
        c_specs = [
            ('1', 1, 0, 'text', None)
        ]
        c_specs_list.append(c_specs)

        c_specs = [
            ('0', 1, 0, 'text', 'Tableau n° 10'),('1', 1, 0, 'text', None),('2', 1, 0, 'text', None),('3', 1, 0, 'text', None),
            ('4', 1, 0, 'text', None),('6', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','Date de cession ou de retrait',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'Compte principal', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'Montant brut', None, self.cell_style_header_tab2),
            ('3', 1, 0, 'text', 'Amortissements cumulés', None, self.cell_style_header_tab2),
            ('4', 1, 0, 'text', 'Valeur nette d\'amortissements', None, self.cell_style_header_tab2),
            ('5', 1, 0, 'text', 'Produit de cession', None, self.cell_style_header_tab2),
            ('6', 1, 0, 'text', 'Plus values', None, self.cell_style_header_tab2),
            ('7', 1, 0, 'text', 'Moins values', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid,databal):
        pm_value_data = self.pool.get('pm.value.erp')
        liasse_balance = self.pool.get('liasse.balance.erp')
        liasse_balance_ids = liasse_balance.search(cr,uid,[("id","=",databal["id"])])
        liasse_balance_obj = liasse_balance.browse(cr,uid,liasse_balance_ids)
        pm_value_data_ids = pm_value_data.search(cr,uid,[('balance_id','=',databal["id"])])
        pm_value_data_obj = pm_value_data.browse(cr,uid,pm_value_data_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number
        if not pm_value_data_obj:
            c_specs = [
                           ('1', 8, 0, 'text','NEANT', None,self.cell_style_neant),
                           ]
            c_sepcs_list.append(c_specs) 
        for data in pm_value_data_obj:
            data1=data.date_cession
            data2=data.compte_princ
            data3=data.montant_brut
            data4=data.amort_cumul
            data5=data.val_net_amort
            data6=data.prod_cess
            data7=data.plus_value
            data8=data.moins_value                                                            
            c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'number', data2 ,None,style_text),
                           ('3', 1, 0, 'number', data3,None,style_number),
                           ('4', 1, 0, 'number', data4, None, style_number),
                           ('5', 1, 0, 'number', data5, None,style_number),
                           ('6', 1, 0, 'number', data6, None ,style_number),
                           ('7', 1, 0, 'number',data7, None,style_number),
                           ('8', 1, 0, 'number', data8 ,None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            # modification des code EDI
            
        if liasse_balance_obj:
            if liasse_balance_obj.pm_value:
                c_specs = [
                           ('0', 1, 0, 'text','Total', None,style_text),
                           ('1', 1, 0, 'number',liasse_balance_obj[0].pm_compte_princ, None,style_text),
                           ('2', 1, 0, 'number', liasse_balance_obj[0].pm_montant_brut ,None,style_text),
                           ('3', 1, 0, 'number', liasse_balance_obj[0].pm_amort_cumul,None,style_number),
                           ('4', 1, 0, 'number', liasse_balance_obj[0].pm_val_net_amort, None, style_number),
                           ('5', 1, 0, 'number', liasse_balance_obj[0].pm_prod_cess, None,style_number),
                           ('6', 1, 0, 'number', liasse_balance_obj[0].pm_plus_value, None ,style_number),
                           ('7', 1, 0, 'number',liasse_balance_obj[0].pm_moins_value, None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                                
        return c_sepcs_list  
    