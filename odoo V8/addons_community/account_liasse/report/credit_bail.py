# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv
from openerp.addons.report_xls.utils import rowcol_to_cell


class credit_bail(osv.osv_memory):
    
    _name = "credit.bail.report.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'xls_neant': 'font: bold true, height 800;',
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_style_neant = xlwt.easyxf(xls_styles['xls_neant']+ xls_styles['center'] + xls_styles['borders_all'])
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'])
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [12,12,12,12,12,12,12,12,12,12,15]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        c_specs_list = []
        cell_style = xlwt.easyxf(self.xls_styles['xls_title']) 
        report_name =  'TABLEAU DES BIENS EN CREDIT-BAIL'
        c_specs = [
            ('0', 2, 0, 'text',data["company"]),('2', 1, 0, 'text', None)
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('0', 1, 0, 'text', 'IF: '+data["if"] or '')
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('0', 2, 0, 'text',None),('2', 1, 0, 'text', None),('report_name', 1, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', None),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('0', 2, 0, 'text', 'Tableau n° 7'),('1', 1, 0, 'text', None),('2', 1, 0, 'text', None),('3', 1, 0, 'text', None),('4', 1, 0, 'text', None)
            ,('5', 1, 0, 'text', None),('6', 1, 0, 'text', None),('7', 1, 0, 'text', None)
            ,('9',1 , 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','Rubriques',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'Date de la 1ère échéance', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'Durée du contrat en mois', None, self.cell_style_header_tab2),
            ('3', 1, 0, 'text', 'Valeur estimée du bien en date du contrat', None, self.cell_style_header_tab2),
            ('4', 1, 0, 'text', 'Durée théorique d\'amortissement du bien', None, self.cell_style_header_tab2),
            ('5', 1, 0, 'text', 'Cumul des exercices précédents des redevances', None, self.cell_style_header_tab2),
            ('6', 1, 0, 'text', 'Montant de  l\'exercice des redevances', None, self.cell_style_header_tab2),
            ('7', 2, 0, 'text', 'Redevances restant à payer', None, self.cell_style_header_tab2),
            ('8', 1, 0, 'text', 'Prix d\'achats résiduel en fin de contrat', None, self.cell_style_header_tab2),
            ('9', 1, 0, 'text', 'Observations\11', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('0', 1, 0, 'text',None,None,self.cell_style_header_tab),
            ('1', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('2', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('3', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('4', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('5', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('6', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('7', 1, 0, 'text', 'A moins d\'un an', None, self.cell_style_header_tab),
            ('8', 1, 0, 'text', 'A plus d\'un an', None, self.cell_style_header_tab),
            ('9', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('10', 1, 0, 'text', None, None, self.cell_style_header_tab),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid,data):
        credit_bail_data = self.pool.get('credi.bail.erp')
        credit_bail_edi = self.pool.get('liasse.credit.bail.erp')
        credit_bail_data_ids = credit_bail_data.search(cr,uid,[('balance_id','=',data["id"])])
        credit_bail_edi_ids = credit_bail_edi.search(cr,uid,[],limit=1)
        credit_bail_data_obj = credit_bail_data.browse(cr,uid,credit_bail_data_ids)
        credit_bail_edi_obj = credit_bail_edi.browse(cr,uid,credit_bail_edi_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number
        if credit_bail_edi_obj:
            if credit_bail_edi_obj.credit_bail_ids:
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code0.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code1.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code2.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code3.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code4.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code5.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code6.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code7.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code8.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code9.id))
                cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code10.id))
                if credit_bail_edi_obj.credit_bail_ids[0].type in ['1']:
                    style_text = self.cell_style_header
                    style_number = self.cell_style_number_header
                elif credit_bail_edi_obj.credit_bail_ids[0].type in ['2']:
                    style_text = self.cell_style_total
                    style_number = self.cell_style_number_header
                else:
                    style_text = self.cell_style_normal
                    style_number = self.cell_style_number 

        if not credit_bail_data_obj:
            c_specs = [
                           ('1', 11, 0, 'text','NEANT', None,self.cell_style_neant),
                           ]
            c_sepcs_list.append(c_specs) 
        for data in credit_bail_data_obj:
            data1=data.rubrique
            data2=data.date_first_ech
            data3=data.duree_contrat
            data4=data.val_estime
            data5=data.duree_theo
            data6=data.cumul_prec
            data7=data.montant_rev
            data8=data.rev_moins
            data9=data.rev_plus
            data10=data.prix_achat
            data11=data.observation
                                                            
            c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'text', data2 ,None,style_text),
                           ('3', 1, 0, 'number', data3,None,style_number),
                           ('4', 1, 0, 'number', data4, None, style_number),
                           ('5', 1, 0, 'number', data5, None,style_number),
                           ('6', 1, 0, 'number', data6, None ,style_number),
                           ('7', 1, 0, 'number',data7, None,style_number),
                           ('8', 1, 0, 'number', data8 ,None,style_number),
                           ('9', 1, 0, 'number', data9,None,style_number),
                           ('10', 1, 0, 'number', data10, None, style_number),
                           ('11', 1, 0, 'text', data11, None,style_text),
                           ]
            c_sepcs_list.append(c_specs)
           
                                
        return c_sepcs_list  
    