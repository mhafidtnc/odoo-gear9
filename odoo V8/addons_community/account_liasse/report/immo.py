# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv
from openerp.addons.report_xls.utils import rowcol_to_cell


class immo(osv.osv_memory):
    
    _name = "immo.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'],num_format_str = '#,##0.00')
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [40,22,18,18,18,18,18,18,18]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style = xlwt.easyxf(self.xls_styles['xls_title']) 
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        report_name =  'TABLEAU DES IMMOBILISATIONS AUTRES QUE FINANCIERES'
        c_specs = [
          ('1', 1, 0, 'text', data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'IF: '+data["if"] or ''),('3', 1, 0, 'text', None),('report_name', 1, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', None)
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'Tableau n° 4'),('3', 1, 0, 'text', None),
            ('4', 1, 0, 'text',None),('40', 1, 0, 'text',None),('41', 1, 0, 'text',None),
            ('42', 1, 0, 'text',None),
            ('5', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','Nature',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'Montant brut début exercice', None, self.cell_style_header_tab2),
            ('2', 3, 0, 'text', 'AUGMENTATION', None, self.cell_style_header_tab2),
            ('3',3, 0, 'text', 'DIMINUTION', None, self.cell_style_header_tab2),
            ('4',1, 0, 'text', 'Montant brut fin exercice', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        
        c_specs = [
            ('0', 1, 0, 'text',None,None,self.cell_style_header_tab),
            ('1', 1, 0, 'text',None,None,self.cell_style_header_tab),
            ('2', 1, 0, 'text','Acquisition',None,self.cell_style_header_tab),
            ('3', 1, 0, 'text','Production par l\'entreprise pour elle-même',None,self.cell_style_header_tab),
            ('4', 1, 0, 'text','Virement',None,self.cell_style_header_tab),
            ('5', 1, 0, 'text','Cession',None,self.cell_style_header_tab),
            ('6', 1, 0, 'text','Retrait',None,self.cell_style_header_tab),
            ('7', 1, 0, 'text','Virement',None,self.cell_style_header_tab),

        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid, data):
        #liasse_conf = self.pool.get('liasse.configuration')
        balance = self.pool.get('liasse.balance.erp')
        balance_ids = balance.search(cr, uid, [('id','=',data["id"])])
        balance_obj = balance.browse(cr, uid, balance_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        if balance_obj:  
                c_specs = [
                           ('1', 1, 0, 'text','IMMOBILISATION EN NON-VALEURS', None,self.cell_style_header),  
                           ('2', 1, 0, 'number', balance_obj[0].immonv_mb,None,self.cell_style_number_header),
                           ('3', 1, 0, 'number', balance_obj[0].immonv_aug_acq,None,self.cell_style_number_header),  
                           ('4', 1, 0, 'number', balance_obj[0].immonv_aug_pd,None,self.cell_style_number_header), 
                           ('5', 1, 0, 'number', balance_obj[0].immonv_aug_vir,None,self.cell_style_number_header),
                           ('6', 1, 0, 'number', balance_obj[0].immonv_dim_cess,None,self.cell_style_number_header),
                           ('7', 1, 0, 'number', balance_obj[0].immonv_dim_ret,None,self.cell_style_number_header),
                           ('8', 1, 0, 'number', balance_obj[0].immonv_dim_vir,None,self.cell_style_number_header), 
                           ('9', 1, 0, 'number', balance_obj[0].immonv_mbf,None,self.cell_style_number_header),                     
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','*Frais préliminaires', None,self.cell_style_normal),  
                           ('2', 1, 0, 'number', balance_obj[0].fp_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].fp_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].fp_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].fp_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].fp_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].fp_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].fp_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].fp_mbf,None,self.cell_style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','*Charges à répartir sur plusieurs exercices', None,self.cell_style_normal),  
                           ('2', 1, 0, 'number', balance_obj[0].charge_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].charge_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].charge_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].charge_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].charge_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].charge_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].charge_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].charge_mbf,None,self.cell_style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','*Primes de remboursement obligations', None,self.cell_style_normal),  
                           ('2', 1, 0, 'number', balance_obj[0].prime_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].prime_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].prime_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].prime_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].prime_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].prime_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].prime_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].prime_mbf,None,self.cell_style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','IMMOBILISATIONS INCORPORELLES', None,self.cell_style_header), 
                           ('2', 1, 0, 'number', balance_obj[0].immoi_mb,None,self.cell_style_number_header),
                           ('3', 1, 0, 'number', balance_obj[0].immoi_aug_acq,None,self.cell_style_number_header),  
                           ('4', 1, 0, 'number', balance_obj[0].immoi_aug_pd,None,self.cell_style_number_header), 
                           ('5', 1, 0, 'number', balance_obj[0].immoi_aug_vir,None,self.cell_style_number_header),
                           ('6', 1, 0, 'number', balance_obj[0].immoi_dim_cess,None,self.cell_style_number_header),
                           ('7', 1, 0, 'number', balance_obj[0].immoi_dim_ret,None,self.cell_style_number_header),
                           ('8', 1, 0, 'number', balance_obj[0].immoi_dim_vir,None,self.cell_style_number_header), 
                           ('9', 1, 0, 'number', balance_obj[0].immoi_mbf,None,self.cell_style_number_header),                          
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','* Immobilisation en recherche et développement', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].immord_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].immord_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].immord_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].immord_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].immord_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].immord_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].immord_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].immord_mbf,None,self.cell_style_number),  
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Brevets, marques, droits et valeurs similaires', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].brevet_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].brevet_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].brevet_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].brevet_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].brevet_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].brevet_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].brevet_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].brevet_mbf,None,self.cell_style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Fonds commercial', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].fond_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].fond_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].fond_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].fond_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].fond_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].fond_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].fond_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].fond_mbf,None,self.cell_style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Autres immobilisations incorporelles', None,self.cell_style), 
                           ('2', 1, 0, 'number', balance_obj[0].autre_incorp_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].autre_incorp_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].autre_incorp_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].autre_incorp_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].autre_incorp_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].autre_incorp_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].autre_incorp_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].autre_incorp_mbf,None,self.cell_style_number),                          
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','IMMOBILISATIONS CORPORELLES', None,self.cell_style_header),
                           ('2', 1, 0, 'number', balance_obj[0].immonc_mb,None,self.cell_style_number_header),
                           ('3', 1, 0, 'number', balance_obj[0].immonc_aug_acq,None,self.cell_style_number_header),  
                           ('4', 1, 0, 'number', balance_obj[0].immonc_aug_pd,None,self.cell_style_number_header), 
                           ('5', 1, 0, 'number', balance_obj[0].immonc_aug_vir,None,self.cell_style_number_header),
                           ('6', 1, 0, 'number', balance_obj[0].immonc_dim_cess,None,self.cell_style_number_header),
                           ('7', 1, 0, 'number', balance_obj[0].immonc_dim_ret,None,self.cell_style_number_header),
                           ('8', 1, 0, 'number', balance_obj[0].immonc_dim_vir,None,self.cell_style_number_header), 
                           ('9', 1, 0, 'number', balance_obj[0].immonc_mbf,None,self.cell_style_number_header),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Terrains', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].terrain_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].terrain_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].terrain_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].terrain_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].terrain_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].terrain_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].terrain_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].terrain_mbf,None,self.cell_style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Constructions', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].constructions_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].constructions_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].constructions_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].constructions_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].constructions_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].constructions_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].constructions_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].constructions_mbf,None,self.cell_style_number),
                           ]
                c_sepcs_list.append(c_specs)

                c_specs = [
                           ('1', 1, 0, 'text','Installat. techniques,materiel et outillage', None,self.cell_style_normal), 
                           ('2', 1, 0, 'number', balance_obj[0].inst_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].inst_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].inst_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].inst_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].inst_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].inst_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].inst_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].inst_mbf,None,self.cell_style_number),                          
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Materiel de transport', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].mat_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].mat_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].mat_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].mat_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].mat_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].mat_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].mat_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].mat_mbf,None,self.cell_style_number), 
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Mobilier, materiel bureau et amenagements', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].mob_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].mob_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].mob_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].mob_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].mob_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].mob_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].mob_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].mob_mbf,None,self.cell_style_number), 
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Immobilisations corporelles diverses', None,self.cell_style_normal),
                           ('2', 1, 0, 'number', balance_obj[0].autre_corp_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].autre_corp_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].autre_corp_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].autre_corp_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].autre_corp_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].autre_corp_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].autre_corp_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].autre_corp_mbf,None,self.cell_style_number),                           
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Immobilisations corporelles en cours', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].immocc_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].immocc_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].immocc_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].immocc_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].immocc_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].immocc_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].immocc_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].immocc_mbf,None,self.cell_style_number),  
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Matériel informatique', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].mati_mb,None,self.cell_style_number),
                           ('3', 1, 0, 'number', balance_obj[0].mati_aug_acq,None,self.cell_style_number),  
                           ('4', 1, 0, 'number', balance_obj[0].mati_aug_pd,None,self.cell_style_number), 
                           ('5', 1, 0, 'number', balance_obj[0].mati_aug_vir,None,self.cell_style_number),
                           ('6', 1, 0, 'number', balance_obj[0].mati_dim_cess,None,self.cell_style_number),
                           ('7', 1, 0, 'number', balance_obj[0].mati_dim_ret,None,self.cell_style_number),
                           ('8', 1, 0, 'number', balance_obj[0].mati_dim_vir,None,self.cell_style_number), 
                           ('9', 1, 0, 'number', balance_obj[0].mati_mbf,None,self.cell_style_number),  
                           ]
                c_sepcs_list.append(c_specs)

        return c_sepcs_list  
    