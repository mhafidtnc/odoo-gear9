# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv


class titre_particip(osv.osv_memory):
    
    _name = "titre.particip.report.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'xls_neant': 'font: bold true, height 800;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_neant = xlwt.easyxf(xls_styles['xls_neant']+ xls_styles['center'] + xls_styles['borders_all'])
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'],num_format_str = '#,##0.00')
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [15,15,15,15,15,15,15,15,15,15]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        cell_style = xlwt.easyxf(self.xls_styles['xls_title']) 
        report_name =  'TABLEAU DES TITRES DE PARTICIPATION'
        c_specs = [
            ('0', 2, 0, 'text',data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('0', 2, 0, 'text', 'IF: '+data["if"] or '')
        ]
        c_specs_list.append(c_specs)

        c_specs = [
            ('0', 1, 0, 'text', None),('1', 1, 0, 'text', None),('report_name', 4, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        
        c_specs = [
            ('0', 1, 0, 'text', None)
        ]
        c_specs_list.append(c_specs)

        c_specs = [
            ('0', 2, 0, 'text', 'Tableau n° 11'),('1', 1, 0, 'text', None),('2', 1, 0, 'text', None),('3', 1, 0, 'text', None),('4', 1, 0, 'text', None),
            ('5', 1, 0, 'text', None),('7', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','Raison sociale de la société émettrice',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'Secteur d\'activité', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'Capital social', None, self.cell_style_header_tab2),
            ('3', 1, 0, 'text', 'Participation au capital en % ', None, self.cell_style_header_tab2),
            ('4', 1, 0, 'text', 'Prix  d\'acquisition global', None, self.cell_style_header_tab2),
            ('5', 1, 0, 'text', 'Valeur comptable nette', None, self.cell_style_header_tab2),
            ('6', 3, 0, 'text', 'Extrait des derniers états de synthèse de la société émettrice', None, self.cell_style_header_tab2),
            ('7', 1, 0, 'text', 'Produits inscrits au C.P.C. de l\'exercice', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        
        c_specs = [
            ('0', 1, 0, 'text',None,None,self.cell_style_header_tab),
            ('1', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('2', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('3', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('4', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('5', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('6', 1, 0, 'text', 'Date de cloture', None, self.cell_style_header_tab),
            ('7', 1, 0, 'text', 'Situation nette', None, self.cell_style_header_tab),
            ('8', 1, 0, 'text', 'Résultat net', None, self.cell_style_header_tab),
            ('9', 1, 0, 'text', None, None, self.cell_style_header_tab),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid,data):
        tp_data = self.pool.get('titre.particip.erp')
        liasse_balance = self.pool.get('liasse.balance.erp')
        liasse_balance_ids = liasse_balance.search(cr,uid,[("id","=",data["id"])])
        liasse_balance_obj = liasse_balance.browse(cr,uid,liasse_balance_ids)
        tp_data_ids = tp_data.search(cr,uid,[('balance_id','=',data["id"])])
        tp_data_obj = tp_data.browse(cr,uid,tp_data_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number

        if not tp_data_obj:
            c_specs = [
                           ('1', 10, 0, 'text','NEANT', None,self.cell_style_neant),
                           ]
            c_sepcs_list.append(c_specs) 
        for data in tp_data_obj:
            data1=data.raison_soc
            data2=data.sect_activity
            data3=data.capit_social
            data4=data.particip_cap
            data5=data.prix_global
            data6=data.val_compt
            data7=data.extr_date
            data8=data.extr_situation
            data9=data.extr_resultat 
            data10=data.prod_inscrit                                                           
            c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'text', data2 ,None,style_text),
                           ('3', 1, 0, 'number', data3,None,style_number),
                           ('4', 1, 0, 'number', data4, None, style_number),
                           ('5', 1, 0, 'number', data5, None,style_number),
                           ('6', 1, 0, 'number', data6, None ,style_number),
                           ('7', 1, 0, 'text',data7, None,style_text),
                           ('8', 1, 0, 'number', data8 ,None,style_number),
                           ('9', 1, 0, 'number',data9, None,style_number),
                           ('10', 1, 0, 'number', data10 ,None,style_number),
                           ]
            c_sepcs_list.append(c_specs)
            
        if liasse_balance_obj:
            if liasse_balance_obj.titre_particip:
                c_specs = [
                           ('0', 1, 0, 'text','Total', None,self.cell_style_header_tab),
                           ('1', 1, 0, 'text',liasse_balance_obj[0].tp_sect_activity, None,style_text),
                           ('2', 1, 0, 'number', liasse_balance_obj[0].tp_capit_social ,None,style_number),
                           ('3', 1, 0, 'number', liasse_balance_obj[0].tp_particip_cap,None,style_number),
                           ('4', 1, 0, 'number', liasse_balance_obj[0].tp_prix_global, None, style_number),
                           ('5', 1, 0, 'number', liasse_balance_obj[0].tp_val_compt, None,style_number),
                           ('6', 1, 0, 'text', liasse_balance_obj[0].tp_extr_date, None ,style_text),
                           ('7', 1, 0, 'number',liasse_balance_obj[0].tp_extr_situation, None,style_number),
                           ('8', 1, 0, 'number',liasse_balance_obj[0].tp_extr_resultat, None,style_number),
                           ('9', 1, 0, 'number',liasse_balance_obj[0].tp_prod_inscrit, None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                                           
        return c_sepcs_list  
    