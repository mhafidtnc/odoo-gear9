# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv
from openerp.addons.report_xls.utils import rowcol_to_cell


class actif(osv.osv_memory):
    
    _name = "det.cpc.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'])
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [10,51,25,25]

    def generate_title(self,cr,uid,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style = xlwt.easyxf(self.xls_styles['xls_title'] + self.xls_styles['center']) 
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        report_name =  'DETAIL DES POSTES DU C.P.C.'
        c_specs = [
             ('1', 1, 0, 'text', data["company"] )
        ]
        c_specs_list.append(c_specs)
        c_specs = [
             ('2', 2, 0, 'text','IF: '+data["if"] or '')
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', None),('report_name', 2, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', None)
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('0', 2, 0, 'text', 'Tableau n° 6'),('3', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        code_conf = self.pool.get('liasse.code.erp')
        extra_tab = self.pool.get("liasse.extra.field.erp")
        extra_tab_ids = extra_tab.search(cr,uid,[],limit=1)
        extra_tab_obj = extra_tab.browse(cr,uid,extra_tab_ids)
        for extraline in extra_tab_obj:
            code_conf.write(cr,uid,extraline.code0cpc.id,{'valeur':data["clos"]})
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('3', 1, 0, 'text','Poste', None,self.cell_style_header_tab2),
            ('4', 1, 0, 'text', 'LIBELLE', None, self.cell_style_header_tab2),
            ('5', 1, 0, 'text', 'Exercice', None, self.cell_style_header_tab2),
            ('6', 1, 0, 'text', 'Exercice précédent',None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid,data):
        bilan_actif = self.pool.get('det.cpc.fiscale.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[('balance_id','=',data['id'])],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        c_sepcs_list = []
        style_text = None
        style_number = None
        for code in bilan_actif_obj:
            if code.type in ['1']:
                style_text = self.cell_style_header
                style_number = self.cell_style_number_header
            elif code.type in ['2']:
                style_text = self.cell_style_total
                style_number = self.cell_style_number_header
            else:
                style_text = self.cell_style_normal
                style_number = self.cell_style_number
            total1=code.code1
            total2=code.code2

                        
            c_specs = [
                           ('0', 1, 0, 'text', code.poste ,None,style_text),
                           ('5', 1, 0, 'text', code.lib ,None,style_text),
                           ('3', 1, 0, 'number', total1,None,style_number),
                           ('4', 1, 0, 'number', total2, None, style_number),
                           ]
            c_sepcs_list.append(c_specs)
                                        
        return c_sepcs_list  
    