# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv
from openerp.addons.report_xls.utils import rowcol_to_cell


class fusion(osv.osv_memory):
    
    _name = "fusion.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'xls_neant': 'font: bold true, height 800;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_style_neant = xlwt.easyxf(xls_styles['xls_neant']+ xls_styles['center'] + xls_styles['borders_all'])
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'])
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'])
    
    column_sizes = [30,18,18,18,18,18,18,18,18]

    def generate_title(self,cr,uid,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        cell_style = xlwt.easyxf(self.xls_styles['xls_title'] +self.xls_styles['borders_all']) 
        report_name =  'ETAT DES PLUS-VALUES CONSTATEES EN CAS DE  FUSIONS'
        c_specs = [
           ('1', 1, 0, 'text', data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'IF: '+data["if"] or '')
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text',None),('report_name', 1, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text',None),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
                   ('0', 1, 0, 'text', 'Tableau n° 17'),('2', 1, 0, 'text', None),('3', 1, 0, 'text', None),('4', 1, 0, 'text', None),
                   ('5', 1, 0, 'text', None),('6', 1, 0, 'text', None),('7', 1, 0, 'text', None),
                   ('1', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        code_conf = self.pool.get('liasse.code.erp')
        extra_tab = self.pool.get("liasse.extra.field.erp")
        extra_tab_ids = extra_tab.search(cr,uid,[],limit=1)
        extra_tab_obj = extra_tab.browse(cr,uid,extra_tab_ids)
        for extraline in extra_tab_obj:
            code_conf.write(cr,uid,extraline.code2fusion.id,{'valeur':data["clos"]})
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','Eléments',None,self.cell_style_header_tab),
            ('1', 1, 0, 'text', 'Valeur d\'apport', None, self.cell_style_header_tab),
            ('2', 1, 0, 'text', 'Valeur nette comptable', None, self.cell_style_header_tab),
            ('3', 1, 0, 'text', 'Plus-value constatée et différée', None, self.cell_style_header_tab),
            ('4', 1, 0, 'text','Fraction de la plus-value rapportée aux l\'exercices antérieurs (cumul) en % (2)',None,self.cell_style_header_tab),
            ('5', 1, 0, 'text', 'Fraction de la plus-value rapportée à l\'exercices actuel en %', None, self.cell_style_header_tab),
            ('6', 1, 0, 'text', 'Cumul des plus-values rapportées', None, self.cell_style_header_tab),
            ('7', 1, 0, 'text', 'Solde des plus-values non imputées', None, self.cell_style_header_tab),
            ('8', 1, 0, 'text', 'Observations', None, self.cell_style_header_tab),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid, data):
        bilan_actif = self.pool.get('fusion.fiscale.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[('balance_id','=',data['id'])],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        c_sepcs_list = []
        style_text = None
        style_number = None
        if not bilan_actif_obj:
            c_specs = [
                           ('1', 9, 0, 'text','NEANT', None,self.cell_style_neant),
                           ]
            c_sepcs_list.append(c_specs) 
        for code in bilan_actif_obj:
            if code.type in ['1']:
                style_text = self.cell_style_header
                style_number = self.cell_style_number_header
            elif code.type in ['2']:
                style_text = self.cell_style_total
                style_number = self.cell_style_number_header
            else:
                style_text = self.cell_style_normal
                style_number = self.cell_style_number
            total1=code.code1
            total2=code.code2
            total3=code.code3
            total4=code.code4
            total5=code.code5
            total6=code.code6
            total7=code.code7
                                                            
            c_specs = [
                           ('1', 1, 0, 'text',code.lib, None,style_text),
                           ('2', 1, 0, 'number', total1,None,style_text),
                           ('3', 1, 0, 'number', total2,None,style_number),
                           ('4', 1, 0, 'number', total3, None, style_number),
                           ('5', 1, 0, 'number', total4, None,style_number),
                           ('6', 1, 0, 'number', total5, None ,style_number),
                           ('7', 1, 0, 'number', total6, None ,style_number),
                           ('8', 1, 0, 'number', total7, None ,style_number),
                           ('9', 1, 0, 'text', code.code8, None ,style_text),
                           ]
            c_sepcs_list.append(c_specs)            
        return c_sepcs_list  
    