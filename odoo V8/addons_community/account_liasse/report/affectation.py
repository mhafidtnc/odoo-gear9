# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv
from openerp.addons.report_xls.utils import rowcol_to_cell


class affect(osv.osv_memory):
    
    _name = "affect.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'])
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right'])
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right'])
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'])
    
    column_sizes = [44,15,44,15]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        cell_style = xlwt.easyxf(self.xls_styles['xls_title'] +self.xls_styles['borders_all']) 
        report_name =  'TABLEAU D\'AFFECTATION DES RESULTATS INTERVENUE AU COURS DE L\'EXERCICE'
        c_specs = [
           ('1', 1, 0, 'text', data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'IF: '+data["if"] or '')
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('3', 1, 0, 'text', None),('report_name', 1, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text',None),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
                   ('0', 1, 0, 'text', 'Tableau n° 14'),('3', 1, 0, 'text', None),('1', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','A. ORIGINE DES RESULTATS A AFFECTER',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'MONTANT', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'B. AFFECTATION DES RESULTATS', None, self.cell_style_header_tab2),
            ('3', 1, 0, 'text', 'MONTANT', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid, data):
        affect = self.pool.get('liasse.affect.erp')
        affect_edi_ids = affect.search(cr,uid,[],limit=1)
        affect_edi_obj = affect.browse(cr,uid,affect_edi_ids)
        balance = self.pool.get('liasse.balance.erp')
        balance_ids = balance.search(cr, uid, [('id','=',data["id"])])
        balance_obj = balance.browse(cr, uid, balance_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number
        for code in affect_edi_obj:
            if balance_obj:                      
                c_specs = [
                           ('1', 1, 0, 'text','Report à nouveau ', None,style_text),
                           ('3', 1, 0, 'number', balance_obj[0].aff_rep_n,None,style_number),
                           ('4', 1, 0, 'text', 'Réserve légale', None, style_text),
                           ('5', 1, 0, 'number', balance_obj[0].aff_rl, None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Résultats nets en instance d\'affectation', None,style_text),
                           ('3', 1, 0, 'number', balance_obj[0].aff_res_n_in,None,style_number),
                           ('4', 1, 0, 'text', 'Autres réserves ', None, style_text),
                           ('5', 1, 0, 'number', balance_obj[0].aff_autre_r, None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                ####
                c_specs = [
                           ('1', 1, 0, 'text','Résultat net de l\'exercice ', None,style_text),
                           ('3', 1, 0, 'number', balance_obj[0].aff_res_n_ex,None,style_number),
                           ('4', 1, 0, 'text', 'Tantièmes', None, style_text),
                           ('5', 1, 0, 'number', balance_obj[0].aff_tant, None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Prélèvements sur les réserves ', None,style_text),
                           ('3', 1, 0, 'number', balance_obj[0].aff_prev,None,style_number),
                           ('4', 1, 0, 'text', 'Dividendes', None, style_text),
                           ('5', 1, 0, 'number', balance_obj[0].aff_div, None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                #####
                c_specs = [
                           ('1', 1, 0, 'text','Autres prélèvements', None,style_text),
                           ('3', 1, 0, 'number', balance_obj[0].aff_autre_prev,None,style_number),
                           ('4', 1, 0, 'text', 'Autres affectations', None, style_text),
                           ('5', 1, 0, 'number', balance_obj[0].aff_autre_aff, None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text',None),
                           ('3', 1, 0, 'text', None),
                           ('4', 1, 0, 'text', 'Report à nouveau', None, style_text),
                           ('5', 1, 0, 'number', balance_obj[0].aff_rep_n2, None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                #####
                c_specs = [
                           ('1', 1, 0, 'text','TOTAL A', None,self.cell_style_total),
                           ('3', 1, 0, 'number', balance_obj[0].aff_totala,None,self.cell_style_number_header),
                           ('4', 1, 0, 'text', 'TOTAL B', None, self.cell_style_total),
                           ('5', 1, 0, 'number', balance_obj[0].aff_totalb, None,self.cell_style_number_header),
                           ]
                c_sepcs_list.append(c_specs)
                                  
        return c_sepcs_list  
    