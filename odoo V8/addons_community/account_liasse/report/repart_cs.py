# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv


class repart_cs(osv.osv_memory):
    
    _name = "repart.cs.report.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'xls_neant': 'font: bold true, height 800;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_neant = xlwt.easyxf(xls_styles['xls_neant']+ xls_styles['center'] + xls_styles['borders_all'])
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'],num_format_str = '#,##0.00')
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [25,15,15,15,15,15,25,15,18,18]

    def generate_title(self,data):
        c_specs_list = []
        cell_style = xlwt.easyxf(self.xls_styles['xls_title']) 
        report_name =  'ETAT DE REPARTITION DU CAPITAL SOCIAL'
        c_specs = [
            ('1', 1, 0, 'text', data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', 'IF: '+data["if"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
           ('0', 2, 0, 'text', None),('report_name', 1, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', None)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self,cr,uid,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        c_specs_list = []
        credit_bail_edi = self.pool.get('liasse.repart.cs.erp')
        credit_bail_edi_ids = credit_bail_edi.search(cr,uid,[],limit=1)
        credit_bail_edi_obj = credit_bail_edi.browse(cr,uid,credit_bail_edi_ids)
        code_conf = self.pool.get('liasse.code.erp')
        #montant
        montant = data["montant_cs"] 
        if credit_bail_edi_obj:      
            c_specs = [
            ('0', 1, 0, 'text',None),
            ('1', 2, 0, 'text', 'Montant du capital : '+str(montant)),
            ]
            c_specs_list.append(c_specs)
            c_specs = [
                   ('0', 1, 0, 'text', 'Tableau n° 13'),('2', 1, 0, 'text', None),('3', 1, 0, 'text', None),('4', 1, 0, 'text', None),
                   ('5', 1, 0, 'text', None),('6', 1, 0, 'text', None),('7', 1, 0, 'text', None),('8', 1, 0, 'text', None),('1', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
                   ]
            c_specs_list.append(c_specs)
            
            code_conf.write(cr,uid,credit_bail_edi_obj.montant_capital.id,{'valeur':str(data["montant_cs"])})
            
        c_specs = [
            ('0', 1, 0, 'text','Nom, prénom ou raison sociale des principaux associés',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'IF', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'CIN', None, self.cell_style_header_tab2),
            ('3', 1, 0, 'text', 'Adresse', None, self.cell_style_header_tab2),
            ('4', 2, 0, 'text', 'NOMBRE DE TITRES', None, self.cell_style_header_tab2),
            ('6', 1, 0, 'text', 'Valeur nominale de chaque action ou part sociale', None, self.cell_style_header_tab2),
            ('7', 3, 0, 'text', 'MONTANT DU CAPITAL', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        
        c_specs = [
            ('0', 1, 0, 'text',None,None,self.cell_style_header_tab),
            ('1', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('2', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('3', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('4', 1, 0, 'text', 'Exercice précédent', None, self.cell_style_header_tab),
            ('5', 1, 0, 'text', 'Exercice actual', None, self.cell_style_header_tab),
            ('6', 1, 0, 'text', None, None, self.cell_style_header_tab),
            ('7', 1, 0, 'text', 'Souscrit', None, self.cell_style_header_tab),
            ('8', 1, 0, 'text', 'Appelé', None, self.cell_style_header_tab),
            ('9', 1, 0, 'text', 'libéré', None, self.cell_style_header_tab),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid,data):
        credit_bail_data = self.pool.get('repart.cs.erp')
        credit_bail_data_ids = credit_bail_data.search(cr,uid,[('balance_id','=',data["id"])])
        credit_bail_data_obj = credit_bail_data.browse(cr,uid,credit_bail_data_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number

        if not credit_bail_data_obj:
            c_specs = [
                           ('1', 10, 0, 'text','NEANT', None,self.cell_style_neant),
                           ]
            c_sepcs_list.append(c_specs) 
        for data in credit_bail_data_obj:
            data1=data.name
            data2=data.id_fisc
            data3=data.cin
            data4=data.adress
            data5=data.number_prec
            data6=data.number_actual
            data7=data.val_nom
            data8=data.val_sousc
            data9=data.val_appele
            data10=data.val_lib
                                                            
            c_specs = [
                           ('1', 1, 0, 'text',data1, None,style_text),
                           ('2', 1, 0, 'number', data2 ,None,style_text),
                           ('3', 1, 0, 'text', data3,None,style_number),
                           ('4', 1, 0, 'text', data4, None, style_number),
                           ('5', 1, 0, 'number', data5, None,style_number),
                           ('6', 1, 0, 'number', data6, None ,style_number),
                           ('7', 1, 0, 'number',data7, None,style_number),
                           ('8', 1, 0, 'number', data8 ,None,style_number),
                           ('9', 1, 0, 'number', data9,None,style_number),
                           ('10', 1, 0, 'number', data10, None, style_number),
                           ]
            c_sepcs_list.append(c_specs)       
                                
        return c_sepcs_list  
    