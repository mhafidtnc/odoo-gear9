# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv
from openerp.addons.report_xls.utils import rowcol_to_cell


class encouragement(osv.osv_memory):
    
    _name = "encouragement.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 200;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'])
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [50,24,24,24]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'])
        cell_style = xlwt.easyxf(self.xls_styles['xls_title'] ) 
        c_specs = [
           ('1', 1, 0, 'text', data["company"])
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'IF: '+data["if"] or '')
        ]
        c_specs_list.append(c_specs)
        report_name =  'ETAT POUR LE CALCUL DE L\'IMPOT DU PAR LES ENTREPRISES BENEFICIANTS DES MESURES D\'ENCOURAGEMENTS AUX INVESTISSEMENTS'
        c_specs = [
           ('report_name', 1, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text',None),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
                   ('0', 1, 0, 'text', 'Tableau n° 15'),('2', 1, 0, 'text', None),('3', 1, 0, 'text', None),
                   ('1', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text','RUBRIQUES',None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text', 'Ensemble des produits', None, self.cell_style_header_tab2),
            ('2', 1, 0, 'text', 'Ensemble des produits correspondant à la base imposable', None, self.cell_style_header_tab2),
            ('3', 1, 0, 'text', 'Ensemble des produits correspondant au numérateur taxable ', None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid, data):
        #liasse_conf = self.pool.get('liasse.configuration')
        balance = self.pool.get('liasse.balance.erp')
        balance_ids = balance.search(cr, uid, [('id','=',data["id"])])
        balance_obj = balance.browse(cr, uid, balance_ids)
        c_sepcs_list = []
        style_text = self.cell_style_normal
        style_number = self.cell_style_number
        if balance_obj:  
                c_specs = [
                           ('1', 1, 0, 'text','Vente', None,self.cell_style_header),                           
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Ventes imposables', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].vente_imp_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].vente_imp_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].vente_imp_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Ventes exonérées à 100%', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].vente_ex100_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].vente_ex100_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].vente_ex100_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Ventes exonérées à 50%', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].vente_ex50_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].vente_ex50_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].vente_ex50_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Lotissement et promotion immobilière', None,self.cell_style_header),                           
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Ventes et locations imposables', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].vente_li_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].vente_li_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].vente_li_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Ventes et locations exclus à 100%', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].vente_lex100_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].vente_lex100_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].vente_lex100_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Ventes et locations exclues à 50%', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].vente_lex50_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].vente_lex50_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].vente_lex50_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Prestations de services', None,self.cell_style_header),                           
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Imposables', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].pres_imp_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].pres_imp_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].pres_imp_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Exonérées à 100%', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].pres_ex100_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].pres_ex100_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].pres_ex100_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Exonérées à 50%', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].pres_ex50_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].pres_ex50_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].pres_ex50_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)

                c_specs = [
                           ('1', 1, 0, 'text','Produits et Subventions', None,self.cell_style_header),                           
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Produits accessoires. Produits financiers, dons et libéralités', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].prod_acc_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].prod_acc_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].prod_acc_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Subventions d\'équipement', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].prod_sub_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].prod_sub_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].prod_sub_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Subventions d\'équilibre', None,self.cell_style_header),                           
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Subventions d\'équilibre', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].sub_eq_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].sub_eq_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].sub_eq_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Imposables', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].sub_imp_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].sub_imp_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].sub_imp_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                c_specs = [
                           ('1', 1, 0, 'text','Exonérées à 100% ', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].sub_ex100_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].sub_ex100_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].sub_ex100_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Exonérées à 50% ', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].sub_ex50_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].sub_ex50_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].sub_ex50_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Totaux partiels', None,self.cell_style_header),                           
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Totaux partiels', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].taux_part_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].taux_part_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].taux_part_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                
                c_specs = [
                           ('1', 1, 0, 'text','Profits', None,self.cell_style_header),                           
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Profit net global des cessions après abattement pondéré', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].profit_g_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].profit_g_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].profit_g_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Autres profils exceptionnels', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].profit_ex_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].profit_ex_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].profit_ex_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                
                c_specs = [
                           ('1', 1, 0, 'text','Total général', None,self.cell_style_header),                           
                           ]
                c_sepcs_list.append(c_specs)                     
                c_specs = [
                           ('1', 1, 0, 'text','Total général', None,style_text),
                           ('2', 1, 0, 'number', balance_obj[0].total_g_ep,None,style_number),
                           ('3', 1, 0, 'number', balance_obj[0].total_g_epi,None,style_number),
                           ('4', 1, 0, 'number', balance_obj[0].total_g_ept,None,style_number),
                           ]
                c_sepcs_list.append(c_specs)
                     
        return c_sepcs_list  
    