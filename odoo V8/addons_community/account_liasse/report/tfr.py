# -*- encoding: utf-8 -*-
##############################################################################
import xlwt
import datetime as dt
from openerp.osv import osv
from openerp.addons.report_xls.utils import rowcol_to_cell


class actif(osv.osv_memory):
    
    _name = "tfr.erp"
    
    row_pos=0
    _pfc = '26'  
    _bc = '28'
    
    xls_styles = {
        'xls_title': 'font: bold true, height 240;',
        'xls_title2': 'font: bold true, height 180;',
        'bold': 'font: bold true;',
        'underline': 'font: underline true;',
        'italic': 'font: italic true;',
        'fill': 'pattern: pattern solid, fore_color %s;' % _pfc,
        'fill_blue': 'pattern: pattern solid, fore_color 27;',
        'fill_grey': 'pattern: pattern solid, fore_color 22;',
        'borders_all': 'borders: left thin, right thin, top thin, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'borders_all2': 'borders: left thin, right thin, top thick, bottom thin, '
            'left_colour %s, right_colour %s, top_colour %s, bottom_colour %s;' % (_bc, _bc, _bc, _bc),
        'left': 'align: horz left;',
        'center': 'align: horz center;',
        'right': 'align: horz right;',
        'wrap': 'align: wrap true;',
        'top': 'align: vert top;',
        'bottom': 'align: vert bottom;',
    }
    
    cell_format = xls_styles['borders_all'] + xls_styles['wrap'] + xls_styles['top'] 
    cell_style = xlwt.easyxf(cell_format)
    cell_format2 = xls_styles['borders_all2'] + xls_styles['wrap'] + xls_styles['top']
    cell_style_header_tab = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['center'])
    cell_style_header_tab2 = xlwt.easyxf(cell_format2 + xls_styles['bold']+ xls_styles['center'])
    cell_style_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['left']+ xls_styles['fill'])
    cell_style_normal = xlwt.easyxf(cell_format + xls_styles['left'])
    cell_style_total = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number_header = xlwt.easyxf(cell_format + xls_styles['bold']+ xls_styles['right']+ xls_styles['fill'],num_format_str = '#,##0.00')
    cell_style_number = xlwt.easyxf(cell_format + xls_styles['right'],num_format_str = '#,##0.00')
    
    column_sizes = [5,4,4,37,17,17]

    def generate_title(self,data):
        year_start = dt.datetime.strptime(str(data["from"]), "%Y-%m-%d")
        year_end = dt.datetime.strptime(str(data["clos"]), "%Y-%m-%d")
        c_specs_list = []
        cell_style = xlwt.easyxf(self.xls_styles['xls_title'] ) 
        cell_style2 = xlwt.easyxf(self.xls_styles['xls_title2'] ) 
        report_name =  'ETAT DES SOLDES DE GESTION (E.S.G)'
        report_name1= '1.Tableau du Formation des Resultats(T.F.R)'
        c_specs = [
           ('1', 1, 0, 'text', data["company"],None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('2', 1, 0, 'text', 'IF: '+data["if"] or '',None,cell_style2),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
          ('2', 1, 0, 'text', None), ('0', 1, 0, 'text', None),('1', 1, 0, 'text', None),('report_name', 3, 0, 'text', report_name,None,cell_style),
        ]
        c_specs_list.append(c_specs)
        c_specs = [
           ('0', 1, 0, 'text', None)
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('1', 1, 0, 'text', 'Tableau n° 5',None, cell_style2)
        ]
        c_specs_list.append(c_specs)
        c_specs = [
            ('report_name1', 4, 0, 'text', report_name1,None,cell_style2)
            ,('5', 1, 0, 'text','Exercice du: '+year_start.strftime('%d/%m/%Y')+' au '+year_end.strftime('%d/%m/%Y'),None,cell_style2)
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_header(self):
        c_specs_list = []
        c_specs = [
            ('0', 1, 0, 'text', None,None,self.cell_style_header_tab2),
            ('1', 1, 0, 'text',None,None,self.cell_style_header_tab2),
            ('3', 1, 0, 'text', None,None,self.cell_style_header_tab2),
            ('4', 1, 0, 'text', 'LIBELLE', None, self.cell_style_header_tab2),
            ('5', 1, 0, 'text', 'Exercice', None, self.cell_style_header_tab2),
            ('6', 1, 0, 'text', 'Exercice précédent',None, self.cell_style_header_tab2),
        ]
        c_specs_list.append(c_specs)
        return c_specs_list
    
    def generate_body(self, cr, uid,data):
        bilan_actif = self.pool.get('tfr.fiscale.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[('balance_id','=',data['id'])],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)

        c_sepcs_list = []
        style_text = None
        style_number = None
        for code in bilan_actif_obj:
            if code.type in ['1']:
                style_text = self.cell_style_header
                style_number = self.cell_style_number_header
            elif code.type in ['2']:
                style_text = self.cell_style_total
                style_number = self.cell_style_number_header
            else:
                style_text = self.cell_style_normal
                style_number = self.cell_style_number
            total1=code.code1
            total2=code.code2
                        
            c_specs = [
                           ('1', 1, 0, 'text',code.lettre, None,style_text),
                           ('2', 1, 0, 'text', code.num ,None,style_text),
                           ('0', 1, 0, 'text', code.op ,None,style_text),
                           ('5', 1, 0, 'text', code.lib ,None,style_text),
                           ('3', 1, 0, 'number', total1,None,style_number),
                           ('4', 1, 0, 'number', total2, None, style_number),
                           ]
            c_sepcs_list.append(c_specs)
        
                                
        return c_sepcs_list  
    