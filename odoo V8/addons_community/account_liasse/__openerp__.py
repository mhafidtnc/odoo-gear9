# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2013 Noviat nv/sa (www.noviat.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Générateur de la liasse normale',
    'version': '0.1',
    'license': 'AGPL-3',
    'author': 'R&B Consulting',
    'category': 'Generic Modules/Accounting',
    'description': """ 
Ce Module vous permet de:
    - Générer la liasse fiscale à partir d’une balance des comptes.
    - Edition des documents au bon format.
    - Conversion de la liasse fiscale en XML conformément au cahier des charges de la direction générale des impôts.
        
    """,
    'depends': ['account'],
    'demo_xml': [],
    'data': ['balance_view.xml','conf_view.xml','report/liasse_normal_xls.xml','liasse_sequence.xml','extra_tab_view.xml',
             'data/code_edi.xml','data/codification_tableaux.xml','data/code_compte.xml','data/compte.xml','security/ir.model.access.csv',
             'check_list_data.xml'],
    'active': False,
    'installable': True,
}
