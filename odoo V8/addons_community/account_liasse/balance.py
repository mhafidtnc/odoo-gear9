# -*- encoding: utf-8 -*-
from openerp.osv import fields, osv
from openerp import tools
import base64
import datetime as dt
from lxml import etree
from .report.common_balance_reports import CommonBalanceReportHeaderWebkit

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
    
class liasse_fiche_signalitique(osv.osv):
    _name = "liasse.fiche.signalitique.erp"
    _columns = {
        'company_id': fields.many2one('res.company', u'Societé', required=True),
        'tp':fields.char('TP'),
        'id_fiscal':fields.char('IF',required=True),
        #'date_start':fields.date('Date d\'ouverture',required=True),
        #'date_end':fields.date('Date de cloture',required=True),
    }
    
    _defaults = {
        'company_id': lambda s, cr, uid, c: s.pool.get('res.company')._company_default_get(cr, uid, 'liasse.balance.erp', context=c),
    }
    _rec_name = 'company_id'
    
class liasse_balance(osv.Model,CommonBalanceReportHeaderWebkit):
    
    def _get_pm_cp(self, cr, uid, ids, field_name, arg, context=None):
        pm_table = self.pool.get('pm.value.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','pm_value'], context):
            for pm in field["pm_value"]:
                pm_obj = pm_table.browse(cr,uid,pm)
                total +=pm_obj.compte_princ
            res[field["id"]] = total
        print total
        return res
    
    def _get_pm_mb(self, cr, uid, ids, field_name, arg, context=None):
        pm_table = self.pool.get('pm.value.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','pm_value'], context):
            for pm in field["pm_value"]:
                pm_obj = pm_table.browse(cr,uid,pm)
                total +=pm_obj.montant_brut
            res[field["id"]]= total
        return res
    
    def _get_pm_ac(self, cr, uid, ids, field_name, arg, context=None):
        pm_table = self.pool.get('pm.value.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','pm_value'], context):
            for pm in field["pm_value"]:
                pm_obj = pm_table.browse(cr,uid,pm)
                total +=pm_obj.amort_cumul
            res[field["id"]]= total
        return res
    
    def _get_pm_vna(self, cr, uid, ids, field_name, arg, context=None):
        pm_table = self.pool.get('pm.value.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','pm_value'], context):
            for pm in field["pm_value"]:
                pm_obj = pm_table.browse(cr,uid,pm)
                total +=pm_obj.val_net_amort
            res[field["id"]]= total
        return res
    
    def _get_pm_pc(self, cr, uid, ids, field_name, arg, context=None):
        pm_table = self.pool.get('pm.value.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','pm_value'], context):
            for pm in field["pm_value"]:
                pm_obj = pm_table.browse(cr,uid,pm)
                total +=pm_obj.prod_cess
            res[field["id"]]= total
        return res
    
    def _get_pm_pv(self, cr, uid, ids, field_name, arg, context=None):
        pm_table = self.pool.get('pm.value.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','pm_value'], context):
            for pm in field["pm_value"]:
                pm_obj = pm_table.browse(cr,uid,pm)
                total +=pm_obj.plus_value
            res[field["id"]]= total
        return res
    
    def _get_pm_mv(self, cr, uid, ids, field_name, arg, context=None):
        pm_table = self.pool.get('pm.value.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','pm_value'], context):
            for pm in field["pm_value"]:
                pm_obj = pm_table.browse(cr,uid,pm)
                total +=pm_obj.moins_value
            res[field["id"]]= total
        return res
    
    def _get_tp_cp(self, cr, uid, ids, field_name, arg, context=None):
        tp_table = self.pool.get('titre.particip.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','titre_particip'], context):
            for tp in field["titre_particip"]:
                tp_obj = tp_table.browse(cr,uid,tp)
                total +=tp_obj.capit_social
            res[field["id"]]= total
        return res
    
    def _get_tp_pc(self, cr, uid, ids, field_name, arg, context=None):
        tp_table = self.pool.get('titre.particip.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','titre_particip'], context):
            for tp in field["titre_particip"]:
                tp_obj = tp_table.browse(cr,uid,tp)
                total +=tp_obj.particip_cap
            res[field["id"]]= total
        return res 
    
    def _get_tp_pg(self, cr, uid, ids, field_name, arg, context=None):
        tp_table = self.pool.get('titre.particip.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','titre_particip'], context):
            for tp in field["titre_particip"]:
                tp_obj = tp_table.browse(cr,uid,tp)
                total +=tp_obj.prix_global
            res[field["id"]]= total
        return res
    
    def _get_tp_vc(self, cr, uid, ids, field_name, arg, context=None):
        tp_table = self.pool.get('titre.particip.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','titre_particip'], context):
            for tp in field["titre_particip"]:
                tp_obj = tp_table.browse(cr,uid,tp)
                total +=tp_obj.val_compt
            res[field["id"]]= total
        return res 
    
    def _get_tp_es(self, cr, uid, ids, field_name, arg, context=None):
        tp_table = self.pool.get('titre.particip.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','titre_particip'], context):
            for tp in field["titre_particip"]:
                tp_obj = tp_table.browse(cr,uid,tp)
                total +=tp_obj.extr_situation
            res[field["id"]]= total
        return res
    
    def _get_tp_er(self, cr, uid, ids, field_name, arg, context=None):
        tp_table = self.pool.get('titre.particip.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','titre_particip'], context):
            for tp in field["titre_particip"]:
                tp_obj = tp_table.browse(cr,uid,tp)
                total +=tp_obj.extr_resultat
            res[field["id"]]= total
        return res
    
    def _get_tp_pi(self, cr, uid, ids, field_name, arg, context=None):
        tp_table = self.pool.get('titre.particip.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','titre_particip'], context):
            for tp in field["titre_particip"]:
                tp_obj = tp_table.browse(cr,uid,tp)
                total +=tp_obj.prod_inscrit
            res[field["id"]]= total
        return res  

    def _get_in_mt(self, cr, uid, ids, field_name, arg, context=None):
        in_table = self.pool.get('interets.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','interets_associe','interets_tier'], context):
            for int in field["interets_associe"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int_obj.mont_pretl
            for int in field["interets_tier"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int.mont_pretl
            res[field["id"]]= total
        return res
    
    def _get_in_cg(self, cr, uid, ids, field_name, arg, context=None):
        in_table = self.pool.get('interets.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','interets_associe','interets_tier'], context):
            for int in field["interets_associe"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int_obj.charge_global
            for int in field["interets_tier"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int.charge_global
            res[field["id"]]= total
        return res
    
    def _get_in_rp(self, cr, uid, ids, field_name, arg, context=None):
        in_table = self.pool.get('interets.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','interets_associe','interets_tier'], context):
            for int in field["interets_associe"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int_obj.remb_princ
            for int in field["interets_tier"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int.remb_princ
            res[field["id"]]= total
        return res
    
    def _get_in_ri(self, cr, uid, ids, field_name, arg, context=None):
        in_table = self.pool.get('interets.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','interets_associe','interets_tier'], context):
            for int in field["interets_associe"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int_obj.remb_inter
            for int in field["interets_tier"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int.remb_inter
            res[field["id"]]= total
        return res
    
    def _get_in_rap(self, cr, uid, ids, field_name, arg, context=None):
        in_table = self.pool.get('interets.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','interets_associe','interets_tier'], context):
            for int in field["interets_associe"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int_obj.remb_actual_princ
            for int in field["interets_tier"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int.remb_actual_princ
            res[field["id"]]= total
        return res
    
    def _get_in_rai(self, cr, uid, ids, field_name, arg, context=None):
        in_table = self.pool.get('interets.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','interets_associe','interets_tier'], context):
            for int in field["interets_associe"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int_obj.remb_actual_inter
            for int in field["interets_tier"]:
                int_obj = in_table.browse(cr,uid,int)
                total +=int.remb_actual_inter
            res[field["id"]]= total
        return res
    
    def _get_bx_ml(self, cr, uid, ids, field_name, arg, context=None):
        beaux_table = self.pool.get('beaux.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','beaux'], context):
            for bx in field["beaux"]:
                bx_obj = beaux_table.browse(cr,uid,bx)
                total +=bx_obj.mont_annuel
            res[field["id"]]= total
        return res 
    
    def _get_bx_mc(self, cr, uid, ids, field_name, arg, context=None):
        beaux_table = self.pool.get('beaux.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','beaux'], context):
            for bx in field["beaux"]:
                bx_obj = beaux_table.browse(cr,uid,bx)
                total +=bx_obj.mont_loyer
            res[field["id"]]= total
        return res
    
    def _get_p_mp(self, cr, uid, ids, field_name, arg, context=None):
        passage_table = self.pool.get('passage.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','passages_rfc','passages_rfnc','p_benifice_p'], context):
            for tp in field['passages_rfc']:
                tp_obj = passage_table.browse(cr,uid,tp)
                total +=tp_obj.montant1
            for tp in field['passages_rfnc']:
                tp_obj = passage_table.browse(cr,uid,tp)
                total +=tp_obj.montant1
            total+=field['p_benifice_p']
            res[field["id"]]= total
        return res
    
    def _get_p_mm(self, cr, uid, ids, field_name, arg, context=None):
        passage_table = self.pool.get('passage.erp')
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','passages_dfc','passages_dfnc','p_perte_m'], context):
            for tp in field['passages_dfc']:
                tp_obj = passage_table.browse(cr,uid,tp)
                total +=tp_obj.montant2
            for tp in field['passages_dfnc']:
                tp_obj = passage_table.browse(cr,uid,tp)
                total +=tp_obj.montant2
            total+=field['p_perte_m']
            res[field["id"]]= total
        return res
    
    def _get_p_bbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','p_total_montantp','p_total_montantm'], context):
            if field['p_total_montantp'] > field['p_total_montantm']:
                total=field['p_total_montantp'] - field['p_total_montantm']
            res[field["id"]]= total
        return res
    
    def _get_p_dbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','p_total_montantp','p_total_montantm'], context):
            if field['p_total_montantp'] < field['p_total_montantm']:
                total=field['p_total_montantm'] - field['p_total_montantp']
            res[field["id"]]= total
        return res
        
    
    def _get_p_dnf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','p_deficitfm'], context):
            total=field['p_deficitfm']
            res[field["id"]]= total
        return res
    
    def _get_aff_ta(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','aff_rep_n','aff_res_n_in','aff_res_n_ex','aff_prev','aff_autre_prev'], context):
            total =field['aff_rep_n'] + field['aff_res_n_in']+ field['aff_res_n_ex'] + field['aff_prev']+ field['aff_autre_prev']
            res[field['id']]= total
        return res
    
    def _get_aff_tb(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','aff_rl','aff_autre_r','aff_tant','aff_div','aff_autre_aff','aff_rep_n2'], context):
            total =field['aff_rl'] + field['aff_autre_r']+ field['aff_tant'] + field['aff_div']+ field['aff_autre_aff'] + field['aff_rep_n2']
            res[field['id']]= total
        return res
    
    def _get_encp_ep(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','vente_imp_ep','vente_ex100_ep','vente_ex50_ep','vente_li_ep','vente_lex100_ep',
            'vente_lex50_ep','pres_imp_ep','pres_ex100_ep','pres_ex50_ep','prod_acc_ep','prod_sub_ep','sub_eq_ep',
            'sub_imp_ep','sub_ex100_ep','sub_ex50_ep'], context):
            total =(field['vente_imp_ep'] + field['vente_ex100_ep']+ field['vente_ex50_ep'] + field['vente_li_ep']+ field['vente_lex100_ep'] +
                     field['vente_lex50_ep'] + field['pres_imp_ep'] + field['pres_ex100_ep'] + field['pres_ex50_ep'] + field['prod_acc_ep'] +
                     field['prod_sub_ep'] + field['sub_eq_ep'] + field['sub_imp_ep'] + field['sub_ex100_ep'] + field['sub_ex50_ep'])
            res[field['id']]= total
        return res 
    
    def _get_encp_epi(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','vente_imp_epi','vente_ex100_epi','vente_ex50_epi','vente_li_epi','vente_lex100_epi',
                'vente_lex50_epi','pres_imp_epi','pres_ex100_epi','pres_ex50_epi','prod_acc_epi','prod_sub_epi',
                'sub_eq_epi','sub_imp_epi','sub_ex100_epi','sub_ex50_epi'], context):    
            total =(field['vente_imp_epi'] + field['vente_ex100_epi']+ field['vente_ex50_epi'] + field['vente_li_epi']+ field['vente_lex100_epi'] +
                     field['vente_lex50_epi'] + field['pres_imp_epi'] + field['pres_ex100_epi'] + field['pres_ex50_epi'] + field['prod_acc_epi'] +
                     field['prod_sub_epi'] + field['sub_eq_epi'] + field['sub_imp_epi'] + field['sub_ex100_epi'] + field['sub_ex50_epi'])
            res[field['id']]= total
        return res 
    
    def _get_encp_ept(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','vente_imp_ept','vente_ex100_ept','vente_ex50_ept','vente_li_ept','vente_lex100_ept',
                'vente_lex50_ept','pres_imp_ept','pres_ex100_ept','pres_ex50_ept','prod_acc_ept','prod_sub_ept',
                'sub_eq_ept','sub_imp_ept','sub_ex100_ept','sub_ex50_ept'], context):
            total =(field['vente_imp_ept'] + field['vente_ex100_ept']+ field['vente_ex50_ept'] + field['vente_li_ept']+ field['vente_lex100_ept'] +
                     field['vente_lex50_ept'] + field['pres_imp_ept'] + field['pres_ex100_ept'] + field['pres_ex50_ept'] + field['prod_acc_ept'] +
                     field['prod_sub_ept'] + field['sub_eq_ept'] + field['sub_imp_ept'] + field['sub_ex100_ept'] + field['sub_ex50_ept'])
            res[field['id']]= total
        return res
    
    def _get_encg_ep(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','vente_imp_ep','vente_ex100_ep','vente_ex50_ep','vente_li_ep','vente_lex100_ep',
                'vente_lex50_ep','pres_imp_ep','pres_ex100_ep','pres_ex50_ep','prod_acc_ep','prod_sub_ep','sub_eq_ep','sub_imp_ep',
                'sub_ex100_ep','sub_ex50_ep','profit_g_ep','profit_ex_ep'], context):
            total =(field['vente_imp_ep'] + field['vente_ex100_ep']+ field['vente_ex50_ep'] + field['vente_li_ep']+ field['vente_lex100_ep'] +
                     field['vente_lex50_ep'] + field['pres_imp_ep'] + field['pres_ex100_ep'] + field['pres_ex50_ep'] + field['prod_acc_ep'] +
                     field['prod_sub_ep'] + field['sub_eq_ep'] + field['sub_imp_ep'] + field['sub_ex100_ep'] + field['sub_ex50_ep'] +
                     field['profit_g_ep'] + field['profit_ex_ep'] )
            res[field['id']]= total
        return res
    
    def _get_encg_epi(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','vente_imp_epi','vente_ex100_epi','vente_ex50_epi','vente_li_epi','vente_lex100_epi',
                'vente_lex50_epi','pres_imp_epi','pres_ex100_epi','pres_ex50_epi','prod_acc_epi','prod_sub_epi',
                'sub_eq_epi','sub_imp_epi','sub_ex100_epi','sub_ex50_epi','profit_g_epi','profit_ex_epi'], context):
            total =(field['vente_imp_epi'] + field['vente_ex100_epi']+ field['vente_ex50_epi'] + field['vente_li_epi']+ field['vente_lex100_epi'] +
                     field['vente_lex50_epi'] + field['pres_imp_epi'] + field['pres_ex100_epi'] + field['pres_ex50_epi'] + field['prod_acc_epi'] +
                     field['prod_sub_epi'] + field['sub_eq_epi'] + field['sub_imp_epi'] + field['sub_ex100_epi'] + field['sub_ex50_epi'] +
                     field['profit_g_epi'] + field['profit_ex_epi'])
            res[field['id']]= total
        return res 
    
    def _get_encg_ept(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','vente_imp_ept','vente_ex100_ept','vente_ex50_ept','vente_li_ept',
                'vente_lex100_ept','vente_lex50_ept','pres_imp_ept','pres_ex100_ept','pres_ex50_ept','prod_acc_ept',
                'prod_sub_ept','sub_eq_ept','sub_imp_ept','sub_ex100_ept','sub_ex50_ept','profit_g_ept','profit_ex_ept'], context):
            total =(field['vente_imp_ept'] + field['vente_ex100_ept']+ field['vente_ex50_ept'] + field['vente_li_ept']+ field['vente_lex100_ept'] +
                     field['vente_lex50_ept'] + field['pres_imp_ept'] + field['pres_ex100_ept'] + field['pres_ex50_ept'] + field['prod_acc_ept'] +
                     field['prod_sub_ept'] + field['sub_eq_ept'] + field['sub_imp_ept'] + field['sub_ex100_ept'] + field['sub_ex50_ept'] +
                     field['profit_g_ept'] + field['profit_ex_ept'])
            res[field['id']]= total
        return res 
    
    # immo
    
    def _get_immonv_mbi(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fp_mb','charge_mb','prime_mb'], context):
            total =(field['fp_mb'] + field['charge_mb'] + field['prime_mb'])
            res[field['id']]= total
        return res
    
    def _get_immonv_aug_acq(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fp_aug_acq','charge_aug_acq','prime_aug_acq'], context):
            total =(field['fp_aug_acq'] + field['charge_aug_acq'] + field['prime_aug_acq'])
            res[field['id']]= total
        return res
    
    def _get_immonv_aug_pd(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fp_aug_pd','charge_aug_pd','prime_aug_pd'], context):
            total =(field['fp_aug_pd'] + field['charge_aug_pd'] + field['prime_aug_pd'])
            res[field['id']]= total
        return res
    
    def _get_immonv_aug_vir(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fp_aug_vir','charge_aug_vir','prime_aug_vir'], context):
            total =(field['fp_aug_vir'] + field['charge_aug_vir'] + field['prime_aug_vir'])
            res[field['id']]= total
        return res
    
    def _get_immonv_dim_cess(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fp_dim_cess','charge_dim_cess','prime_dim_cess'], context):
            total =(field['fp_dim_cess'] + field['charge_dim_cess'] + field['prime_dim_cess'])
            res[field['id']]= total
        return res
    
    def _get_immonv_dim_ret(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fp_dim_ret','charge_dim_ret','prime_dim_ret'], context):
            total =(field['fp_dim_ret'] + field['charge_dim_ret'] + field['prime_dim_ret'])
            res[field['id']]= total
        return res

    def _get_immonv_dim_vir(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fp_dim_vir','charge_dim_vir','prime_dim_vir'], context):
            total =(field['fp_dim_vir'] + field['charge_dim_vir'] + field['prime_dim_vir'])
            res[field['id']]= total
        return res
    
    def _get_immonv_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fp_mbf','charge_mbf','prime_mbf'], context):
            total =(field['fp_mbf'] + field['charge_mbf'] + field['prime_mbf'])
            res[field['id']]= total
        return res
    
    def _get_immoi_mbi(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immord_mb','brevet_mb','fond_mb','autre_incorp_mb'], context):
            total =(field['immord_mb'] + field['brevet_mb'] + field['fond_mb'] + field['autre_incorp_mb'])
            res[field['id']]= total
        return res
    
    def _get_immoi_aug_acq(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immord_aug_acq','brevet_aug_acq','fond_aug_acq','autre_incorp_aug_acq'], context):
            total =(field['immord_aug_acq'] + field['brevet_aug_acq'] + field['fond_aug_acq'] + field['autre_incorp_aug_acq'] )
            res[field['id']]= total
        return res
    
    def _get_immoi_aug_pd(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immord_aug_pd','brevet_aug_pd','fond_aug_pd','autre_incorp_aug_pd'], context):
            total =(field['immord_aug_pd'] + field['brevet_aug_pd'] + field['fond_aug_pd'] + field['autre_incorp_aug_pd'])
            res[field['id']]= total
        return res
    
    def _get_immoi_aug_vir(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immord_aug_vir','brevet_aug_vir','fond_aug_vir','autre_incorp_aug_vir'], context):
            total =(field['immord_aug_vir'] + field['brevet_aug_vir'] + field['fond_aug_vir'] + field['autre_incorp_aug_vir'])
            res[field['id']]= total
        return res
    
    def _get_immoi_dim_cess(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immord_dim_cess','brevet_dim_cess','fond_dim_cess','autre_incorp_dim_cess'], context):
            total =(field['immord_dim_cess'] + field['brevet_dim_cess'] + field['fond_dim_cess'] + field['autre_incorp_dim_cess'])
            res[field['id']]= total
        return res
    
    def _get_immoi_dim_ret(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immord_dim_ret','brevet_dim_ret','fond_dim_ret','autre_incorp_dim_ret'], context):
            total =(field['immord_dim_ret'] + field['brevet_dim_ret'] + field['fond_dim_ret'] + field['autre_incorp_dim_ret'])
            res[field['id']]= total
        return res
    
    def _get_immoi_dim_vir(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immord_dim_vir','brevet_dim_vir','fond_dim_vir','autre_incorp_dim_vir'], context):
            total =(field['immord_dim_vir'] + field['brevet_dim_vir'] + field['fond_dim_vir'] + field['autre_incorp_dim_vir'])
            res[field['id']]= total
        return res
    
    def _get_immoi_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immord_mbf','brevet_mbf','fond_mbf','autre_incorp_mbf'], context):
            total =(field['immord_mbf'] + field['brevet_mbf'] + field['fond_mbf'] + field['autre_incorp_mbf'])
            res[field['id']]= total
        return res
    
    def _get_immoc_mbi(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','terrain_mb','constructions_mb','inst_mb','mat_mb',
                'mob_mb','autre_corp_mb','immocc_mb','mati_mb'], context):
            total =(field['terrain_mb'] + field['constructions_mb'] + field['inst_mb'] + field['mat_mb'] + field['mob_mb'] + field['autre_corp_mb']
                    + field['immocc_mb'] + field['mati_mb'])
            res[field['id']]= total
        return res
    
    def _get_immoc_aug_acq(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','terrain_aug_acq','constructions_aug_acq','inst_aug_acq','mat_aug_acq',
                'mob_aug_acq','autre_corp_aug_acq','immocc_aug_acq','mati_aug_acq'], context):
            total =(field['terrain_aug_acq'] + field['constructions_aug_acq'] + field['inst_aug_acq'] + field['mat_aug_acq'] +
                     field['mob_aug_acq'] + field['autre_corp_aug_acq'] + field['immocc_aug_acq'] + field['mati_aug_acq'])
            res[field['id']]= total
        return res

    def _get_immoc_aug_pd(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','terrain_aug_pd','constructions_aug_pd','inst_aug_pd','mat_aug_pd',
                'mob_aug_pd','autre_corp_aug_pd','immocc_aug_pd','mati_aug_pd'], context):
            total =(field['terrain_aug_pd'] + field['constructions_aug_pd'] + field['inst_aug_pd'] + field['mat_aug_pd'] + field['mob_aug_pd'] + 
                    field['autre_corp_aug_pd'] + field['immocc_aug_pd'] + field['mati_aug_pd'])
            res[field['id']]= total
        return res
    
    def _get_immoc_aug_vir(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','terrain_aug_vir','constructions_aug_vir','inst_aug_vir','mat_aug_vir',
                'mob_aug_vir','autre_corp_aug_vir','immocc_aug_vir','mati_aug_vir'], context):
            total =(field['terrain_aug_vir'] + field['constructions_aug_vir'] + field['inst_aug_vir'] + field['mat_aug_vir'] + field['mob_aug_vir'] + 
                    field['autre_corp_aug_vir'] + field['immocc_aug_vir'] + field['mati_aug_vir'])
            res[field['id']]= total
        return res
    
    def _get_immoc_dim_cess(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','terrain_dim_cess','constructions_dim_cess','inst_dim_cess','mat_dim_cess',
                'mob_dim_cess','autre_corp_dim_cess','immocc_dim_cess','mati_dim_cess'], context):
            total =(field['terrain_dim_cess'] + field['constructions_dim_cess'] + field['inst_dim_cess'] + field['mat_dim_cess'] + field['mob_dim_cess'] + 
                    field['autre_corp_dim_cess'] + field['immocc_dim_cess'] + field['mati_dim_cess'])
            res[field['id']]= total
        return res
    
    def _get_immoc_dim_ret(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','terrain_dim_ret','constructions_dim_ret','inst_dim_ret','mat_dim_ret',
                'mob_dim_ret','autre_corp_dim_ret','immocc_dim_ret','mati_dim_ret'], context):
            total =(field['terrain_dim_ret'] + field['constructions_dim_ret'] + field['inst_dim_ret'] + field['mat_dim_ret'] + field['mob_dim_ret'] + 
                    field['autre_corp_dim_ret'] + field['immocc_dim_ret'] + field['mati_dim_ret'])
            res[field['id']]= total
        return res
    
    def _get_immoc_dim_vir(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','terrain_dim_vir','constructions_dim_vir','inst_dim_vir','mat_dim_vir',
                'mob_dim_vir','autre_corp_dim_vir','immocc_dim_vir','mati_dim_vir'], context):
            total =(field['terrain_dim_vir'] + field['constructions_dim_vir'] + field['inst_dim_vir'] + field['mat_dim_vir'] + field['mob_dim_vir'] + 
                    field['autre_corp_dim_vir'] + field['immocc_dim_vir'] + field['mati_dim_vir'])
            res[field["id"]]= total
        return res
    
    def _get_immoc_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','terrain_mbf','constructions_mbf','inst_mbf','mat_mbf','mob_mbf',
                'autre_corp_mbf','immocc_mbf','mati_mbf'], context):
            total =(field['terrain_mbf'] + field['constructions_mbf'] + field['inst_mbf'] + field['mat_mbf'] + field['mob_mbf'] + 
                    field['autre_corp_mbf'] + field['immocc_mbf'] + field['mati_mbf'])
            res[field['id']]= total
        return res

    def _get_fp_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fp_mb','fp_aug_acq','fp_aug_pd','fp_aug_vir','fp_dim_cess',
                'fp_dim_ret','fp_dim_vir'], context):
            total =(field['fp_mb'] + field['fp_aug_acq'] + field['fp_aug_pd'] + field['fp_aug_vir'] - field['fp_dim_cess'] - 
                    field['fp_dim_ret'] - field['fp_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_charge_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','charge_mb','charge_aug_acq','charge_aug_pd','charge_aug_vir','charge_dim_cess',
                'charge_dim_ret','charge_dim_vir'], context):
            total =(field['charge_mb'] + field['charge_aug_acq'] + field['charge_aug_pd'] + field['charge_aug_vir'] - field['charge_dim_cess'] - 
                    field['charge_dim_ret'] - field['charge_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_prime_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','prime_mb','prime_aug_acq','prime_aug_pd','prime_aug_vir','prime_dim_cess',
                'prime_dim_ret','prime_dim_vir'], context):
            total =(field['prime_mb'] + field['prime_aug_acq'] + field['prime_aug_pd'] + field['prime_aug_vir'] - field['prime_dim_cess'] - 
                    field['prime_dim_ret'] - field['prime_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_immord_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immord_mb','immord_aug_acq','immord_aug_pd','immord_aug_vir','immord_dim_cess',
                'immord_dim_ret','immord_dim_vir'], context):
            total =(field['immord_mb'] + field['immord_aug_acq'] + field['immord_aug_pd'] + field['immord_aug_vir'] - field['immord_dim_cess'] - 
                    field['immord_dim_ret'] - field['immord_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_brevet_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','brevet_mb','brevet_aug_acq','brevet_aug_pd','brevet_aug_vir','brevet_dim_cess',
                'brevet_dim_ret','brevet_dim_vir'], context):
            total =(field['brevet_mb'] + field['brevet_aug_acq'] + field['brevet_aug_pd'] + field['brevet_aug_vir'] - field['brevet_dim_cess'] - 
                    field['brevet_dim_ret'] - field['brevet_dim_vir'] )
            res[field['id']]= total
        return res

    def _get_fond_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','fond_mb','fond_aug_acq','fond_aug_pd','fond_aug_vir','fond_dim_cess',
                'fond_dim_ret','fond_dim_vir'], context):
            total =(field['fond_mb'] + field['fond_aug_acq'] + field['fond_aug_pd'] + field['fond_aug_vir'] - field['fond_dim_cess'] - 
                    field['fond_dim_ret'] - field['fond_dim_vir'] )
            res[field['id']]= total
        return res

    def _get_autre_incorp_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','autre_incorp_mb','autre_incorp_aug_acq','autre_incorp_aug_pd','autre_incorp_aug_vir','autre_incorp_dim_cess',
                'autre_incorp_dim_ret','autre_incorp_dim_vir'], context):
            total =(field['autre_incorp_mb'] + field['autre_incorp_aug_acq'] + field['autre_incorp_aug_pd'] + field['autre_incorp_aug_vir'] - field['autre_incorp_dim_cess'] - 
                    field['autre_incorp_dim_ret'] - field['autre_incorp_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_terrain_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','terrain_mb','terrain_aug_acq','terrain_aug_pd','terrain_aug_vir','terrain_dim_cess',
                'terrain_dim_ret','terrain_dim_vir'], context):
            total =(field['terrain_mb'] + field['terrain_aug_acq'] + field['terrain_aug_pd'] + field['terrain_aug_vir'] - field['terrain_dim_cess'] - 
                    field['terrain_dim_ret'] - field['terrain_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_constructions_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','constructions_mb','constructions_aug_acq','constructions_aug_pd','constructions_aug_vir','constructions_dim_cess',
                'constructions_dim_ret','constructions_dim_vir'], context):
            total =(field['constructions_mb'] + field['constructions_aug_acq']+ field['constructions_aug_pd'] + field['constructions_aug_vir'] - field['constructions_dim_cess'] - 
                    field['constructions_dim_ret'] - field['constructions_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_inst_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','inst_mb','inst_aug_acq','inst_aug_pd','inst_aug_vir','inst_dim_cess',
                'inst_dim_ret','inst_dim_vir'], context):
            total =(field['inst_mb'] + field['inst_aug_acq'] + field['inst_aug_pd'] + field['inst_aug_vir'] - field['inst_dim_cess'] - 
                    field['inst_dim_ret'] - field['inst_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_mat_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','mat_mb','mat_aug_acq','mat_aug_pd','mat_aug_vir','mat_dim_cess',
                'mat_dim_ret','mat_dim_vir'], context):
            total =(field['mat_mb'] + field['mat_aug_acq'] + field['mat_aug_pd'] + field['mat_aug_vir'] - field['mat_dim_cess'] - 
                    field['mat_dim_ret'] - field['mat_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_mob_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','mob_mb','mob_aug_acq','mob_aug_pd','mob_aug_vir','mob_dim_cess',
                'mob_dim_ret','mob_dim_vir'], context):
            total =(field['mob_mb'] + field['mob_aug_acq'] + field['mob_aug_pd'] + field['mob_aug_vir'] - field['mob_dim_cess'] - 
                    field['mob_dim_ret'] - field['mob_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_autre_corp_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','autre_corp_mb','autre_corp_aug_acq','autre_corp_aug_pd','autre_corp_aug_vir','autre_corp_dim_cess',
                'autre_corp_dim_ret','autre_corp_dim_vir'], context):
            total =(field['autre_corp_mb'] + field['autre_corp_aug_acq'] + field['autre_corp_aug_pd'] + field['autre_corp_aug_vir'] - field['autre_corp_dim_cess'] - 
                    field['autre_corp_dim_ret'] - field['autre_corp_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_immocc_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','immocc_mb','immocc_aug_acq','immocc_aug_pd','immocc_aug_vir','immocc_dim_cess',
                'immocc_dim_ret','immocc_dim_vir'], context):
            total =(field['immocc_mb'] + field['immocc_aug_acq'] + field['immocc_aug_pd'] + field['immocc_aug_vir'] - field['immocc_dim_cess'] - 
                    field['immocc_dim_ret'] - field['immocc_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_mati_mbf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','mati_mb','mati_aug_acq','mati_aug_pd','mati_aug_vir','mati_dim_cess',
                'mati_dim_ret','mati_dim_vir'], context):
            total =(field['mati_mb'] + field['mati_aug_acq'] + field['mati_aug_pd'] + field['mati_aug_vir'] - field['mati_dim_cess'] - 
                    field['mati_dim_ret'] - field['mati_dim_vir'] )
            res[field['id']]= total
        return res
    
    def _get_tva_recsd(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','tva_charsd','tva_immosd'], context):
            total =(field['tva_charsd'] + field['tva_immosd'] )
            res[field['id']]= total
        return res
    
    def _get_tva_reco(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','tva_charo','tva_immoo'], context):
            total =(field['tva_charo'] + field['tva_immoo'] )
            res[field['id']]= total
        return res
    
    def _get_tva_recd(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','tva_chard','tva_immod'], context):
            total =(field['tva_chard'] + field['tva_immod'] )
            res[field['id']]= total
        return res
    
    def _get_tva_recsf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','tva_charsf','tva_immosf'], context):
            total =(field['tva_charsf'] + field['tva_immosf'] )
            res[field['id']]= total
        return res
    
    def _get_totalsd(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','tva_facsd','tva_recsd'], context):
            total =(field['tva_facsd'] - field['tva_recsd'] )
            res[field['id']]= total
        return res
    
    def _get_totald(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','tva_facd','tva_recd'], context):
            total =(field['tva_facd'] - field['tva_recd'] )
            res[field['id']]= total
        return res
    
    def _get_totalo(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','tva_faco','tva_reco'], context):
            total =(field['tva_faco'] - field['tva_reco'] )
            res[field['id']]= total
        return res
  
    def _get_totalsf(self, cr, uid, ids, field_name, arg, context=None):
        res={}
        total =0
        for field in self.read(cr, uid, ids, ['id','tva_facsf','tva_recsf'], context):
            total =(field['tva_facsf'] - field['tva_recsf'] )
            res[field['id']]= total
        return res

    _name = "liasse.balance.erp"
    _columns = {
        'date_start':fields.date('Date d\'ouverture',required=True),
        'date_end':fields.date('Date de cloture',required=True),
        'output2':fields.binary('Data XML',readonly=True),
        'actif':fields.one2many('bilan.actif.fiscale.erp','balance_id','Actif'),
        'passif':fields.one2many('bilan.passif.fiscale.erp','balance_id','Passif'),
        'cpc':fields.one2many('cpc.fiscale.erp','balance_id','CPC'),
        'det_cpc':fields.one2many('det.cpc.fiscale.erp','balance_id','Det CPC'),
        'tfr':fields.one2many('tfr.fiscale.erp','balance_id','TFR'),
        'caf':fields.one2many('caf.fiscale.erp','balance_id','CAF'),
        'amort':fields.one2many('amort.fiscale.erp','balance_id','Amort'),
        'provision':fields.one2many('provision.fiscale.erp','balance_id','Prov'),
        'stock':fields.one2many('stock.fiscale.erp','balance_id','Stock'),
        'fusion':fields.one2many('fusion.fiscale.erp','balance_id','Fusion'),
        'exercice_prec':fields.many2one('account.fiscalyear','Exercice precedent'),
        'output':fields.binary('Declaration EDI',readonly=True),
        'output_name':fields.char('File name'),
        'date_cloture':fields.date('Date',required=True),
        'name':fields.many2one('account.fiscalyear','Exercice',required=True),
        #'file_data': fields.binary('Fichier CSV'),
        #'file_name':fields.char('File name'),
        #'balance_line':fields.one2many('liasse.balance.line','balance_id','Balance Generale'),
        'credit_bail':fields.one2many('credi.bail.erp','balance_id','Credit Bail'),
        'pm_value':fields.one2many('pm.value.erp','balance_id','+- Valeur '),
        'dotation_amort':fields.one2many('dotation.amort.erp','balance_id','Dotation'),
        'titre_particip':fields.one2many('titre.particip.erp','balance_id','Titre Participation '),
        'repart_cs':fields.one2many('repart.cs.erp','balance_id', 'ETAT DE REPARTITION DU CAPITAL SOCIAL '),
        'interets_associe':fields.one2many('interets.erp','balance_id', 'Interets Associe ', domain=[('type','=','0')], context={'default_type':'0'}),
        'interets_tier':fields.one2many('interets.erp','balance_id', 'Interets tiers ', domain=[('type','=','1')], context={'default_type':'1'}),
        'beaux':fields.one2many('beaux.erp','balance_id', 'BEAUX '),
        'extra_tab':fields.boolean('Afficher la liasse'),
        'fiche': fields.many2one('liasse.fiche.signalitique.erp', u'Fiche signalitique', required=True),
        'passages_rfc':fields.one2many('passage.erp','balance_id', 'Reintegrations fiscales courantes', domain=[('type','=','0')], context={'default_type':'0'}),
        'passages_rfnc':fields.one2many('passage.erp','balance_id', 'Reintegrations fiscales non courantes', domain=[('type','=','1')], context={'default_type':'1'}),
        'passages_dfc':fields.one2many('passage.erp','balance_id', 'Déductions fiscales courantes', domain=[('type','=','2')], context={'default_type':'2'}),
        'passages_dfnc':fields.one2many('passage.erp','balance_id', 'Déductions fiscales non courantes', domain=[('type','=','3')], context={'default_type':'3'}),
        #pm total
        'pm_compte_princ':fields.function(_get_pm_cp, string='Compte principal',type="float"),
        'pm_montant_brut':fields.function(_get_pm_mb, string='Montant brut',type="float"),
        'pm_amort_cumul':fields.function(_get_pm_ac, string='Amortissements cumulés',type="float"),
        'pm_val_net_amort':fields.function(_get_pm_vna, string='Valeur nette d\'amortissements',type="float"),
        'pm_prod_cess':fields.function(_get_pm_pc, string='Produit de cession',type="float"),
        'pm_plus_value':fields.function(_get_pm_pv, string='Plus values',type="float"),
        'pm_moins_value':fields.function(_get_pm_mv, string='Moins values',type="float"),
        # titre_particip
        'tp_sect_activity':fields.char('Secteur d\'activité'),
        'tp_capit_social':fields.function(_get_tp_cp, string='Capital social',type="float"),
        'tp_particip_cap':fields.function(_get_tp_pc, string='Participation au capital %',type="float"),
        'tp_prix_global':fields.function(_get_tp_pg, string='Prix d\'acquisition global',type="float"),
        'tp_val_compt':fields.function(_get_tp_vc, string='Valeur comptable nette',type="float"),
        'tp_extr_date':fields.date('Extrait(Date de clôture)'),
        'tp_extr_situation':fields.function(_get_tp_es, string='Extrait(Situation nette)',type="float"),
        'tp_extr_resultat':fields.function(_get_tp_er, string='Extrait(résultat net)',type="float"),
        'tp_prod_inscrit':fields.function(_get_tp_pi, string='Produits inscrits au C.P.C de l\'exercice',type="float"),
        # repartition du cs
        'montant_rcs':fields.float('Montant du capital'),
        #interets
        'in_mont_pretl':fields.function(_get_in_mt, string='Montant du prêt',type="float"),
        'in_charge_global':fields.function(_get_in_cg, string='Charge financière globale',type="float"),
        'in_remb_princ':fields.function(_get_in_rp, string='Remboursement exercices antérieurs(Principal)',type="float"),
        'in_remb_inter':fields.function(_get_in_ri, string='Remboursement exercices antérieurs(Intérêt)',type="float"),
        'in_remb_actual_princ':fields.function(_get_in_rap, string='Remboursement exercice actuel(Principal)',type="float"),
        'in_remb_actual_inter':fields.function(_get_in_rai, string='Remboursement exercice actuel(Intérêt)',type="float"),
        #beaux
        'bx_mont_pretl':fields.function(_get_bx_ml, string='Montant annuel de location',type="float"),
        'bx_charge_global':fields.function(_get_bx_mc, string='Montant du loyer compris dans les charges de l\'exercice',type="float"),
        #passage
        'p_benifice_p':fields.float('Bénéfice net (+)'),
        'p_benifice_m':fields.float('Bénéfice net (-)'),
        'p_perte_p':fields.float('Perte nette (+)'),
        'p_perte_m':fields.float('Perte nette (-)'),
        'p_total_montantp':fields.function(_get_p_mp, string='Total mantant(+)',type="float"),
        'p_total_montantm':fields.function(_get_p_mm, string='Total mantant(-)',type="float"),
        'p_benificebp':fields.float('Bénéfice brut si T1> T2 (+)'),
        'p_benificebm':fields.function(_get_p_bbf, string='Bénéfice brut si T1> T2 (-)'),
        'p_deficitfp':fields.float('Déficit brut fiscal si T2> T1 (+)'),
        'p_deficitfm':fields.function(_get_p_dbf, string='Déficit brut fiscal si T2> T1 (-)'),
        'p_exo4p':fields.float('Exercice n-4 (+)'),
        #'p_exo4m':fields.float('Exercice n-4 (-)'),
        'p_exo3p':fields.float('Exercice n-3 (+)'),
        #'p_exo3m':fields.float('Exercice n-3 (-)'),
        'p_exo2p':fields.float('Exercice n-2 (+)'),
        #'p_exo2m':fields.float('Exercice n-2 (-)'),
        'p_exo1p':fields.float('Exercice n-1 (+)'),
        #'p_exo1m':fields.float('Exercice n-1 (-)'),
        'p_benificenfp':fields.float(u'Bénéfice net fiscal(+)'),
        'p_benificenfm':fields.float(u'Bénéfice net fiscal(-)'),
        'p_deficitnfp':fields.float(u'déficit net fiscal(+)'),
        'p_deficitnfm':fields.function(_get_p_dnf, string=u'déficit net fiscal(-)'),
        'p_cumulafdm':fields.float(u'CUMUL DES AMORTISSEMENTS FISCALEMENT DIFFERES(-)'),
        'p_exo4cumulp':fields.float('Exercice n-4 (+)'),
        'p_exo3cumulp':fields.float('Exercice n-3 (+)'),
        'p_exo2cumulp':fields.float('Exercice n-2 (+)'),
        'p_exo1cumulp':fields.float('Exercice n-1 (+)'),
        #affect
        'aff_rep_n':fields.float(u'Report à nouveau'),
        'aff_res_n_in':fields.float(u'Résultats nets en instance d\'affectation '),
        'aff_res_n_ex':fields.float(u'Résultat net de l\'exercice'),
        'aff_prev':fields.float(u'Prélèvements sur les réserves'),
        'aff_autre_prev':fields.float(u'Autres prélèvements '),
        'aff_totala':fields.function(_get_aff_ta, string='Total A',type="float"),
        
        'aff_rl':fields.float(u'Réserve légale'),
        'aff_autre_r':fields.float(u'Autres réserves'),
        'aff_tant':fields.float(u'Tantièmes'),
        'aff_div':fields.float(u'Dividendes'),
        'aff_autre_aff':fields.float(u'Autres affectations'),
        'aff_rep_n2':fields.float(u'Report à nouveau '),
        'aff_totalb':fields.function(_get_aff_tb, string='Total B',type="float"),
        
        #encouragement
        'vente_imp_ep':fields.float('Vente imposable',help="Ensemble des produits"),
        'vente_imp_epi':fields.float('Vente imposable',help="Ensemble des produits correspondant à la base imposable"),
        'vente_imp_ept':fields.float('Vente imposable',help="Ensemble des produits correspondant au numérateur taxable"),
        'vente_ex100_ep':fields.float('Ventes exonérées à 100% ',help="Ensemble des produits"),
        'vente_ex100_epi':fields.float('Ventes exonérées à 100% ',help="Ensemble des produits correspondant à la base imposable"),
        'vente_ex100_ept':fields.float('Ventes exonérées à 100% ',help="Ensemble des produits correspondant au numérateur taxable"),
        'vente_ex50_ep':fields.float('Ventes exonérées à 50% ',help="Ensemble des produits"),
        'vente_ex50_epi':fields.float('Ventes exonérées à 50% ',help="Ensemble des produits correspondant à la base imposable"),
        'vente_ex50_ept':fields.float('Ventes exonérées à 50% ',help="Ensemble des produits correspondant au numérateur taxable"),
        
        'vente_li_ep':fields.float('Ventes et locations imposables',help="Ensemble des produits"),
        'vente_li_epi':fields.float('Ventes et locations imposables',help="Ensemble des produits correspondant à la base imposable"),
        'vente_li_ept':fields.float('Ventes et locations imposables',help="Ensemble des produits correspondant au numérateur taxable"),
        'vente_lex100_ep':fields.float('Ventes et locations exclues à 100% ',help="Ensemble des produits"),
        'vente_lex100_epi':fields.float('Ventes et locations exclues à 100%  ',help="Ensemble des produits correspondant à la base imposable"),
        'vente_lex100_ept':fields.float('Ventes et locations exclues à 100%  ',help="Ensemble des produits correspondant au numérateur taxable"),
        'vente_lex50_ep':fields.float('Ventes et locations exclues à 50%  ',help="Ensemble des produits"),
        'vente_lex50_epi':fields.float('Ventes et locations exclues à 50%  ',help="Ensemble des produits correspondant à la base imposable"),
        'vente_lex50_ept':fields.float('Ventes et locations exclues à 50% ',help="Ensemble des produits correspondant au numérateur taxable"),

        'pres_imp_ep':fields.float('Imposables',help="Ensemble des produits"),
        'pres_imp_epi':fields.float('Imposables',help="Ensemble des produits correspondant à la base imposable"),
        'pres_imp_ept':fields.float('Imposables',help="Ensemble des produits correspondant au numérateur taxable"),
        'pres_ex100_ep':fields.float('Exonérées à 100% ',help="Ensemble des produits"),
        'pres_ex100_epi':fields.float('Exonérées à 100% ',help="Ensemble des produits correspondant à la base imposable"),
        'pres_ex100_ept':fields.float('Exonérées à 100% ',help="Ensemble des produits correspondant au numérateur taxable"),
        'pres_ex50_ep':fields.float('Exonérées à 50% ',help="Ensemble des produits"),
        'pres_ex50_epi':fields.float('Exonérées à 50% ',help="Ensemble des produits correspondant à la base imposable"),
        'pres_ex50_ept':fields.float('Exonérées à 50% ',help="Ensemble des produits correspondant au numérateur taxable"),
        
        'prod_acc_ep':fields.float(u'Produits accessoires. Produits financiers, dons et libéralités',help="Ensemble des produits"),
        'prod_acc_epi':fields.float(u'Produits accessoires. Produits financiers, dons et libéralités ',help="Ensemble des produits correspondant à la base imposable"),
        'prod_acc_ept':fields.float(u'Produits accessoires. Produits financiers, dons et libéralités ',help="Ensemble des produits correspondant au numérateur taxable"),
        'prod_sub_ep':fields.float('Subventions d\'équipement',help="Ensemble des produits"),
        'prod_sub_epi':fields.float('Subventions d\'équipement ',help="Ensemble des produits correspondant à la base imposable"),
        'prod_sub_ept':fields.float('Subventions d\'équipement ',help="Ensemble des produits correspondant au numérateur taxable"),
        
        'sub_eq_ep':fields.float(u'Subventions d\'équilibre',help="Ensemble des produits"),
        'sub_eq_epi':fields.float(u'Subventions d\'équilibre ',help="Ensemble des produits correspondant à la base imposable"),
        'sub_eq_ept':fields.float(u'Subventions d\'équilibre',help="Ensemble des produits correspondant au numérateur taxable"),
        'sub_imp_ep':fields.float('Imposables',help="Ensemble des produits"),
        'sub_imp_epi':fields.float('Imposables',help="Ensemble des produits correspondant à la base imposable"),
        'sub_imp_ept':fields.float('Imposables',help="Ensemble des produits correspondant au numérateur taxable"),
        'sub_ex100_ep':fields.float(u'Exonérées à 100%',help="Ensemble des produits"),
        'sub_ex100_epi':fields.float(u'Exonérées à 100%',help="Ensemble des produits correspondant à la base imposable"),
        'sub_ex100_ept':fields.float(u'Exonérées à 100%',help="Ensemble des produits correspondant au numérateur taxable"),
        'sub_ex50_ep':fields.float(u'Exonérées à 50%',help="Ensemble des produits"),
        'sub_ex50_epi':fields.float(u'Exonérées à 50%',help="Ensemble des produits correspondant à la base imposable"),
        'sub_ex50_ept':fields.float(u'Exonérées à 50%',help="Ensemble des produits correspondant au numérateur taxable"),
        
        'taux_part_ep':fields.function(_get_encp_ep, string='partiels',type="float",help="Ensemble des produits"),
        'taux_part_epi':fields.function(_get_encp_epi, string='Totaux partiels',type="float",help="Ensemble des produits correspondant à la base imposable"),
        'taux_part_ept':fields.function(_get_encp_ept, string='Totaux partiels',type="float",help="Ensemble des produits correspondant au numérateur taxable"),
        
        'profit_g_ep':fields.float(u'Profit net global des cessions après abattement pondéré',help="Ensemble des produits"),
        'profit_g_epi':fields.float(u'Profit net global des cessions après abattement pondéré',help="Ensemble des produits correspondant à la base imposable"),
        'profit_g_ept':fields.float(u'Profit net global des cessions après abattement pondéré',help="Ensemble des produits correspondant au numérateur taxable"),
        'profit_ex_ep':fields.float(u'Autres profils exceptionnels',help="Ensemble des produits"),
        'profit_ex_epi':fields.float(u'Autres profils exceptionnels',help="Ensemble des produits correspondant à la base imposable"),
        'profit_ex_ept':fields.float(u'Autres profils exceptionnels',help="Ensemble des produits correspondant au numérateur taxable"),
        
        'total_g_ep':fields.function(_get_encg_ep, string='Total général',type="float",help="Ensemble des produits"),
        'total_g_epi':fields.function(_get_encg_epi, string='Total général',type="float",help="Ensemble des produits correspondant à la base imposable"),
        'total_g_ept':fields.function(_get_encg_ept, string='Total général',type="float",help="Ensemble des produits correspondant au numérateur taxable"),
                
        # immo
        'immonv_mb':fields.function(_get_immonv_mbi, string='IMMOBILISATION EN NON-VALEURS',type="float",help="Montant brut debut exercice"),
        'immonv_aug_acq':fields.function(_get_immonv_aug_acq, string='Immo non valeurs',type="float",help="Augmentation: acquisition"),
        'immonv_aug_pd':fields.function(_get_immonv_aug_pd, string='Immo non valeurs',type="float",help="Augmentation: Production par l'entreprise pour elle-même"),
        'immonv_aug_vir':fields.function(_get_immonv_aug_vir, string='Immo non valeurs',type="float",help="Augmentation: Virement"),
        'immonv_dim_cess':fields.function(_get_immonv_dim_cess, string='Immo non valeurs',type="float",help="Diminution: cession"),
        'immonv_dim_ret':fields.function(_get_immonv_dim_ret, string='Immo non valeurs',type="float",help="Diminution: retrait"),
        'immonv_dim_vir':fields.function(_get_immonv_dim_vir, string='Immo non valeurs',type="float",help="Diminution: virement"),
        'immonv_mbf':fields.function(_get_immonv_mbf, string='Immo non valeurs',type="float",help="Montant brut fin exercice"),
        
        'fp_mb':fields.float('Frais preliminaires',help="Montant brut debut exercice"),
        'fp_aug_acq':fields.float('Frais preliminaires',help="Augmentation: acquisition"),
        'fp_aug_pd':fields.float('Frais preliminaires',help="Augmentation: Production par l'entreprise pour elle-même"),
        'fp_aug_vir':fields.float('Frais preliminaires',help="Augmentation: Virement"),
        'fp_dim_cess':fields.float('Frais preliminaires',help="Diminution: cession"),
        'fp_dim_ret':fields.float('Frais preliminaires',help="Diminution: retrait"),
        'fp_dim_vir':fields.float('Frais preliminaires',help="Diminution: virement"),
        'fp_mbf':fields.function(_get_fp_mbf, string='Frais preliminaires',type="float",help="Montant brut fin exercice"),
        
        'charge_mb':fields.float('Charges a repartir sur plusieurs exercices',help="Montant brut debut exercice"),
        'charge_aug_acq':fields.float('Charges a repartir sur plusieurs exercices',help="Augmentation: acquisition"),
        'charge_aug_pd':fields.float('Charges a repartir sur plusieurs exercices',help="Augmentation: Production par l'entreprise pour elle-même"),
        'charge_aug_vir':fields.float('Charges a repartir sur plusieurs exercices',help="Augmentation: Virement"),
        'charge_dim_cess':fields.float('Charges a repartir sur plusieurs exercices',help="Diminution: cession"),
        'charge_dim_ret':fields.float('Charges a repartir sur plusieurs exercices',help="Diminution: retrait"),
        'charge_dim_vir':fields.float('Charges a repartir sur plusieurs exercices',help="Diminution: virement"),
        'charge_mbf':fields.function(_get_charge_mbf, string='Charges a repartir sur plusieurs exercices',type="float",help="Montant brut fin exercice"),
        
        'prime_mb':fields.float('Primes de remboursement obligations',help="Montant brut debut exercice"),
        'prime_aug_acq':fields.float('Primes de remboursement obligations',help="Augmentation: acquisition"),
        'prime_aug_pd':fields.float('Primes de remboursement obligations',help="Augmentation: Production par l'entreprise pour elle-même"),
        'prime_aug_vir':fields.float('Primes de remboursement obligations',help="Augmentation: Virement"),
        'prime_dim_cess':fields.float('Primes de remboursement obligations',help="Diminution: cession"),
        'prime_dim_ret':fields.float('Primes de remboursement obligations',help="Diminution: retrait"),
        'prime_dim_vir':fields.float('Primes de remboursement obligations',help="Diminution: virement"),
        'prime_mbf':fields.function(_get_prime_mbf, string='Primes de remboursement obligations',type="float",help="Montant brut fin exercice"),
        
        'immoi_mb':fields.function(_get_immoi_mbi, string='IMMOBILISATIONS INCORPORELLES',type="float",help="Montant brut debut exercice"),
        'immoi_aug_acq':fields.function(_get_immoi_aug_acq, string='IMMOBILISATIONS INCORPORELLES',type="float",help="Augmentation: acquisition"),
        'immoi_aug_pd':fields.function(_get_immoi_aug_pd, string='IMMOBILISATIONS INCORPORELLES',type="float",help="Augmentation: Production par l'entreprise pour elle-même"),
        'immoi_aug_vir':fields.function(_get_immoi_aug_vir, string='IMMOBILISATIONS INCORPORELLES',type="float",help="Augmentation: Virement"),
        'immoi_dim_cess':fields.function(_get_immoi_dim_cess, string='IMMOBILISATIONS INCORPORELLES',type="float",help="Diminution: cession"),
        'immoi_dim_ret':fields.function(_get_immoi_dim_ret, string='IMMOBILISATIONS INCORPORELLES',type="float",help="Diminution: retrait"),
        'immoi_dim_vir':fields.function(_get_immoi_dim_vir, string='IMMOBILISATIONS INCORPORELLES',type="float",help="Diminution: virement"),
        'immoi_mbf':fields.function(_get_immoi_mbf, string='IMMOBILISATIONS INCORPORELLES',type="float",help="Montant brut fin exercice"),
        
        'immord_mb':fields.float('Immobilisation en recherche et developpement',help="Montant brut debut exercice"),
        'immord_aug_acq':fields.float('Immobilisation en recherche et developpement',help="Augmentation: acquisition"),
        'immord_aug_pd':fields.float('Immobilisation en recherche et developpement',help="Augmentation: Production par l'entreprise pour elle-même"),
        'immord_aug_vir':fields.float('Immobilisation en recherche et developpement',help="Augmentation: Virement"),
        'immord_dim_cess':fields.float('Immobilisation en recherche et developpement',help="Diminution: cession"),
        'immord_dim_ret':fields.float('Immobilisation en recherche et developpement',help="Diminution: retrait"),
        'immord_dim_vir':fields.float('Immobilisation en recherche et developpement',help="Diminution: virement"),
        'immord_mbf':fields.function(_get_immord_mbf, string='Immobilisation en recherche et developpement',type="float",help="Montant brut fin exercice"),
        
        'brevet_mb':fields.float('Brevets, marques, droits et valeurs similaires',help="Montant brut debut exercice"),
        'brevet_aug_acq':fields.float('Brevets, marques, droits et valeurs similaires',help="Augmentation: acquisition"),
        'brevet_aug_pd':fields.float('Brevets, marques, droits et valeurs similaires',help="Augmentation: Production par l'entreprise pour elle-même"),
        'brevet_aug_vir':fields.float('Brevets, marques, droits et valeurs similaires',help="Augmentation: Virement"),
        'brevet_dim_cess':fields.float('Brevets, marques, droits et valeurs similaires',help="Diminution: cession"),
        'brevet_dim_ret':fields.float('Brevets, marques, droits et valeurs similaires',help="Diminution: retrait"),
        'brevet_dim_vir':fields.float('Brevets, marques, droits et valeurs similaires',help="Diminution: virement"),
        'brevet_mbf':fields.function(_get_brevet_mbf, string='Brevets, marques, droits et valeurs similaires',type="float",help="Montant brut fin exercice"),
        
        'fond_mb':fields.float('Fonds commercial',help="Montant brut debut exercice"),
        'fond_aug_acq':fields.float('Fonds commercial',help="Augmentation: acquisition"),
        'fond_aug_pd':fields.float('Fonds commercial',help="Augmentation: Production par l'entreprise pour elle-même"),
        'fond_aug_vir':fields.float('Fonds commercial',help="Augmentation: Virement"),
        'fond_dim_cess':fields.float('Fonds commercial',help="Diminution: cession"),
        'fond_dim_ret':fields.float('Fonds commercial',help="Diminution: retrait"),
        'fond_dim_vir':fields.float('Fonds commercial',help="Diminution: virement"),
        'fond_mbf':fields.function(_get_fond_mbf, string='Fonds commercial',type="float",help="Montant brut fin exercice"),
        
        'autre_incorp_mb':fields.float('Autres immobilisations incorporelles',help="Montant brut debut exercice"),
        'autre_incorp_aug_acq':fields.float('Autres immobilisations incorporelles',help="Augmentation: acquisition"),
        'autre_incorp_aug_pd':fields.float('Autres immobilisations incorporelles',help="Augmentation: Production par l'entreprise pour elle-même"),
        'autre_incorp_aug_vir':fields.float('Autres immobilisations incorporelles',help="Augmentation: Virement"),
        'autre_incorp_dim_cess':fields.float('Autres immobilisations incorporelles',help="Diminution: cession"),
        'autre_incorp_dim_ret':fields.float('Autres immobilisations incorporelles',help="Diminution: retrait"),
        'autre_incorp_dim_vir':fields.float('Autres immobilisations incorporelles',help="Diminution: virement"),
        'autre_incorp_mbf':fields.function(_get_autre_incorp_mbf, string='Autres immobilisations incorporelles',type="float",help="Montant brut fin exercice"),
        
        'immonc_mb':fields.function(_get_immoc_mbi, string='IMMOBILISATIONS CORPORELLES',type="float",help="Montant brut debut exercice"),
        'immonc_aug_acq':fields.function(_get_immoc_aug_acq, string='IMMOBILISATIONS CORPORELLES',type="float",help="Augmentation: acquisition"),
        'immonc_aug_pd':fields.function(_get_immoc_aug_pd, string='IMMOBILISATIONS CORPORELLES',type="float",help="Augmentation: Production par l'entreprise pour elle-même"),
        'immonc_aug_vir':fields.function(_get_immoc_aug_vir, string='IMMOBILISATIONS CORPORELLES',type="float",help="Augmentation: Virement"),
        'immonc_dim_cess':fields.function(_get_immoc_dim_cess, string='IMMOBILISATIONS CORPORELLES',type="float",help="Diminution: cession"),
        'immonc_dim_ret':fields.function(_get_immoc_dim_ret, string='IMMOBILISATIONS CORPORELLES',type="float",help="Diminution: retrait"),
        'immonc_dim_vir':fields.function(_get_immoc_dim_vir, string='IMMOBILISATIONS CORPORELLES',type="float",help="Diminution: virement"),
        'immonc_mbf':fields.function(_get_immoc_mbf, string='IMMOBILISATIONS CORPORELLES',type="float",help="Montant brut fin exercice"),
        
        'terrain_mb':fields.float('Terrains',help="Montant brut debut exercice"),
        'terrain_aug_acq':fields.float('Terrains',help="Augmentation: acquisition"),
        'terrain_aug_pd':fields.float('Terrains',help="Augmentation: Production par l'entreprise pour elle-même"),
        'terrain_aug_vir':fields.float('Terrains',help="Augmentation: Virement"),
        'terrain_dim_cess':fields.float('Terrains',help="Diminution: cession"),
        'terrain_dim_ret':fields.float('Terrains',help="Diminution: retrait"),
        'terrain_dim_vir':fields.float('Terrains',help="Diminution: virement"),
        'terrain_mbf':fields.function(_get_terrain_mbf, string='Terrains',type="float",help="Montant brut fin exercice"),
        
        'constructions_mb':fields.float('Constructions',help="Montant brut debut exercice"),
        'constructions_aug_acq':fields.float('Constructions',help="Augmentation: acquisition"),
        'constructions_aug_pd':fields.float('Constructions',help="Augmentation: Production par l'entreprise pour elle-même"),
        'constructions_aug_vir':fields.float('Constructions',help="Augmentation: Virement"),
        'constructions_dim_cess':fields.float('Constructions',help="Diminution: cession"),
        'constructions_dim_ret':fields.float('Constructions',help="Diminution: retrait"),
        'constructions_dim_vir':fields.float('Constructions',help="Diminution: virement"),
        'constructions_mbf':fields.function(_get_constructions_mbf, string='Constructionsins',type="float",help="Montant brut fin exercice"),
        
        'inst_mb':fields.float('Installat. techniques,materiel et outillage',help="Montant brut debut exercice"),
        'inst_aug_acq':fields.float('Installat. techniques,materiel et outillage',help="Augmentation: acquisition"),
        'inst_aug_pd':fields.float('Installat. techniques,materiel et outillage',help="Augmentation: Production par l'entreprise pour elle-même"),
        'inst_aug_vir':fields.float('Installat. techniques,materiel et outillage',help="Augmentation: Virement"),
        'inst_dim_cess':fields.float('Installat. techniques,materiel et outillage',help="Diminution: cession"),
        'inst_dim_ret':fields.float('Installat. techniques,materiel et outillage',help="Diminution: retrait"),
        'inst_dim_vir':fields.float('Installat. techniques,materiel et outillage',help="Diminution: virement"),
        'inst_mbf':fields.function(_get_inst_mbf, string='Installat. techniques,materiel et outillage',type="float",help="Montant brut fin exercice"),
        
        'mat_mb':fields.float('Materiel de transport',help="Montant brut debut exercice"),
        'mat_aug_acq':fields.float('Materiel de transport',help="Augmentation: acquisition"),
        'mat_aug_pd':fields.float('Materiel de transport',help="Augmentation: Production par l'entreprise pour elle-même"),
        'mat_aug_vir':fields.float('Materiel de transport',help="Augmentation: Virement"),
        'mat_dim_cess':fields.float('Materiel de transport',help="Diminution: cession"),
        'mat_dim_ret':fields.float('Materiel de transport',help="Diminution: retrait"),
        'mat_dim_vir':fields.float('Materiel de transport',help="Diminution: virement"),
        'mat_mbf':fields.function(_get_mat_mbf, string='Materiel de transport',type="float",help="Montant brut fin exercice"),
        
        'mob_mb':fields.float('Mobilier, materiel bureau et amenagements',help="Montant brut debut exercice"),
        'mob_aug_acq':fields.float('Mobilier, materiel bureau et amenagements',help="Augmentation: acquisition"),
        'mob_aug_pd':fields.float('Mobilier, materiel bureau et amenagements',help="Augmentation: Production par l'entreprise pour elle-même"),
        'mob_aug_vir':fields.float('Mobilier, materiel bureau et amenagements',help="Augmentation: Virement"),
        'mob_dim_cess':fields.float('Mobilier, materiel bureau et amenagements',help="Diminution: cession"),
        'mob_dim_ret':fields.float('Mobilier, materiel bureau et amenagements',help="Diminution: retrait"),
        'mob_dim_vir':fields.float('Mobilier, materiel bureau et amenagements',help="Diminution: virement"),
        'mob_mbf':fields.function(_get_mob_mbf, string='Mobilier, materiel bureau et amenagements',help="Montant brut fin exercice"),
        
        'autre_corp_mb':fields.float('Immobilisations corporelles diverses',help="Montant brut debut exercice"),
        'autre_corp_aug_acq':fields.float('Immobilisations corporelles diverses',help="Augmentation: acquisition"),
        'autre_corp_aug_pd':fields.float('Immobilisations corporelles diverses',help="Augmentation: Production par l'entreprise pour elle-même"),
        'autre_corp_aug_vir':fields.float('Immobilisations corporelles diverses',help="Augmentation: Virement"),
        'autre_corp_dim_cess':fields.float('Immobilisations corporelles diverses',help="Diminution: cession"),
        'autre_corp_dim_ret':fields.float('Immobilisations corporelles diverses',help="Diminution: retrait"),
        'autre_corp_dim_vir':fields.float('Immobilisations corporelles diverses',help="Diminution: virement"),
        'autre_corp_mbf':fields.function(_get_autre_corp_mbf, string='Immobilisations corporelles diverses',help="Montant brut fin exercice"),
        
        'immocc_mb':fields.float('Immobilisations corporelles en cours',help="Montant brut debut exercice"),
        'immocc_aug_acq':fields.float('Immobilisations corporelles en cours',help="Augmentation: acquisition"),
        'immocc_aug_pd':fields.float('Immobilisations corporelles en cours',help="Augmentation: Production par l'entreprise pour elle-même"),
        'immocc_aug_vir':fields.float('Immobilisations corporelles en cours',help="Augmentation: Virement"),
        'immocc_dim_cess':fields.float('Immobilisations corporelles en cours',help="Diminution: cession"),
        'immocc_dim_ret':fields.float('Immobilisations corporelles en cours',help="Diminution: retrait"),
        'immocc_dim_vir':fields.float('Immobilisations corporelles en cours',help="Diminution: virement"),
        'immocc_mbf':fields.function(_get_immocc_mbf, string='Immobilisations corporelles en cours',help="Montant brut fin exercice"),
        
        'mati_mb':fields.float('Matériel informatique',help="Montant brut debut exercice"),
        'mati_aug_acq':fields.float('Matériel informatique',help="Augmentation: acquisition"),
        'mati_aug_pd':fields.float('Matériel informatique',help="Augmentation: Production par l'entreprise pour elle-même"),
        'mati_aug_vir':fields.float('Matériel informatique',help="Augmentation: Virement"),
        'mati_dim_cess':fields.float('Matériel informatique',help="Diminution: cession"),
        'mati_dim_ret':fields.float('Matériel informatique',help="Diminution: retrait"),
        'mati_dim_vir':fields.float('Matériel informatique',help="Diminution: virement"),
        'mati_mbf':fields.function(_get_mati_mbf, string='Matériel informatique',help="Montant brut fin exercice"),
        
        # dotation
        'val_acq':fields.float('Valeur a amortir (Prix d acquisition)'),
        'val_compt':fields.float('Valeur a amortir - Valeur comptable apres reevaluation'),
        'amort_ant':fields.float('Amortissements anterieurs'),
        'amort_ded_et':fields.float('Amortissements deduits du Benefice brut de l exercice (Taux)'),
        'amort_ded_e':fields.float('Amortissements deduits du Benefice brut de l exercice Amortissements normaux ou acceleres de l exercice'),
        'amort_fe':fields.float('Total des amortissements a la fin de l exercice'),
        'montant_dot':fields.float('Montant global'),
        
        # tva
        'tva_facsd':fields.float('tva',help="solde au début de l\'exercice"),
        'tva_faco':fields.float('tva',help="Opérations comptables de l\'exercice"),
        'tva_facd':fields.float('tva',help="Déclarations T.V.A de l\'exercice"),
        'tva_facsf':fields.float('tva',help="Solde fin d\'exercice"),
        
        'tva_recsd':fields.function(_get_tva_recsd,string='tva',help="solde au début de l\'exercice"),
        'tva_reco':fields.function(_get_tva_reco,string='tva',help="Opérations comptables de l\'exercice"),
        'tva_recd':fields.function(_get_tva_recd,string='tva',help="Déclarations T.V.A de l\'exercice"),
        'tva_recsf':fields.function(_get_tva_recsf,string='tva',help="Solde fin d\'exercice"),
        
        'tva_charsd':fields.float('tva',help="solde au début de l\'exercice"),
        'tva_charo':fields.float('tva',help="Opérations comptables de l\'exercice"),
        'tva_chard':fields.float('tva',help="Déclarations T.V.A de l\'exercice"),
        'tva_charsf':fields.float('tva',help="Solde fin d\'exercice"),
        
        'tva_immosd':fields.float('tva',help="solde au début de l\'exercice"),
        'tva_immoo':fields.float('tva',help="Opérations comptables de l\'exercice"),
        'tva_immod':fields.float('tva',help="Déclarations T.V.A de l\'exercice"),
        'tva_immosf':fields.float('tva',help="Solde fin d\'exercice"),
        
        'tva_totalsd':fields.function(_get_totalsd,string='tva',help="solde au début de l\'exercice"),
        'tva_totalo':fields.function(_get_totalo,string='tva',help="Opérations comptables de l\'exercice"),
        'tva_totald':fields.function(_get_totald,string='tva',help="Déclarations T.V.A de l\'exercice"),
        'tva_totalsf':fields.function(_get_totalsf,string='tva',help="Solde fin d\'exercice"),
    
    }
    """
    _constraints = [
        (osv.osv._check_recursion, 'Error ! You cannot create recursive categories.', ['exercice_prec'])
    ]
    """

    _defaults = {
        'output_name':'edi.xml',
        #'file_name':'balance.csv',
        'date_cloture':fields.date.context_today,
        'extra_tab': False,
    }
    
    def onchange_fiscalyer(self, cr, uid, ids, fy, context=None):
        date_start = False
        date_stop = False
        if fy:
            f_year = self.pool.get('account.fiscalyear').browse(cr, uid, fy, context=context)
            date_start = f_year.date_start
            date_stop = f_year.date_stop
        return {'value': {'date_start': date_start,'date_end':date_stop}}
    
    def update_data_passage(self,cr,uid,ids,fy,start_period, stop_period, initial_balance_mode,context=None):
        pasage_edi = self.pool.get('liasse.passage.erp')
        passage_data = self.pool.get('passage.erp')
        #liasse_conf = self.pool.get('liasse.configuration')
        #balance = self.pool.get('liasse.balance.line')
        balance = self.pool.get('account.account')
        pasage_edi_ids = pasage_edi.search(cr,uid,[],limit=1)
        total =0
                            
        for rec in self.browse(cr,uid,ids,context=context):
            compte_bal_ids = balance.search(cr, uid, [('code','=like','7'+"%"),('type','not in',['view'])])
            signe = self.check_account_signe('7')
            for acc_id in compte_bal_ids:
                net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start_period, stop_period, initial_balance_mode)[acc_id].get('balance',0)
                total += net*signe
            compte_bal_ids = balance.search(cr, uid, [('code','=like','6'+"%"),('type','not in',['view'])])
            signe = self.check_account_signe('6')
            for acc_id in compte_bal_ids:
                net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start_period, stop_period, initial_balance_mode)[acc_id].get('balance',0)
                total -= net*signe
                            
        if total >=0:
            self.write(cr, uid, ids, {'p_benifice_p':total})
        else:
            self.write(cr, uid, ids, {'p_perte_m':abs(total)})
        # reintegration courante
        total =0
        for rec in self.browse(cr,uid,ids,context=context):
            cr.execute("delete from passage_erp where balance_id=%s and name=%s",(rec.id,"Ecart de conversion passif",))
            compte_bal_ids = balance.search(cr, uid, [('code','=like','47'+"%"),('type','not in',['view'])])
            signe = self.check_account_signe('47')
            for acc_id in compte_bal_ids:
                net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start_period, stop_period, initial_balance_mode)[acc_id].get('balance',0)
                total += net*signe
            if total != 0:
                passage_data.create(cr,uid,{"name":"Ecart de conversion passif","montant1":total,"type":"0","balance_id":rec.id})
                
        list_compte=['6128','6148','6168','6178','6188','6198','6388','6398']
        total =0
        for rec in self.browse(cr,uid,ids,context=context):
            cr.execute("delete from passage_erp where balance_id=%s and name=%s",(rec.id,"Charge sur exercice anterieur",))
            for com in list_compte:
                compte_bal_ids = balance.search(cr, uid, [('code','=like',com+"%"),('type','not in',['view'])])
                signe = self.check_account_signe(com)
                for acc_id in compte_bal_ids:
                    net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start_period, stop_period, initial_balance_mode)[acc_id].get('balance',0)
                    total += net*signe
            if total != 0:
                passage_data.create(cr,uid,{"name":"Charge sur exercice anterieur","montant1":total,"type":"0","balance_id":rec.id})
        
        # reintegration non courante
        total =0
        for rec in self.browse(cr,uid,ids,context=context):
            cr.execute("delete from passage_erp where balance_id=%s and name=%s",(rec.id,"Impôt sur societé",))
            compte_bal_ids = balance.search(cr, uid, [('code','=like','67'+"%"),('type','not in',['view'])])
            signe = self.check_account_signe('67')
            for acc_id in compte_bal_ids:
                net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start_period, stop_period, initial_balance_mode)[acc_id].get('balance',0)
                total += net*signe
            if total != 0:
                passage_data.create(cr,uid,{"name":"Impôt sur societé","montant1":total,"type":"1","balance_id":rec.id})

        total =0
        for rec in self.browse(cr,uid,ids,context=context):
            cr.execute("delete from passage_erp where balance_id=%s and name=%s",(rec.id,"Penalité et amande fiscal",))
            compte_bal_ids = balance.search(cr, uid, [('code','=like','6581'+"%"),('type','not in',['view'])])
            signe = self.check_account_signe('6581')
            for acc_id in compte_bal_ids:
                net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start_period, stop_period, initial_balance_mode)[acc_id].get('balance',0)
                total += net*signe
            if total != 0:
                passage_data.create(cr,uid,{"name":"Penalité et amande fiscal","montant1":total,"type":"1","balance_id":rec.id})
                
        total =0
        for rec in self.browse(cr,uid,ids,context=context):
            cr.execute("delete from passage_erp where balance_id=%s and name=%s",(rec.id,"Dons et liberalité",))
            compte_bal_ids = balance.search(cr, uid, [('code','=like','6586'+"%"),('type','not in',['view'])])
            signe = self.check_account_signe('6586')
            for acc_id in compte_bal_ids:
                net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start_period, stop_period, initial_balance_mode)[acc_id].get('balance',0)
                total += net*signe
            if total != 0:
                passage_data.create(cr,uid,{"name":"Dons et liberalité","montant1":total,"type":"1","balance_id":rec.id})
                
        # deduction non courante        
        total =0
        for rec in self.browse(cr,uid,ids,context=context):
            cr.execute("delete from passage_erp where balance_id=%s and name=%s",(rec.id,"Ecarts de conversion",))
            compte_bal_ids = balance.search(cr, uid, [('code','=like','47'+"%"),('type','not in',['view'])])
            signe = self.check_account_signe('47')
            for acc_id in compte_bal_ids:
                net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start_period, stop_period, initial_balance_mode)[acc_id].get('init_balance',0)
                total += net*signe
            if total != 0:
                passage_data.create(cr,uid,{"name":"Ecarts de conversion","montant2":total,"type":"2","balance_id":rec.id})
        
        return True
    
    def update_tva(self, cr, uid, ids,fy,start, stop, initial_balance_mode, context=None):
        tva = self.pool.get('liasse.tva.erp')
        tva_ids = tva.search(cr,uid,[],limit=1)
        tva_obj = tva.browse(cr,uid,tva_ids)
        liasse_conf = self.pool.get('liasse.configuration.erp')
        balance = self.pool.get('account.account')
        
        total1=0
        total2=0
        total3=0
        total4=0
        total5=0
        total6=0
        total7=0
        total8=0
        total9=0
        for line in tva_obj:
            compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',line.tva_facsd.id)])
            compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            for compte_conf in compte_conf_obj:
                for compte_id in compte_conf.compte:
                    compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                    for acc_id in compte_bal_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('init_balance',0)
                        total1 += abs(net)
                            
            compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',line.tva_charsd.id)])
            compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            for compte_conf in compte_conf_obj:
                for compte_id in compte_conf.compte:
                    compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                    for acc_id in compte_bal_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('init_balance',0)
                        total2 += abs(net)
                            
            compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',line.tva_immosd.id)])
            compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            for compte_conf in compte_conf_obj:
                for compte_id in compte_conf.compte:
                    compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                    for acc_id in compte_bal_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('init_balance',0)
                        total3 += abs(net)

            compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',line.tva_facsf.id)])
            compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            for compte_conf in compte_conf_obj:
                for compte_id in compte_conf.compte:
                    compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                    signe = self.check_account_signe(compte_id.compte)
                    for acc_id in compte_bal_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('balance',0)
                        total1 += net*signe
                            
            compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',line.tva_charsf.id)])
            compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            for compte_conf in compte_conf_obj:
                for compte_id in compte_conf.compte:
                    compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                    signe = self.check_account_signe(compte_id.compte)
                    for acc_id in compte_bal_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('balance',0)
                        total2 += net*signe
                            
            compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',line.tva_immosf.id)])
            compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            for compte_conf in compte_conf_obj:
                for compte_id in compte_conf.compte:
                    compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                    signe = self.check_account_signe(compte_id.compte)
                    for acc_id in compte_bal_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('balance',0)
                        total3 += net*signe
                            
            ####
            compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',line.tva_facd.id)])
            compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            for compte_conf in compte_conf_obj:
                for compte_id in compte_conf.compte:
                    compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                    for acc_id in compte_bal_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('debit',0)
                        total7 += net
                            
            compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',line.tva_chard.id)])
            compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            for compte_conf in compte_conf_obj:
                for compte_id in compte_conf.compte:
                    compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                    for acc_id in compte_bal_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('credit',0)
                        total8 += net
                            
            compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',line.tva_immod.id)])
            compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            for compte_conf in compte_conf_obj:
                for compte_id in compte_conf.compte:
                    compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                    for acc_id in compte_bal_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('credit',0)
                        total9 += net
        total10 = total4 + total7 - total1
        total11 = total6 + total9 - total3
        total12 = total5 +total8 - total2
        self.write(cr, uid, ids, {
            'tva_facsd': total1,'tva_charsd':total2,'tva_immosd':total3,'tva_facsf':total4,'tva_charsf':total5,
            'tva_immosf':total6,'tva_facd':total7,'tva_chard':total8,'tva_immod':total9,
            'tva_faco':total10,'tva_faco':total11,'tva_charo':total12
                    })
        return True
                
                            
    def update_data(self, cr, uid, ids,fy,start,stop,initial_balance_mode, context=None):
        immo = self.pool.get('liasse.immo.erp')
        immo_ids = immo.search(cr,uid,[],limit=1)
        immo_obj = immo.browse(cr,uid,immo_ids)
        # acquisition
        for line in immo_obj:
                frais,frais1 = self.get_acq(cr, uid, ids, line.fp_aug_acq.id,fy,start,stop,initial_balance_mode)
                charge,charge1 =  self.get_acq(cr, uid, ids, line.charge_aug_acq.id,fy,start,stop,initial_balance_mode)
                prime,prime1 = self.get_acq(cr, uid, ids, line.prime_aug_acq.id,fy,start,stop,initial_balance_mode)
                immord,immord1 = self.get_acq(cr, uid, ids, line.immord_aug_acq.id,fy,start,stop,initial_balance_mode)
                brevet,brevet1 = self.get_acq(cr, uid, ids,line.brevet_aug_acq.id,fy,start,stop,initial_balance_mode)
                fond,fond1 = self.get_acq(cr, uid, ids,line.fond_aug_acq.id,fy,start,stop,initial_balance_mode)
                autre_incorp,autre_incorp1 = self.get_acq(cr, uid, ids,line.autre_incorp_aug_acq.id,fy,start,stop,initial_balance_mode)
                terrain,terrain1 = self.get_acq(cr, uid, ids,line.terrain_aug_acq.id,fy,start,stop,initial_balance_mode)
                constructions,constructions1 = self.get_acq(cr, uid, ids,line.constructions_aug_acq.id,fy,start,stop,initial_balance_mode) 
                inst,inst1 = self.get_acq(cr, uid, ids,line.inst_aug_acq.id,fy,start,stop,initial_balance_mode) 
                mat,mat1 = self.get_acq(cr, uid, ids, line.mat_aug_acq.id,fy,start,stop,initial_balance_mode)
                mob,mob1 = self.get_acq(cr, uid, ids, line.mob_aug_acq.id,fy,start,stop,initial_balance_mode)
                autre,autre1 = self.get_acq(cr, uid, ids,line.autre_corp_aug_acq.id,fy,start,stop,initial_balance_mode)
                immocc,immocc1 = self.get_acq(cr, uid, ids,line.immocc_aug_acq.id,fy,start,stop,initial_balance_mode)
                mati,mati1 = self.get_acq(cr, uid, ids,line.mati_aug_acq.id,fy,start,stop,initial_balance_mode)
                self.write(cr, uid, ids, {
            'fp_aug_acq': frais,'charge_aug_acq':charge,'prime_aug_acq':prime,'immord_aug_acq':immord,'brevet_aug_acq':brevet,
            'fond_aug_acq':fond,'autre_incorp_aug_acq':autre_incorp,'terrain_aug_acq':terrain,'constructions_aug_acq':constructions,
            'inst_aug_acq':inst,'mat_aug_acq':mat,'mob_aug_acq':mob,'autre_corp_aug_acq':autre,'immocc_aug_acq':immocc,
            'mati_aug_acq':mati,
            'fp_mb': frais1,'charge_mb':charge1,'prime_mb':prime1,'immord_mb':immord1,'brevet_mb':brevet1,
            'fond_mb':fond1,'autre_incorp_mb':autre_incorp1,'terrain_mb':terrain1,'constructions_mb':constructions1,
            'inst_mb':inst1,'mat_mb':mat1,'mob_mb':mob1,'autre_corp_mb':autre1,'immocc_mb':immocc1,
            'mati_mb':mati1
                    })
                frais = self.get_cess(cr, uid, ids,line.fp_dim_cess.id,fy,start,stop,initial_balance_mode)
                charge =  self.get_cess(cr, uid, ids, line.charge_dim_cess.id,fy,start,stop,initial_balance_mode)
                prime = self.get_cess(cr, uid, ids,line.prime_dim_cess.id,fy,start,stop,initial_balance_mode)
                immord = self.get_cess(cr, uid, ids,line.immord_dim_cess.id,fy,start,stop,initial_balance_mode)
                brevet = self.get_cess(cr, uid, ids,line.brevet_dim_cess.id,fy,start,stop,initial_balance_mode)
                fond = self.get_cess(cr, uid, ids,line.fond_dim_cess.id,fy,start,stop,initial_balance_mode)
                autre_incorp = self.get_cess(cr, uid, ids,line.autre_incorp_dim_cess.id,fy,start,stop,initial_balance_mode)
                terrain = self.get_cess(cr, uid, ids,line.terrain_dim_cess.id,fy,start,stop,initial_balance_mode)
                constructions = self.get_cess(cr, uid, ids,line.constructions_dim_cess.id,fy,start,stop,initial_balance_mode) 
                inst = self.get_cess(cr, uid, ids,line.inst_dim_cess.id,fy,start,stop,initial_balance_mode) 
                mat = self.get_cess(cr, uid, ids,line.mat_dim_cess.id,fy,start,stop,initial_balance_mode)
                mob = self.get_cess(cr, uid, ids,line.mob_dim_cess.id,fy,start,stop,initial_balance_mode)
                autre = self.get_cess(cr, uid, ids,line.autre_corp_dim_cess.id,fy,start,stop,initial_balance_mode)
                immocc = self.get_cess(cr, uid, ids,line.immocc_dim_cess.id,fy,start,stop,initial_balance_mode)
                mati = self.get_cess(cr, uid, ids,line.mati_dim_cess.id,fy,start,stop,initial_balance_mode)
                self.write(cr, uid, ids, {
            'fp_dim_cess': frais,'charge_dim_cess':charge,'prime_dim_cess':prime,'immord_dim_cess':immord,'brevet_dim_cess':brevet,
            'fond_dim_cess':fond,'autre_incorp_dim_cess':autre_incorp,'terrain_dim_cess':terrain,'constructions_dim_cess':constructions,
            'inst_dim_cess':inst,'mat_dim_cess':mat,'mob_dim_cess':mob,'autre_corp_dim_cess':autre,'immocc_dim_vir':immocc,
            'mati_dim_cess':mati
                    })
                
        return True
                
                
                
    def get_acq(self,cr,uid,ids, code,fy,start,stop,initial_balance_mode,context=None):
        liasse_conf = self.pool.get('liasse.configuration.erp')
        balance = self.pool.get('account.account')
        total =0
        total1 =0
        compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',code)])
        compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
        for compte_conf in compte_conf_obj:
            for compte_id in compte_conf.compte:
                compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                for acc_id in compte_bal_ids:
                    det =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id]
                    total += det.get('debit',0)
                    total1 += det.get('init_balance',0)
        return total,total1
    
    def get_cess(self,cr,uid,ids, code,fy,start,stop,initial_balance_mode,context=None):
        liasse_conf = self.pool.get('liasse.configuration.erp')
        balance = self.pool.get('account.account')
        total =0
        compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',code)])
        compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
        for compte_conf in compte_conf_obj:
            for compte_id in compte_conf.compte:
                compte_bal_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+"%"),('type','not in',['view'])])
                for acc_id in compte_bal_ids:
                    det =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id]
                    total += det.get('credit',0)
        return total
    
    def check_list_rub(self, cr, uid, ids, context=None):
        check_data = self.pool.get('check.rubrique.erp')
        cr.execute(""" delete from check_rubrique_erp""")
        check_config = self.pool.get('liasse.check.rub.erp')
        check_config_ids = check_config.search(cr,uid,[])
        check_config_obj = check_config.browse(cr,uid,check_config_ids,context=context)
        for record in self.browse(cr, uid, ids, context=context):
            start_period, stop_period, start, stop = \
            self._get_start_stop_for_filter('filter_no', record.name, None, None, None, None,cr,uid)
            initial_balance_mode = self._get_initial_balance_mode(cr,uid,start_period)
            for check_line in check_config_obj:
                data = {}
                data["name"]=check_line.rub
                data["compte"]=check_line.compte
                total = 0
                signe = self.check_account_signe(check_line.compte)
                compte_bal_somme_ids = self.pool.get('account.account').search(cr, uid, [('code','=like',check_line.compte+'%'),('type','not in',['view'])])
                for acc_id in compte_bal_somme_ids:
                    net =self._get_account_details(cr,uid,[acc_id], 'all', record.name, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('balance',0)
                    total += net*signe
                data["valeurbal"]=total
                total=0
                for code_some in check_line.code_ids:
                    total+=float(code_some.valeur)
                data["valeurres"]=total
              
                check_data.create(cr,uid,data)
        return {
                    'type': 'ir.actions.act_window',
                    'name': 'Rubriques de controle',
                    'view_mode': 'tree',
                    'view_type': 'form',
                    'res_model': 'check.rubrique.erp',
                    'target': 'new',
                    }
        
        
    def check_list(self, cr, uid, ids, context=None):
        check_data = self.pool.get('check.list.erp')
        cr.execute(""" delete from check_list_erp""")
        check_config = self.pool.get('liasse.check.erp')
        check_config_ids = check_config.search(cr,uid,[])
        check_config_obj = check_config.browse(cr,uid,check_config_ids)
        for check_line in check_config_obj:
            data = {}
            data["etat"]=check_line.etat
            data["name"]=check_line.code
            total_exo = 0
            total_exop=0
            for code_some in check_line.coden_ids:
                total_exo+=float(code_some.valeur)
            for code_min in check_line.coden_min_ids:
                total_exo-=float(code_min.valeur)
            data["exercice"]=total_exo
              
            for code_some in check_line.codenp_ids:
                total_exop+=float(code_some.valeur)
            for code_min in check_line.codenp_min_ids:
                total_exop-=float(code_min.valeur)
            data["exercicep"]=total_exop
            check_data.create(cr,uid,data)
        return {
                    'type': 'ir.actions.act_window',
                    'name': 'Check list',
                    'view_mode': 'tree',
                    'view_type': 'form',
                    'res_model': 'check.list.erp',
                    'target': 'new',
                    }
        
     
    def generate_report(self, cr, uid, ids, context=None):
        
        data = {}
        data["id"]=ids[0]
        for rec in self.browse(cr,uid,ids,context=context):
            data["company"]=rec.fiche.company_id.name
            data["if"]=rec.fiche.id_fiscal
            data["clos"]=rec.date_end
            data["from"]=rec.date_start
            data["montant_cs"] = rec.montant_rcs
            data["montant_dot"] = rec.montant_dot
        print data
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'liasse.normal.erp.xls',
            'datas': data,
        }
        
    def generate_head(self,cr,uid,ids,context=None):
        for rec in self.browse(cr, uid, ids, context=context):
            model_id = 1
            id_fiscal = rec.fiche.id_fiscal
            year_deb = rec.date_start
            year_fin = rec.date_end
            doc = etree.Element( "Liasse", nsmap={})
            model = etree.SubElement( doc, "modele" )
            id = etree.SubElement( model, "id" )
            id.text = str(model_id)
            fiscal_result = etree.SubElement( doc, "resultatFiscal" )
            id_fiscale = etree.SubElement( fiscal_result, "identifiantFiscal" )
            id_fiscale.text = str(id_fiscal)
            yearf = etree.SubElement( fiscal_result, "exerciceFiscalDu" )
            yearf.text = str(year_deb)
            yearl = etree.SubElement( fiscal_result, "exerciceFiscalAu" )
            yearl.text = str(year_fin)
        return doc

    def generate_table(self,cr,uid,ids,group_val,code,context=None):
        if float(code.valeur) != 0:
            val_cell = etree.SubElement( group_val, "ValeurCellule" )
            cellule = etree.SubElement( val_cell, "cellule" )
            codeEdi = etree.SubElement( cellule, "codeEdi" )
            codeEdi.text = str(code.code)
            valeur = etree.SubElement( val_cell, "valeur" )
            valeur.text = str("{0:.2f}".format(float(code.valeur)))
        else:
            val_cell = etree.SubElement( group_val, "ValeurCellule" )
            cellule = etree.SubElement( val_cell, "cellule" )
            codeEdi = etree.SubElement( cellule, "codeEdi" )
            codeEdi.text = str(code.code)
            valeur = etree.SubElement( val_cell, "valeur" )
        return True
    
    def generate_table_text(self,cr,uid,ids,group_val,code,context=None):
        if code.valeur:
            val_cell = etree.SubElement( group_val, "ValeurCellule" )
            cellule = etree.SubElement( val_cell, "cellule" )
            codeEdi = etree.SubElement( cellule, "codeEdi" )
            codeEdi.text = str(code.code)
            valeur = etree.SubElement( val_cell, "valeur" )
            valeur.text = str(code.valeur)
        else:
            val_cell = etree.SubElement( group_val, "ValeurCellule" )
            cellule = etree.SubElement( val_cell, "cellule" )
            codeEdi = etree.SubElement( cellule, "codeEdi" )
            codeEdi.text = str(code.code)
            valeur = etree.SubElement( val_cell, "valeur" )
        return True
    
    def generate_table_date(self,cr,uid,ids,group_val,code,context=None):
        if code.valeur:
            val_cell = etree.SubElement( group_val, "ValeurCellule" )
            cellule = etree.SubElement( val_cell, "cellule" )
            codeEdi = etree.SubElement( cellule, "codeEdi" )
            codeEdi.text = str(code.code)
            valeur = etree.SubElement( val_cell, "valeur" )
            date_f = dt.datetime.strptime(code.valeur, "%Y-%m-%d")
            date = date_f.strftime('%d/%m/%Y')
            valeur.text = str(date)
        else:
            val_cell = etree.SubElement( group_val, "ValeurCellule" )
            cellule = etree.SubElement( val_cell, "cellule" )
            codeEdi = etree.SubElement( cellule, "codeEdi" )
            codeEdi.text = str(code.code)
            valeur = etree.SubElement( val_cell, "valeur" )
        return True
    
    def generate_table_date_ilimite(self,cr,uid,ids,group_val,code,context=None):
        if not code.valeur_ids:
                val_cell = etree.SubElement( group_val, "ValeurCellule" )
                cellule = etree.SubElement( val_cell, "cellule" )
                codeEdi = etree.SubElement( cellule, "codeEdi" )
                codeEdi.text = str(code.code)
                valeur = etree.SubElement( val_cell, "valeur" )
                lignenum = etree.SubElement( val_cell, "numeroLigne" )
                lignenum.text=str(1)
        else:
            for val in code.valeur_ids:
                if val.valeur:
                    val_cell = etree.SubElement( group_val, "ValeurCellule" )
                    cellule = etree.SubElement( val_cell, "cellule" )
                    codeEdi = etree.SubElement( cellule, "codeEdi" )
                    codeEdi.text = str(code.code)
                    valeur = etree.SubElement( val_cell, "valeur" )
                    if val.valeur:
                        date_f = dt.datetime.strptime(val.valeur, "%Y-%m-%d")
                        date = date_f.strftime('%d/%m/%Y')
                        valeur.text = str(date)
                    lignenum = etree.SubElement( val_cell, "numeroLigne" )
                    lignenum.text=str(val.ligne)
        return True
    
    def generate_table_ilimite(self,cr,uid,ids,group_val,code,context=None):
        if not code.valeur_ids:
                val_cell = etree.SubElement( group_val, "ValeurCellule" )
                cellule = etree.SubElement( val_cell, "cellule" )
                codeEdi = etree.SubElement( cellule, "codeEdi" )
                codeEdi.text = str(code.code)
                valeur = etree.SubElement( val_cell, "valeur" )
                lignenum = etree.SubElement( val_cell, "numeroLigne" )
                lignenum.text=str(1)
            
        else:
            for val in code.valeur_ids:
                if float(val.valeur) != 0:
                    val_cell = etree.SubElement( group_val, "ValeurCellule" )
                    cellule = etree.SubElement( val_cell, "cellule" )
                    codeEdi = etree.SubElement( cellule, "codeEdi" )
                    codeEdi.text = str(code.code)
                    valeur = etree.SubElement( val_cell, "valeur" )
                    if val.valeur:
                        valeur.text = str("{0:.2f}".format(float(val.valeur)))
                    lignenum = etree.SubElement( val_cell, "numeroLigne" )
                    lignenum.text=str(val.ligne)
        return True
    
    def generate_table_entier_ilimite(self,cr,uid,ids,group_val,code,context=None):
        if not code.valeur_ids:
                val_cell = etree.SubElement( group_val, "ValeurCellule" )
                cellule = etree.SubElement( val_cell, "cellule" )
                codeEdi = etree.SubElement( cellule, "codeEdi" )
                codeEdi.text = str(code.code)
                valeur = etree.SubElement( val_cell, "valeur" )
                lignenum = etree.SubElement( val_cell, "numeroLigne" )
                lignenum.text=str(1)
        else:
            for val in code.valeur_ids:
                if int(val.valeur) != 0:
                    val_cell = etree.SubElement( group_val, "ValeurCellule" )
                    cellule = etree.SubElement( val_cell, "cellule" )
                    codeEdi = etree.SubElement( cellule, "codeEdi" )
                    codeEdi.text = str(code.code)
                    valeur = etree.SubElement( val_cell, "valeur" )
                    if val.valeur:
                        valeur.text = str(val.valeur)
                    lignenum = etree.SubElement( val_cell, "numeroLigne" )
                    lignenum.text=str(val.ligne)
        return True
    
    def generate_table_text_ilimite(self,cr,uid,ids,group_val,code,context=None):
        if not code.valeur_ids:
                val_cell = etree.SubElement( group_val, "ValeurCellule" )
                cellule = etree.SubElement( val_cell, "cellule" )
                codeEdi = etree.SubElement( cellule, "codeEdi" )
                codeEdi.text = str(code.code)
                valeur = etree.SubElement( val_cell, "valeur" )
                lignenum = etree.SubElement( val_cell, "numeroLigne" )
                lignenum.text=str(1)
        else:
            for val in code.valeur_ids:
                if val.valeur:
                    val_cell = etree.SubElement( group_val, "ValeurCellule" )
                    cellule = etree.SubElement( val_cell, "cellule" )
                    codeEdi = etree.SubElement( cellule, "codeEdi" )
                    codeEdi.text = str(code.code)
                    valeur = etree.SubElement( val_cell, "valeur" )
                    if val.valeur:
                        valeur.text = val.valeur
                    lignenum = etree.SubElement( val_cell, "numeroLigne" )
                    lignenum.text=str(val.ligne)
        return True
    
    def generate_extra_field(self,cr,uid,ids,extra_vals,code,context=None):
        if code.valeur:
            extra_val = etree.SubElement( extra_vals, "ExtraFieldValeur" )
            extra_field = etree.SubElement( extra_val, "extraField" )
            code_extra = etree.SubElement( extra_field, "code" )
            code_extra.text = str(code.code)
            date_f = dt.datetime.strptime(code.valeur, "%Y-%m-%d")
            date = date_f.strftime('%d/%m/%Y')
            code_val = etree.SubElement( extra_val, "valeur" )
            code_val.text= str(date)
        return True
    
    def generate_extra_double_field(self,cr,uid,ids,extra_vals,code,context=None):
        if float(code.valeur) !=0:
            extra_val = etree.SubElement( extra_vals, "ExtraFieldValeur" )
            extra_field = etree.SubElement( extra_val, "extraField" )
            code_extra = etree.SubElement( extra_field, "code" )
            code_extra.text = str(code.code)
            code_val = etree.SubElement( extra_val, "valeur" )
            code_val.text= str("{0:.2f}".format(float(code.valeur)))
        return True
    
    def generate(self,cr,uid,ids,context=None):
        #edi            
        self.edi_actif(cr, uid, ids, context)
        self.edi_affectation(cr, uid, ids, context)
        self.edi_amort(cr, uid, ids, context)
        self.edi_beaux(cr, uid, ids, context)
        self.edi_caf(cr, uid, ids, context)
        self.edi_cpc(cr, uid, ids, context)
        self.edi_credit_bail(cr, uid, ids, context)
        self.edi_det_cpc(cr, uid, ids, context)
        self.edi_dotation(cr, uid, ids, context)
        self.edi_encouragement(cr, uid, ids, context)
        self.edi_fusion(cr, uid, ids, context)
        self.edi_immo(cr, uid, ids, context)
        self.edi_interets(cr, uid, ids, context)
        self.edi_passage(cr, uid, ids, context)
        self.edi_passif(cr, uid, ids, context)
        self.edi_pm_value(cr, uid, ids, context)
        self.edi_provision(cr, uid, ids, context)
        self.edi_repart_cs(cr, uid, ids, context)
        self.edi_stock(cr, uid, ids, context)
        self.edi_tfr(cr, uid, ids, context)
        self.edi_titre_particp(cr, uid, ids, context)
        self.edi_tva(cr, uid, ids, context)
        #############################
        doc =self.generate_head(cr,uid,ids,context)
        group_val_tab = etree.SubElement( doc, "groupeValeursTableau" )
        extra_tab = self.pool.get("liasse.extra.field.erp")
        extra_tab_ids = extra_tab.search(cr,uid,[],limit=1)
        extra_tab_obj = extra_tab.browse(cr,uid,extra_tab_ids,context)
        
        #actif
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(2)
        group_val = etree.SubElement( val_tab, "groupeValeurs")
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        actif_ids = self.pool.get("liasse.bilan.actif.erp").search(cr,uid,[])
        actif_obj = self.pool.get("liasse.bilan.actif.erp").browse(cr,uid,actif_ids)
        for line in actif_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            
        # passif
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(1)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        passif_ids = self.pool.get("liasse.bilan.passif.erp").search(cr,uid,[])
        passif_obj = self.pool.get("liasse.bilan.passif.erp").browse(cr,uid,passif_ids)
        for line in passif_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)

        # cpc
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(6)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        cpc_ids = self.pool.get("liasse.cpc.erp").search(cr,uid,[])
        cpc_obj = self.pool.get("liasse.cpc.erp").browse(cr,uid,cpc_ids)
        for line in cpc_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            
        # esg
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(32)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        tfr_ids = self.pool.get("liasse.tfr.erp").search(cr,uid,[])
        tfr_obj = self.pool.get("liasse.tfr.erp").browse(cr,uid,tfr_ids)
        for line in tfr_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
        caf_ids = self.pool.get("liasse.caf.erp").search(cr,uid,[])
        caf_obj = self.pool.get("liasse.caf.erp").browse(cr,uid,caf_ids)
        for line in caf_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            
        # det cpc
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(34)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        det_cpc_ids = self.pool.get("liasse.det.cpc.erp").search(cr,uid,[])
        det_cpc_obj = self.pool.get("liasse.det.cpc.erp").browse(cr,uid,det_cpc_ids)
        for line in det_cpc_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
        for extra_line in extra_tab_obj:
            self.generate_extra_field(cr, uid, ids, extra_vals, extra_line.code0cpc, context)

        # amort
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(24)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        amort_ids = self.pool.get("liasse.amort.erp").search(cr,uid,[])
        amort_obj = self.pool.get("liasse.amort.erp").browse(cr,uid,amort_ids)
        for line in amort_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            
        # prov
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(37)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        prov_ids = self.pool.get("liasse.provision.erp").search(cr,uid,[])
        prov_obj = self.pool.get("liasse.provision.erp").browse(cr,uid,prov_ids)
        for line in prov_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            self.generate_table(cr, uid, ids, group_val, line.code5, context)
            self.generate_table(cr, uid, ids, group_val, line.code6, context)
            self.generate_table(cr, uid, ids, group_val, line.code7, context)
            self.generate_table(cr, uid, ids, group_val, line.code8, context)
        for extra_line in extra_tab_obj:
            self.generate_extra_field(cr, uid, ids, extra_vals, extra_line.code1prov, context)

        # stock
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(36)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        stock_ids = self.pool.get("liasse.stock.erp").search(cr,uid,[])
        stock_obj = self.pool.get("liasse.stock.erp").browse(cr,uid,stock_ids)
        for line in stock_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            self.generate_table(cr, uid, ids, group_val, line.code5, context)
            self.generate_table(cr, uid, ids, group_val, line.code6, context)
            self.generate_table(cr, uid, ids, group_val, line.code7, context)
            
        # tva
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(40)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        tva_ids = self.pool.get("liasse.tva.erp").search(cr,uid,[],limit=1)
        tva_obj = self.pool.get("liasse.tva.erp").browse(cr,uid,tva_ids)
        for line in tva_obj:
            self.generate_table(cr, uid, ids, group_val, line.tva_facsd, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_faco, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_facd, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_facsf, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_recsd, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_reco, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_recd, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_recsf, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_charsd, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_charo, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_chard, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_charsf, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_immosd, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_immoo, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_immod, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_immosf, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_totalsd, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_totalo, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_totald, context)
            self.generate_table(cr, uid, ids, group_val, line.tva_totalsf, context)
            
        # immo
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(11)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        immo_ids = self.pool.get("liasse.immo.erp").search(cr,uid,[],limit=1)
        immo_obj = self.pool.get("liasse.immo.erp").browse(cr,uid,immo_ids)
        for line in immo_obj:
            self.generate_table(cr, uid, ids, group_val, line.code0, context)
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            self.generate_table(cr, uid, ids, group_val, line.code5, context)
            self.generate_table(cr, uid, ids, group_val, line.code6, context)
            self.generate_table(cr, uid, ids, group_val, line.code7, context)
            self.generate_table(cr, uid, ids, group_val, line.fp_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.fp_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.fp_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.fp_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.fp_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.fp_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.fp_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.fp_mbf, context)
            self.generate_table(cr, uid, ids, group_val, line.charge_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.charge_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.charge_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.charge_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.charge_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.charge_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.charge_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.charge_mbf, context)
            self.generate_table(cr, uid, ids, group_val, line.prime_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.prime_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.prime_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.prime_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.prime_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.prime_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.prime_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.prime_mbf, context)
            self.generate_table(cr, uid, ids, group_val, line.code8, context)
            self.generate_table(cr, uid, ids, group_val, line.code9, context)
            self.generate_table(cr, uid, ids, group_val, line.code10, context)
            self.generate_table(cr, uid, ids, group_val, line.code11, context)
            self.generate_table(cr, uid, ids, group_val, line.code12, context)
            self.generate_table(cr, uid, ids, group_val, line.code13, context)
            self.generate_table(cr, uid, ids, group_val, line.code14, context)
            self.generate_table(cr, uid, ids, group_val, line.code15, context)
            self.generate_table(cr, uid, ids, group_val, line.immord_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.immord_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.immord_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.immord_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.immord_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.immord_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.immord_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.immord_mbf, context)
            self.generate_table(cr, uid, ids, group_val, line.brevet_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.brevet_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.brevet_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.brevet_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.brevet_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.brevet_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.brevet_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.brevet_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.fond_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.fond_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.fond_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.fond_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.fond_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.fond_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.fond_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.fond_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.autre_incorp_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_incorp_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_incorp_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_incorp_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_incorp_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_incorp_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_incorp_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_incorp_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.code16, context)
            self.generate_table(cr, uid, ids, group_val, line.code17, context)
            self.generate_table(cr, uid, ids, group_val, line.code18, context)
            self.generate_table(cr, uid, ids, group_val, line.code19, context)
            self.generate_table(cr, uid, ids, group_val, line.code20, context)
            self.generate_table(cr, uid, ids, group_val, line.code21, context)
            self.generate_table(cr, uid, ids, group_val, line.code22, context)
            self.generate_table(cr, uid, ids, group_val, line.code23, context)
            
            self.generate_table(cr, uid, ids, group_val, line.terrain_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.terrain_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.terrain_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.terrain_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.terrain_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.terrain_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.terrain_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.terrain_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.constructions_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.constructions_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.constructions_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.constructions_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.constructions_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.constructions_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.constructions_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.constructions_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.inst_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.inst_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.inst_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.inst_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.inst_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.inst_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.inst_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.inst_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.mat_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.mat_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.mat_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.mat_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.mat_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.mat_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.mat_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.mat_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.mob_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.mob_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.mob_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.mob_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.mob_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.mob_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.mob_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.mob_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.autre_corp_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_corp_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_corp_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_corp_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_corp_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_corp_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_corp_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.autre_corp_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.immocc_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.immocc_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.immocc_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.immocc_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.immocc_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.immocc_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.immocc_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.immocc_mbf, context)
            
            self.generate_table(cr, uid, ids, group_val, line.mati_mb, context)
            self.generate_table(cr, uid, ids, group_val, line.mati_aug_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.mati_aug_pd, context)
            self.generate_table(cr, uid, ids, group_val, line.mati_aug_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.mati_dim_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.mati_dim_ret, context)
            self.generate_table(cr, uid, ids, group_val, line.mati_dim_vir, context)
            self.generate_table(cr, uid, ids, group_val, line.mati_mbf, context)

        # fusion
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(2)
        group_val = etree.SubElement( val_tab, "groupeValeurs")
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        actif_ids = self.pool.get("liasse.fusion.erp").search(cr,uid,[])
        actif_obj = self.pool.get("liasse.fusion.erp").browse(cr,uid,actif_ids)
        for line in actif_obj:
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            self.generate_table(cr, uid, ids, group_val, line.code5, context)
            self.generate_table(cr, uid, ids, group_val, line.code6, context)
            self.generate_table(cr, uid, ids, group_val, line.code7, context)
            self.generate_table_text(cr, uid, ids, group_val, line.code8, context)
        for extra_line in extra_tab_obj:
            self.generate_extra_field(cr, uid, ids, extra_vals, extra_line.code2fusion, context)
            
        # encouragement
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(10)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        encourg_ids = self.pool.get("liasse.encour.erp").search(cr,uid,[],limit=1)
        encourg_obj = self.pool.get("liasse.encour.erp").browse(cr,uid,encourg_ids)
        for line in encourg_obj:
            self.generate_table(cr, uid, ids, group_val, line.code0, context)
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            self.generate_table(cr, uid, ids, group_val, line.code5, context)
            self.generate_table(cr, uid, ids, group_val, line.code6, context)
            self.generate_table(cr, uid, ids, group_val, line.code7, context)
            self.generate_table(cr, uid, ids, group_val, line.code8, context)
            self.generate_table(cr, uid, ids, group_val, line.code9, context)
            self.generate_table(cr, uid, ids, group_val, line.code10, context)
            self.generate_table(cr, uid, ids, group_val, line.code12, context)
            self.generate_table(cr, uid, ids, group_val, line.code13, context)
            self.generate_table(cr, uid, ids, group_val, line.code14, context)
            self.generate_table(cr, uid, ids, group_val, line.code15, context)
            self.generate_table(cr, uid, ids, group_val, line.code16, context)
            self.generate_table(cr, uid, ids, group_val, line.code17, context)
            self.generate_table(cr, uid, ids, group_val, line.code18, context)
            self.generate_table(cr, uid, ids, group_val, line.code19, context)
            self.generate_table(cr, uid, ids, group_val, line.code20, context)
            self.generate_table(cr, uid, ids, group_val, line.code21, context)
            self.generate_table(cr, uid, ids, group_val, line.code22, context)
            self.generate_table(cr, uid, ids, group_val, line.code23, context)
            self.generate_table(cr, uid, ids, group_val, line.code24, context)
            self.generate_table(cr, uid, ids, group_val, line.code25, context)
            self.generate_table(cr, uid, ids, group_val, line.code26, context)
            self.generate_table(cr, uid, ids, group_val, line.code27, context)
            self.generate_table(cr, uid, ids, group_val, line.code28, context)
            self.generate_table(cr, uid, ids, group_val, line.code29, context)
            self.generate_table(cr, uid, ids, group_val, line.code30, context)
            self.generate_table(cr, uid, ids, group_val, line.code31, context)
            self.generate_table(cr, uid, ids, group_val, line.code32, context)
            self.generate_table(cr, uid, ids, group_val, line.code33, context)
            self.generate_table(cr, uid, ids, group_val, line.code34, context)
            self.generate_table(cr, uid, ids, group_val, line.code35, context)
            self.generate_table(cr, uid, ids, group_val, line.code36, context)
            self.generate_table(cr, uid, ids, group_val, line.code37, context)
            self.generate_table(cr, uid, ids, group_val, line.code38, context)
            self.generate_table(cr, uid, ids, group_val, line.code39, context)
            self.generate_table(cr, uid, ids, group_val, line.code40, context)
            self.generate_table(cr, uid, ids, group_val, line.code41, context)
            self.generate_table(cr, uid, ids, group_val, line.code42, context)
            self.generate_table(cr, uid, ids, group_val, line.code43, context)
            self.generate_table(cr, uid, ids, group_val, line.code44, context)
            self.generate_table(cr, uid, ids, group_val, line.code45, context)
            self.generate_table(cr, uid, ids, group_val, line.code46, context)
            self.generate_table(cr, uid, ids, group_val, line.code47, context)
            self.generate_table(cr, uid, ids, group_val, line.code48, context)
            self.generate_table(cr, uid, ids, group_val, line.code49, context)
            self.generate_table(cr, uid, ids, group_val, line.code50, context)
            self.generate_table(cr, uid, ids, group_val, line.code51, context)
            self.generate_table(cr, uid, ids, group_val, line.code52, context)
            self.generate_table(cr, uid, ids, group_val, line.code53, context)
            self.generate_table(cr, uid, ids, group_val, line.code54, context)
            self.generate_table(cr, uid, ids, group_val, line.code55, context)
            self.generate_table(cr, uid, ids, group_val, line.code56, context)
            
        # affectation
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(5)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        affect_ids = self.pool.get("liasse.affect.erp").search(cr,uid,[],limit=1)
        affect_obj = self.pool.get("liasse.affect.erp").browse(cr,uid,affect_ids)
        for line in affect_obj:
            self.generate_table(cr, uid, ids, group_val, line.code0, context)
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            self.generate_table(cr, uid, ids, group_val, line.code5, context)
            self.generate_table(cr, uid, ids, group_val, line.code6, context)
            self.generate_table(cr, uid, ids, group_val, line.code7, context)
            self.generate_table(cr, uid, ids, group_val, line.code8, context)
            self.generate_table(cr, uid, ids, group_val, line.code9, context)
            self.generate_table(cr, uid, ids, group_val, line.code10, context)
            self.generate_table(cr, uid, ids, group_val, line.code12, context)
            
        # credit bail
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(23)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        credit_ids = self.pool.get("liasse.credit.bail.erp").search(cr,uid,[],limit=1)
        credit_obj = self.pool.get("liasse.credit.bail.erp").browse(cr,uid,credit_ids)
        for line in credit_obj:
            for creditbail in line.credit_bail_ids:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, creditbail.code0, context)
                self.generate_table_date_ilimite(cr, uid, ids, group_val, creditbail.code1, context)
                self.generate_table_entier_ilimite(cr, uid, ids, group_val, creditbail.code2, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, creditbail.code3, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, creditbail.code4, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, creditbail.code5, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, creditbail.code6, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, creditbail.code7, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, creditbail.code8, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, creditbail.code9, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, creditbail.code10, context)
            self.generate_extra_field(cr, uid, ids, extra_vals, line.extra_field_clos, context)
            
        # pm value
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(38)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        pm_ids = self.pool.get("liasse.pm.value.erp").search(cr,uid,[],limit=1)
        pm_obj = self.pool.get("liasse.pm.value.erp").browse(cr,uid,pm_ids)
        for line in pm_obj:
            for pmline in line.pm_val_ids:
                self.generate_table_date_ilimite(cr, uid, ids, group_val, pmline.code0, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, pmline.code1, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, pmline.code2, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, pmline.code3, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, pmline.code4, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, pmline.code5, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, pmline.code6, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, pmline.code7, context)
            self.generate_extra_field(cr, uid, ids, extra_vals, line.extra_field_clos, context)
            self.generate_table(cr, uid, ids, group_val, line.compte_princ, context)
            self.generate_table(cr, uid, ids, group_val, line.montant_brut, context)
            self.generate_table(cr, uid, ids, group_val, line.amort_cumul, context)
            self.generate_table(cr, uid, ids, group_val, line.val_net_amort, context)
            self.generate_table(cr, uid, ids, group_val, line.prod_cess, context)
            self.generate_table(cr, uid, ids, group_val, line.plus_value, context)
            self.generate_table(cr, uid, ids, group_val, line.moins_value, context)
            
        # Titre de participation
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(39)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        titre_ids = self.pool.get("liasse.titre.particip.erp").search(cr,uid,[],limit=1)
        titre_obj = self.pool.get("liasse.titre.particip.erp").browse(cr,uid,titre_ids)
        for line in titre_obj:
            for titreline in line.titre_particip_ids:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, titreline.code0, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, titreline.code1, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, titreline.code2, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, titreline.code3, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, titreline.code4, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, titreline.code5, context)
                self.generate_table_date_ilimite(cr, uid, ids, group_val, titreline.code6, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, titreline.code7, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, titreline.code8, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, titreline.code9, context)
                
            #self.generate_table_text(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code2, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            self.generate_table(cr, uid, ids, group_val, line.code5, context)
            self.generate_table_date(cr, uid, ids, group_val, line.code6, context)
            self.generate_table(cr, uid, ids, group_val, line.code7, context)
            self.generate_table(cr, uid, ids, group_val, line.code8, context)
            self.generate_table(cr, uid, ids, group_val, line.code9, context)
            
        # repartition capital social
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(41)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        rpcp_ids = self.pool.get("liasse.repart.cs.erp").search(cr,uid,[],limit=1)
        rpcp_obj = self.pool.get("liasse.repart.cs.erp").browse(cr,uid,rpcp_ids)
        for line in rpcp_obj:
            for rpcpline in line.repart_cs_ids:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, rpcpline.code0, context)
                self.generate_table_entier_ilimite(cr, uid, ids, group_val, rpcpline.code1, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, rpcpline.code2, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, rpcpline.code3, context)
                self.generate_table_entier_ilimite(cr, uid, ids, group_val, rpcpline.code4, context)
                self.generate_table_entier_ilimite(cr, uid, ids, group_val, rpcpline.code5, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, rpcpline.code6, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, rpcpline.code7, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, rpcpline.code8, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, rpcpline.code9, context)
                
            self.generate_extra_double_field(cr, uid, ids, extra_vals, line.montant_capital, context)
            
        # Beaux
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(28)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        beaux_ids = self.pool.get("liasse.beaux.erp").search(cr,uid,[],limit=1)
        beaux_obj = self.pool.get("liasse.beaux.erp").browse(cr,uid,beaux_ids)
        for line in beaux_obj:
            for beaux in line.beaux_ids:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, beaux.code0, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, beaux.code1, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, beaux.code2, context)
                self.generate_table_date_ilimite(cr, uid, ids, group_val, beaux.code3, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, beaux.code4, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, beaux.code5, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, beaux.code6, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, beaux.code7, context)
               
            self.generate_table(cr, uid, ids, group_val, line.code0, context)
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            
        # passage
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(7)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        passage_ids = self.pool.get("liasse.passage.erp").search(cr,uid,[],limit=1)
        passage_obj = self.pool.get("liasse.passage.erp").browse(cr,uid,passage_ids)
        for line in passage_obj:
            for passage in line.passages_rfc:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, passage.code0, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, passage.code1, context)
                #self.generate_table_ilimite(cr, uid, ids, group_val, passage.code2, context)
            for passage in line.passages_rfnc:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, passage.code0, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, passage.code1, context)
                #self.generate_table_ilimite(cr, uid, ids, group_val, passage.code2, context)
            for passage in line.passages_dfc:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, passage.code0, context)
                #self.generate_table_ilimite(cr, uid, ids, group_val, passage.code1, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, passage.code2, context)
            for passage in line.passages_dfnc:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, passage.code0, context)
                #self.generate_table_ilimite(cr, uid, ids, group_val, passage.code1, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, passage.code2, context)
                
            self.generate_table(cr, uid, ids, group_val, line.code0, context)
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            self.generate_table(cr, uid, ids, group_val, line.code5, context)
            self.generate_table(cr, uid, ids, group_val, line.code6, context)
            self.generate_table(cr, uid, ids, group_val, line.code7, context)
            self.generate_table(cr, uid, ids, group_val, line.code8, context)
            self.generate_table(cr, uid, ids, group_val, line.code9, context)
            self.generate_table(cr, uid, ids, group_val, line.code10, context)
            self.generate_table(cr, uid, ids, group_val, line.code12, context)
            self.generate_table(cr, uid, ids, group_val, line.code14, context)
            self.generate_table(cr, uid, ids, group_val, line.code16, context)
            self.generate_table(cr, uid, ids, group_val, line.code18, context)
            self.generate_table(cr, uid, ids, group_val, line.code19, context)
            self.generate_table(cr, uid, ids, group_val, line.code20, context)
            self.generate_table(cr, uid, ids, group_val, line.code21, context)
            self.generate_table(cr, uid, ids, group_val, line.code22, context)
            self.generate_table(cr, uid, ids, group_val, line.code23, context)
            self.generate_table(cr, uid, ids, group_val, line.code24, context)
            self.generate_table(cr, uid, ids, group_val, line.code25, context)
            self.generate_table(cr, uid, ids, group_val, line.code26, context)
            
        # Interets
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(27)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        interets_ids = self.pool.get("liasse.interets.erp").search(cr,uid,[],limit=1)
        interets_obj = self.pool.get("liasse.interets.erp").browse(cr,uid,interets_ids)
        for line in interets_obj:
            for interet in line.interets_associe:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code0, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code1, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code2, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code3, context)
                self.generate_table_date_ilimite(cr, uid, ids, group_val, interet.code4, context)
                self.generate_table_entier_ilimite(cr, uid, ids, group_val, interet.code5, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code6, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code7, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code8, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code9, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code10, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code11, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code12, context)
                
            for interet in line.interets_tier:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code0, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code1, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code2, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code3, context)
                self.generate_table_date_ilimite(cr, uid, ids, group_val, interet.code4, context)
                self.generate_table_entier_ilimite(cr, uid, ids, group_val, interet.code5, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code6, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code7, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code8, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code9, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code10, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, interet.code11, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, interet.code12, context)
                
            self.generate_extra_field(cr, uid, ids, extra_vals, line.code0, context)
            self.generate_table(cr, uid, ids, group_val, line.code1, context)
            self.generate_table(cr, uid, ids, group_val, line.code3, context)
            self.generate_table(cr, uid, ids, group_val, line.code4, context)
            self.generate_table(cr, uid, ids, group_val, line.code5, context)
            self.generate_table(cr, uid, ids, group_val, line.code6, context)

        # Dotation aux amortissement
        val_tab = etree.SubElement( group_val_tab, "ValeursTableau" )
        tableau = etree.SubElement( val_tab, "tableau" )
        id = etree.SubElement( tableau, "id" )
        id.text = str(12)
        group_val = etree.SubElement( val_tab, "groupeValeurs" )
        extra_vals = etree.SubElement( val_tab, "extraFieldvaleurs")
        dotation_ids = self.pool.get("liasse.dotation.erp").search(cr,uid,[],limit=1)
        dotation_obj = self.pool.get("liasse.dotation.erp").browse(cr,uid,dotation_ids)
        for line in dotation_obj:
            for dot in line.dotation_line_ids:
                self.generate_table_text_ilimite(cr, uid, ids, group_val, dot.code0, context)
                self.generate_table_date_ilimite(cr, uid, ids, group_val, dot.code1, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, dot.code2, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, dot.code3, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, dot.code4, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, dot.code5, context)
                self.generate_table_entier_ilimite(cr, uid, ids, group_val, dot.code6, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, dot.code7, context)
                self.generate_table_ilimite(cr, uid, ids, group_val, dot.code8, context)
                self.generate_table_text_ilimite(cr, uid, ids, group_val, dot.code9, context)
                
            self.generate_extra_double_field(cr, uid, ids, extra_vals, line.montant_global, context)
            self.generate_extra_field(cr, uid, ids, extra_vals, line.clos, context)
            self.generate_extra_field(cr, uid, ids, extra_vals, line.date_from, context)
            self.generate_extra_field(cr, uid, ids, extra_vals, line.date_end, context)
            self.generate_table(cr, uid, ids, group_val, line.val_acq, context)
            self.generate_table(cr, uid, ids, group_val, line.val_compt, context)
            self.generate_table(cr, uid, ids, group_val, line.amort_ant, context)
            self.generate_table(cr, uid, ids, group_val, line.amort_ded_e, context)
            self.generate_table(cr, uid, ids, group_val, line.amort_ded_e, context)
            self.generate_table(cr, uid, ids, group_val, line.amort_fe, context)

        xml_data = "%s" % (
                etree.tostring(doc, pretty_print = True, xml_declaration = True, encoding='UTF-8')
                )
        self.write(cr, uid, ids, {
            'output': base64.b64encode(xml_data),}, context=context)
        
        return True 
    
    def generate_fiscale(self, cr, uid, ids, context=None):
        fy=False
        for field in self.browse(cr, uid, ids, context):
                self.report_nouveau(cr, uid, ids, field.exercice_prec, context)
                fy= field.name
        
        start_period, stop_period, start, stop = \
            self._get_start_stop_for_filter('filter_no', fy, None, None, None, None,cr,uid)
        initial_balance_mode = self._get_initial_balance_mode(cr,uid,start_period)
        
        ###        
        self.generate_actif(cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.generate_passif(cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.generate_cpc(cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.update_data_passage(cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.update_data(cr, uid, ids,fy,start,stop,initial_balance_mode, context)
        self.edi_affectation(cr, uid, ids, context)
        self.generate_tfr(cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.generate_caf(cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.generate_det_cpc(cr, uid,ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.generate_amort(cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.generate_provision(cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.generate_stock(cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context)
        self.generate_fusion(cr, uid, ids, context)
        self.update_tva(cr, uid, ids,fy,start, stop, initial_balance_mode, context)
        return True
        
    def generate_actif(self, cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context=None):
        code_conf = self.pool.get('liasse.code.erp')
        actif = self.pool.get('bilan.actif.fiscale.erp')
        bilan_actif = self.pool.get('liasse.bilan.actif.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from bilan_actif_fiscale_erp where balance_id="+str(rec.id))
            fy = rec.name
        for code in bilan_actif_obj:
            total1= self.count_cell(cr, uid, ids, code.code1,fy,start_period, stop_period, initial_balance_mode, context)
            total2= self.count_cell(cr, uid, ids, code.code2,fy,start_period, stop_period, initial_balance_mode, context)
            total3= total1 - total2 
            total4= self.count_cell(cr, uid, ids, code.code4,fy,start_period, stop_period, initial_balance_mode, context)
            code_conf.write(cr,uid,code.code3.id,{'valeur':total3})
            data={}
            data["lib"] = code.lib
            data["code1"]= total1
            data["code2"] = total2
            data["code3"] = total3
            data["code4"] = total4
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True
    
    def generate_passif(self, cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context=None):
        actif = self.pool.get('bilan.passif.fiscale.erp')
        bilan_actif = self.pool.get('liasse.bilan.passif.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from bilan_passif_fiscale_erp where balance_id="+str(rec.id))
        for code in bilan_actif_obj:
            total2= self.count_cell(cr, uid, ids, code.code2,fy,start_period, stop_period, initial_balance_mode, context)
            print ('passif: code2: ',total2,'code: ',code.code2.code)
            total1= self.count_cell(cr, uid, ids, code.code1,fy,start_period, stop_period, initial_balance_mode, context)
            print ('passif: code1: ',total1)
            
            data={}
            data["lib"] = code.lib
            data["code1"]= total1
            data["code2"] = total2
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True
    
    def generate_cpc(self, cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context=None):
        code_conf = self.pool.get('liasse.code.erp')
        actif = self.pool.get('cpc.fiscale.erp')
        bilan_actif = self.pool.get('liasse.cpc.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from cpc_fiscale_erp where balance_id="+str(rec.id))
        for code in bilan_actif_obj:
            total1 = self.count_cell(cr, uid, ids, code.code1,fy,start_period, stop_period, initial_balance_mode, context)
            total2= self.count_cell(cr, uid, ids, code.code2,fy,start_period, stop_period, initial_balance_mode, context)
            total4= self.count_cell(cr, uid, ids, code.code4,fy,start_period, stop_period, initial_balance_mode, context)
            total3= total1 + total2
            code_conf.write(cr,uid,code.code3.id,{'valeur':total3})
                                                
            data={}
            data["lib"] = code.lib
            data["code1"]= total1
            data["code2"] = total2
            data["code3"] = total3
            data["code4"] = total4
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True
    
    def generate_det_cpc(self, cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context=None):
        actif = self.pool.get('det.cpc.fiscale.erp')
        bilan_actif = self.pool.get('liasse.det.cpc.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from det_cpc_fiscale_erp where balance_id="+str(rec.id))
        for code in bilan_actif_obj:
            total2= self.count_cell(cr, uid, ids, code.code2,fy,start_period, stop_period, initial_balance_mode, context)
            total1= self.count_cell(cr, uid, ids, code.code1,fy,start_period, stop_period, initial_balance_mode, context)
                                                
            data={}
            data["poste"] = code.poste
            data["lib"] = code.lib
            data["code1"]= total1
            data["code2"] = total2
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True

    def generate_tfr(self, cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context=None):
        actif = self.pool.get('tfr.fiscale.erp')
        bilan_actif = self.pool.get('liasse.tfr.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from tfr_fiscale_erp where balance_id="+str(rec.id))
        for code in bilan_actif_obj:
            total2= self.count_cell(cr, uid, ids, code.code2,fy,start_period, stop_period, initial_balance_mode, context)
            total1= self.count_cell(cr, uid, ids, code.code1,fy,start_period, stop_period, initial_balance_mode, context)
                                                
            data={}
            data["lettre"] = code.lettre
            data["num"] = code.num
            data["lib"] = code.lib
            data["op"] = code.op
            data["code1"]= total1
            data["code2"] = total2
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True
    
    def generate_caf(self, cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context=None):
        actif = self.pool.get('caf.fiscale.erp')
        bilan_actif = self.pool.get('liasse.caf.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from caf_fiscale_erp where balance_id="+str(rec.id))
        for code in bilan_actif_obj:
            total2= self.count_cell(cr, uid, ids, code.code2,fy,start_period, stop_period, initial_balance_mode, context)
            total1= self.count_cell(cr, uid, ids, code.code1,fy,start_period, stop_period, initial_balance_mode, context)
                                                
            data={}
            data["lettre"] = code.lettre
            data["num"] = code.num
            data["op"] = code.op
            data["lib"] = code.lib
            data["code1"]= total1
            data["code2"] = total2
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True
    
    def generate_amort(self, cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context=None):
        actif = self.pool.get('amort.fiscale.erp')
        bilan_actif = self.pool.get('liasse.amort.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from amort_fiscale_erp where balance_id="+str(rec.id))
        for code in bilan_actif_obj:
            total1 = self.count_init(cr, uid, ids, code.code1, context)
            total2= self.count_cell(cr, uid, ids, code.code2,fy,start_period, stop_period, initial_balance_mode, context)
            total4 = self.count_cell(cr, uid, ids, code.code4,fy,start_period, stop_period, initial_balance_mode, context)
            total3 = total1 + total2 - total4 
                                                
            data={}
            data["lib"] = code.lib
            data["code1"]= total1
            data["code2"] = total2
            data["code3"]= total3
            data["code4"] = total4
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True

    def generate_provision(self, cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context=None):
        actif = self.pool.get('provision.fiscale.erp')
        bilan_actif = self.pool.get('liasse.provision.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from provision_fiscale_erp where balance_id="+str(rec.id))
        for code in bilan_actif_obj:
            total1 = self.count_init(cr, uid, ids, code.code1, context)
            total2 = self.count_cell(cr, uid, ids, code.code2,fy,start_period, stop_period, initial_balance_mode, context)
            total3 = self.count_cell(cr, uid, ids, code.code3,fy,start_period, stop_period, initial_balance_mode, context)
            total4 = self.count_cell(cr, uid, ids, code.code4,fy,start_period, stop_period, initial_balance_mode, context)
            total5 = self.count_cell(cr, uid, ids, code.code5,fy,start_period, stop_period, initial_balance_mode, context)
            total6 = self.count_cell(cr, uid, ids, code.code6,fy,start_period, stop_period, initial_balance_mode, context)
            total7 = self.count_cell(cr, uid, ids, code.code7,fy,start_period, stop_period, initial_balance_mode, context)
                    
            total8= total1 + total2 + total3 + total4 - total5 - total6 - total7
            total8 = abs(total8) 
                                                
            data={}
            data["lib"] = code.lib
            data["code1"]= total1
            data["code2"] = total2
            data["code3"]= total3
            data["code4"] = total4
            data["code5"]= total5
            data["code6"] = total6
            data["code7"]= total7
            data["code8"] = total8
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True

    def generate_stock(self, cr, uid, ids,fy,start_period, stop_period, initial_balance_mode, context=None):
        actif = self.pool.get('stock.fiscale.erp')
        bilan_actif = self.pool.get('liasse.stock.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from stock_fiscale_erp where balance_id="+str(rec.id))
        for code in bilan_actif_obj:
            total4 = self.count_cell(cr, uid, ids, code.code4,fy,start_period, stop_period, initial_balance_mode, context)
            total5 = self.count_cell(cr, uid, ids, code.code5,fy,start_period, stop_period, initial_balance_mode, context)
            total1 = self.count_cell(cr, uid, ids, code.code1,fy,start_period, stop_period, initial_balance_mode, context)
            total2 = self.count_cell(cr, uid, ids, code.code2,fy,start_period, stop_period, initial_balance_mode, context)

            
            total3= total1 - total2
            total6= total4-total5
            total7= total3 - total6 
                                                            
            data={}
            data["lib"] = code.lib
            data["code1"]= total1
            data["code2"] = total2
            data["code3"]= total3
            data["code4"] = total4
            data["code5"]= total5
            data["code6"] = total6
            data["code7"]= total7
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True
    
    def generate_fusion(self, cr, uid, ids, context=None):
        actif = self.pool.get('fusion.fiscale.erp')
        bilan_actif = self.pool.get('liasse.fusion.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        for rec in self.browse(cr,uid,ids):
            cr.execute("delete from fusion_fiscale_erp where balance_id="+str(rec.id))
        for code in bilan_actif_obj:
            total1 = 0
            total2 = 0
            total3 = 0
            total4 = 0
            total5 = 0
            total6 = 0
            total7 = 0
            total8 = ''
                                                                        
            data={}
            data["lib"] = code.lib
            data["code1"]= total1
            data["code2"] = total2
            data["code3"]= total3
            data["code4"] = total4
            data["code5"]= total5
            data["code6"] = total6
            data["code7"]= total7
            data["code8"]= total8
            data["code0"] = code.id
            data["type"] = code.type
            data["sequence"] = code.sequence
            for rec in self.browse(cr,uid,ids):
                data["balance_id"] = rec.id
            actif.create(cr, uid,data)
        return True
    
    def edi_actif(self, cr, uid, ids, context=None):
        actif = self.pool.get('bilan.actif.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
                code_conf.write(cr,uid,ac.code0.code3.id,{'valeur':ac.code3})
                code_conf.write(cr,uid,ac.code0.code4.id,{'valeur':ac.code4})
        return True
    
    def edi_passif(self, cr, uid, ids, context=None):
        actif = self.pool.get('bilan.passif.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
        return True
    
    def edi_cpc(self, cr, uid, ids, context=None):
        actif = self.pool.get('cpc.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
                code_conf.write(cr,uid,ac.code0.code3.id,{'valeur':ac.code3})
                code_conf.write(cr,uid,ac.code0.code4.id,{'valeur':ac.code4})
        return True
    
    def edi_det_cpc(self, cr, uid, ids, context=None):
        extra_tab = self.pool.get("liasse.extra.field.erp")
        extra_tab_ids = extra_tab.search(cr,uid,[],limit=1)
        extra_tab_obj = extra_tab.browse(cr,uid,extra_tab_ids,context)
        actif = self.pool.get('det.cpc.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ex in extra_tab_obj:
                code_conf.write(cr,uid,ex.code0cpc.id,{'valeur':rec.date_end})
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
        return True
    
    def edi_tfr(self, cr, uid, ids, context=None):
        actif = self.pool.get('tfr.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
        return True
    
    def edi_caf(self, cr, uid, ids, context=None):
        actif = self.pool.get('caf.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
        return True
    
    def edi_amort(self, cr, uid, ids, context=None):
        actif = self.pool.get('amort.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
                code_conf.write(cr,uid,ac.code0.code3.id,{'valeur':ac.code3})
                code_conf.write(cr,uid,ac.code0.code4.id,{'valeur':ac.code4})
        return True
    
    def edi_provision(self, cr, uid, ids, context=None):
        extra_tab = self.pool.get("liasse.extra.field.erp")
        extra_tab_ids = extra_tab.search(cr,uid,[],limit=1)
        extra_tab_obj = extra_tab.browse(cr,uid,extra_tab_ids,context)
        actif = self.pool.get('provision.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ex in extra_tab_obj:
                code_conf.write(cr,uid,ex.code1prov.id,{'valeur':rec.date_end})
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
                code_conf.write(cr,uid,ac.code0.code3.id,{'valeur':ac.code3})
                code_conf.write(cr,uid,ac.code0.code4.id,{'valeur':ac.code4})
                code_conf.write(cr,uid,ac.code0.code5.id,{'valeur':ac.code5})
                code_conf.write(cr,uid,ac.code0.code6.id,{'valeur':ac.code6})
                code_conf.write(cr,uid,ac.code0.code7.id,{'valeur':ac.code7})
                code_conf.write(cr,uid,ac.code0.code8.id,{'valeur':ac.code8})
        return True
    
    def edi_stock(self, cr, uid, ids, context=None):
        actif = self.pool.get('stock.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
                code_conf.write(cr,uid,ac.code0.code3.id,{'valeur':ac.code3})
                code_conf.write(cr,uid,ac.code0.code4.id,{'valeur':ac.code4})
                code_conf.write(cr,uid,ac.code0.code5.id,{'valeur':ac.code5})
                code_conf.write(cr,uid,ac.code0.code6.id,{'valeur':ac.code6})
                code_conf.write(cr,uid,ac.code0.code7.id,{'valeur':ac.code7})
        return True
    
    def edi_fusion(self, cr, uid, ids, context=None):
        extra_tab = self.pool.get("liasse.extra.field.erp")
        extra_tab_ids = extra_tab.search(cr,uid,[],limit=1)
        extra_tab_obj = extra_tab.browse(cr,uid,extra_tab_ids,context)
        actif = self.pool.get('fusion.fiscale.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            actif_ids = actif.search(cr,uid,[('balance_id','=',rec.id)])
            actif_obj = actif.browse(cr,uid,actif_ids)
            for ex in extra_tab_obj:
                code_conf.write(cr,uid,ex.code2fusion.id,{'valeur':rec.date_end})
            for ac in actif_obj:
                code_conf.write(cr,uid,ac.code0.code1.id,{'valeur':ac.code1})
                code_conf.write(cr,uid,ac.code0.code2.id,{'valeur':ac.code2})
                code_conf.write(cr,uid,ac.code0.code3.id,{'valeur':ac.code3})
                code_conf.write(cr,uid,ac.code0.code4.id,{'valeur':ac.code4})
                code_conf.write(cr,uid,ac.code0.code5.id,{'valeur':ac.code5})
                code_conf.write(cr,uid,ac.code0.code6.id,{'valeur':ac.code6})
                code_conf.write(cr,uid,ac.code0.code7.id,{'valeur':ac.code7})
                code_conf.write(cr,uid,ac.code0.code8.id,{'valeur':ac.code8})
        return True

    def edi_credit_bail(self, cr, uid, ids, context=None):
        credit_bail_data = self.pool.get('credi.bail.erp')
        credit_bail_edi = self.pool.get('liasse.credit.bail.erp')
        code_conf = self.pool.get('liasse.code.erp')
        code_conf_valeur = self.pool.get('liasse.code.line.erp')
        for rec in self.browse(cr,uid,ids):
            credit_bail_data_ids = credit_bail_data.search(cr,uid,[('balance_id','=',rec.id)])
            credit_bail_edi_ids = credit_bail_edi.search(cr,uid,[],limit=1)
            credit_bail_data_obj = credit_bail_data.browse(cr,uid,credit_bail_data_ids)
            credit_bail_edi_obj = credit_bail_edi.browse(cr,uid,credit_bail_edi_ids)
            if credit_bail_edi_obj:
                code_conf.write(cr,uid,credit_bail_edi_obj.extra_field_clos.id,{'valeur':rec.date_end})
                if credit_bail_edi_obj.credit_bail_ids:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code1.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code2.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code3.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code4.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code5.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code6.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code7.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code8.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code9.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.credit_bail_ids[0].code10.id))
            row =1
            for data in credit_bail_data_obj:
                data1=data.rubrique
                data2=data.date_first_ech
                data3=data.duree_contrat
                data4=data.val_estime
                data5=data.duree_theo
                data6=data.cumul_prec
                data7=data.montant_rev
                data8=data.rev_moins
                data9=data.rev_plus
                data10=data.prix_achat
                data11=data.observation
                if credit_bail_edi_obj:
                    if credit_bail_edi_obj.credit_bail_ids:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code1.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code2.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data4, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code3.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data5, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code4.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data6, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code5.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data7, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code6.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data8, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code7.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data9, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code8.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data10, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code9.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data11, 'ligne':row,'code_id':credit_bail_edi_obj.credit_bail_ids[0].code10.id})
                    else:
                        break
                row +=1
                
        return True
    

    def edi_pm_value(self, cr, uid, ids, context=None):
        pm_value_data = self.pool.get('pm.value.erp')
        pm_value_edi = self.pool.get('liasse.pm.value.erp')
        code_conf = self.pool.get('liasse.code.erp')
        code_conf_valeur = self.pool.get('liasse.code.line.erp')
        for rec in self.browse(cr,uid,ids):
            credit_bail_data_ids = pm_value_data.search(cr,uid,[('balance_id','=',rec.id)])
            credit_bail_edi_ids = pm_value_edi.search(cr,uid,[],limit=1)
            credit_bail_data_obj = pm_value_data.browse(cr,uid,credit_bail_data_ids)
            credit_bail_edi_obj = pm_value_edi.browse(cr,uid,credit_bail_edi_ids)
            if credit_bail_edi_obj:
                code_conf.write(cr,uid,credit_bail_edi_obj[0].extra_field_clos.id,{'valeur':rec.date_end})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].compte_princ.id,{'valeur':rec.pm_compte_princ})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].montant_brut.id,{'valeur':rec.pm_montant_brut})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].amort_cumul.id,{'valeur':rec.pm_amort_cumul})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].val_net_amort.id,{'valeur':rec.pm_val_net_amort})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].prod_cess.id,{'valeur':rec.pm_prod_cess})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].plus_value.id,{'valeur':rec.pm_plus_value})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].moins_value.id,{'valeur':rec.pm_moins_value})
                
                if credit_bail_edi_obj.pm_val_ids:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.pm_val_ids[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.pm_val_ids[0].code1.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.pm_val_ids[0].code2.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.pm_val_ids[0].code3.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.pm_val_ids[0].code4.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.pm_val_ids[0].code5.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.pm_val_ids[0].code6.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.pm_val_ids[0].code7.id))
            row =1
            for data in credit_bail_data_obj:
                data1=data.date_cession
                data2=data.compte_princ
                data3=data.montant_brut
                data4=data.amort_cumul
                data5=data.val_net_amort
                data6=data.prod_cess
                data7=data.plus_value
                data8=data.moins_value  
                
                if credit_bail_edi_obj:
                    if credit_bail_edi_obj.pm_val_ids:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':credit_bail_edi_obj.pm_val_ids[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':credit_bail_edi_obj.pm_val_ids[0].code1.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':credit_bail_edi_obj.pm_val_ids[0].code2.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data4, 'ligne':row,'code_id':credit_bail_edi_obj.pm_val_ids[0].code3.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data5, 'ligne':row,'code_id':credit_bail_edi_obj.pm_val_ids[0].code4.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data6, 'ligne':row,'code_id':credit_bail_edi_obj.pm_val_ids[0].code5.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data7, 'ligne':row,'code_id':credit_bail_edi_obj.pm_val_ids[0].code6.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data8, 'ligne':row,'code_id':credit_bail_edi_obj.pm_val_ids[0].code7.id})
                    else:
                        break
                row +=1
            
        return True
    
    def edi_dotation(self, cr, uid, ids, context=None):
        pm_value_data = self.pool.get('dotation.amort.erp')
        pm_value_edi = self.pool.get('liasse.dotation.erp')
        code_conf = self.pool.get('liasse.code.erp')
        code_conf_valeur = self.pool.get('liasse.code.line.erp')
        for rec in self.browse(cr,uid,ids):
            credit_bail_data_ids = pm_value_data.search(cr,uid,[('balance_id','=',rec.id)])
            credit_bail_edi_ids = pm_value_edi.search(cr,uid,[],limit=1)
            credit_bail_data_obj = pm_value_data.browse(cr,uid,credit_bail_data_ids)
            credit_bail_edi_obj = pm_value_edi.browse(cr,uid,credit_bail_edi_ids)
            if credit_bail_edi_obj:
                code_conf.write(cr,uid,credit_bail_edi_obj.clos.id,{'valeur':rec.date_end})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].date_from.id,{'valeur':rec.date_start})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].date_end.id,{'valeur':rec.date_end})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].montant_global.id,{'valeur':rec.montant_dot})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].val_acq.id,{'valeur':rec.val_acq})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].val_compt.id,{'valeur':rec.val_compt})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].amort_ant.id,{'valeur':rec.amort_ant})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].amort_ded_et.id,{'valeur':rec.amort_ded_et})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].amort_ded_e.id,{'valeur':rec.amort_ded_e})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].amort_fe.id,{'valeur':rec.amort_fe})
                
                if credit_bail_edi_obj.dotation_line_ids:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code1.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code2.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code3.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code4.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code5.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code6.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code7.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code8.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.dotation_line_ids[0].code9.id))
            row =1
            for data in credit_bail_data_obj:
                data1=data.code0
                data2=data.code1
                data3=data.code2
                data4=data.code3
                data5=data.code4
                data6=data.code5
                data7=data.code6
                data8=data.code7
                data9=data.code8
                data10=data.code9 
                
                if credit_bail_edi_obj:
                    if credit_bail_edi_obj.dotation_line_ids:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code1.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code2.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data4, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code3.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data5, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code4.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data6, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code5.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data7, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code6.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data8, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code7.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data9, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code8.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data10, 'ligne':row,'code_id':credit_bail_edi_obj.dotation_line_ids[0].code9.id})
                    else:
                        break
                row +=1
            
        return True
    
    def edi_titre_particp(self, cr, uid, ids, context=None):
        pm_value_data = self.pool.get('titre.particip.erp')
        pm_value_edi = self.pool.get('liasse.titre.particip.erp')
        code_conf = self.pool.get('liasse.code.erp')
        code_conf_valeur = self.pool.get('liasse.code.line.erp')
        for rec in self.browse(cr,uid,ids):
            credit_bail_data_ids = pm_value_data.search(cr,uid,[('balance_id','=',rec.id)])
            credit_bail_edi_ids = pm_value_edi.search(cr,uid,[],limit=1)
            credit_bail_data_obj = pm_value_data.browse(cr,uid,credit_bail_data_ids)
            credit_bail_edi_obj = pm_value_edi.browse(cr,uid,credit_bail_edi_ids)
            if credit_bail_edi_obj:
                code_conf.write(cr,uid,credit_bail_edi_obj[0].code2.id,{'valeur':rec.tp_capit_social})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].code3.id,{'valeur':rec.tp_particip_cap})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].code4.id,{'valeur':rec.tp_prix_global})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].code5.id,{'valeur':rec.tp_val_compt})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].code6.id,{'valeur':rec.tp_extr_date})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].code7.id,{'valeur':rec.tp_extr_situation})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].code8.id,{'valeur':rec.tp_extr_resultat})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].code9.id,{'valeur':rec.tp_prod_inscrit})
                
                if credit_bail_edi_obj.titre_particip_ids:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code1.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code2.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code3.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code4.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code5.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code6.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code7.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code8.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.titre_particip_ids[0].code9.id))
            row =1
            for data in credit_bail_data_obj:
                data1=data.raison_soc
                data2=data.sect_activity
                data3=data.capit_social
                data4=data.particip_cap
                data5=data.prix_global
                data6=data.val_compt
                data7=data.extr_date
                data8=data.extr_situation
                data9=data.extr_resultat 
                data10=data.prod_inscrit  
                
                if credit_bail_edi_obj:
                    if credit_bail_edi_obj.titre_particip_ids:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code1.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code2.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data4, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code3.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data5, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code4.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data6, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code5.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data7, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code6.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data8, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code7.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data9, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code8.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data10, 'ligne':row,'code_id':credit_bail_edi_obj.titre_particip_ids[0].code9.id})
                    else:
                        break
                row +=1
            
        return True
    
    def edi_repart_cs(self, cr, uid, ids, context=None):
        pm_value_data = self.pool.get('repart.cs.erp')
        pm_value_edi = self.pool.get('liasse.repart.cs.erp')
        code_conf = self.pool.get('liasse.code.erp')
        code_conf_valeur = self.pool.get('liasse.code.line.erp')
        for rec in self.browse(cr,uid,ids):
            credit_bail_data_ids = pm_value_data.search(cr,uid,[('balance_id','=',rec.id)])
            credit_bail_edi_ids = pm_value_edi.search(cr,uid,[],limit=1)
            credit_bail_data_obj = pm_value_data.browse(cr,uid,credit_bail_data_ids)
            credit_bail_edi_obj = pm_value_edi.browse(cr,uid,credit_bail_edi_ids)
            if credit_bail_edi_obj:
                code_conf.write(cr,uid,credit_bail_edi_obj[0].montant_capital.id,{'valeur':rec.montant_rcs})
                if credit_bail_edi_obj.repart_cs_ids:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code1.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code2.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code3.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code4.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code5.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code6.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code7.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code8.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.repart_cs_ids[0].code9.id))
            row =1
            for data in credit_bail_data_obj:
                data1=data.name
                data2=data.id_fisc
                data3=data.cin
                data4=data.adress
                data5=data.number_prec
                data6=data.number_actual
                data7=data.val_nom
                data8=data.val_sousc
                data9=data.val_appele
                data10=data.val_lib
                
                if credit_bail_edi_obj:
                    if credit_bail_edi_obj.repart_cs_ids:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code1.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code2.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data4, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code3.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data5, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code4.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data6, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code5.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data7, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code6.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data8, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code7.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data9, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code8.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data10, 'ligne':row,'code_id':credit_bail_edi_obj.repart_cs_ids[0].code9.id})
                    else:
                        break
                row +=1
            
        return True
    
    def edi_interets(self, cr, uid, ids, context=None):
        in_data = self.pool.get('interets.erp')
        in_edi = self.pool.get('liasse.interets.erp')
        code_conf = self.pool.get('liasse.code.erp')
        code_conf_valeur = self.pool.get('liasse.code.line.erp')
        for rec in self.browse(cr,uid,ids):
            ina_data_ids = in_data.search(cr,uid,[("type","=","0"),('balance_id','=',rec.id)])
            int_data_ids = in_data.search(cr,uid,[("type","=","1"),('balance_id','=',rec.id)])
            in_edi_ids = in_edi.search(cr,uid,[],limit=1)
            ina_data_obj = in_data.browse(cr,uid,ina_data_ids)
            int_data_obj = in_data.browse(cr,uid,int_data_ids)
            in_edi_obj = in_edi.browse(cr,uid,in_edi_ids)
            if in_edi_obj:
                code_conf.write(cr,uid,in_edi_obj.code0.id,{'valeur':rec.date_end})
                code_conf.write(cr,uid,in_edi_obj.code1.id,{'valeur':rec.in_mont_pretl})
                code_conf.write(cr,uid,in_edi_obj.code2.id,{'valeur':rec.in_charge_global})
                code_conf.write(cr,uid,in_edi_obj.code3.id,{'valeur':rec.in_remb_princ})
                code_conf.write(cr,uid,in_edi_obj.code4.id,{'valeur':rec.in_remb_inter})
                code_conf.write(cr,uid,in_edi_obj.code5.id,{'valeur':rec.in_remb_actual_princ})
                code_conf.write(cr,uid,in_edi_obj.code6.id,{'valeur':rec.in_remb_actual_inter})
                
                if in_edi_obj.interets_associe:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code1.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code2.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code3.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code4.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code5.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code6.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code7.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code8.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code9.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code10.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code11.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_associe[0].code12.id))
                if in_edi_obj.interets_tier:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code1.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code2.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code3.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code4.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code5.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code6.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code7.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code8.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code9.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code10.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code11.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(in_edi_obj.interets_tier[0].code12.id))
            row =1
            for data in ina_data_obj:
                data1=data.name
                data2=data.adress
                data3=data.cin
                data4=data.mont_pretl
                data5=data.date_pret
                data6=data.duree_mois
                data7=data.taux_inter
                data8=data.charge_global
                data9=data.remb_princ
                data10=data.remb_inter
                data11=data.remb_actual_princ
                data12=data.remb_actual_inter
                data13=data.observation
                
                if in_edi_obj:
                    if in_edi_obj.interets_associe:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code1.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code2.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data4, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code3.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data5, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code4.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data6, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code5.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data7, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code6.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data8, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code7.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data9, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code8.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data10, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code9.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data11, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code10.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data12, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code11.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data13, 'ligne':row,'code_id':in_edi_obj.interets_associe[0].code12.id})
                    else:
                        break
                row +=1

            row =1
            for data in int_data_obj:
                data1=data.name
                data2=data.adress
                data3=data.cin
                data4=data.mont_pretl
                data5=data.date_pret
                data6=data.duree_mois
                data7=data.taux_inter
                data8=data.charge_global
                data9=data.remb_princ
                data10=data.remb_inter
                data11=data.remb_actual_princ
                data12=data.remb_actual_inter
                data13=data.observation
                
                if in_edi_obj:
                    if in_edi_obj.interets_tier:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code1.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code2.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data4, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code3.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data5, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code4.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data6, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code5.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data7, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code6.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data8, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code7.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data9, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code8.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data10, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code9.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data11, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code10.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data12, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code11.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data13, 'ligne':row,'code_id':in_edi_obj.interets_tier[0].code12.id})
                    else:
                        break
                row +=1
            
            
        return True
    
    def edi_beaux(self, cr, uid, ids, context=None):
        pm_value_data = self.pool.get('beaux.erp')
        pm_value_edi = self.pool.get('liasse.beaux.erp')
        code_conf = self.pool.get('liasse.code.erp')
        code_conf_valeur = self.pool.get('liasse.code.line.erp')
        for rec in self.browse(cr,uid,ids):
            credit_bail_data_ids = pm_value_data.search(cr,uid,[('balance_id','=',rec.id)])
            credit_bail_edi_ids = pm_value_edi.search(cr,uid,[],limit=1)
            credit_bail_data_obj = pm_value_data.browse(cr,uid,credit_bail_data_ids)
            credit_bail_edi_obj = pm_value_edi.browse(cr,uid,credit_bail_edi_ids)
            if credit_bail_edi_obj:
                code_conf.write(cr,uid,credit_bail_edi_obj[0].code0.id,{'valeur':rec.bx_mont_pretl})
                code_conf.write(cr, uid,credit_bail_edi_obj[0].code1.id,{'valeur':rec.bx_charge_global})
                if credit_bail_edi_obj.beaux_ids:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.beaux_ids[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.beaux_ids[0].code1.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.beaux_ids[0].code2.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.beaux_ids[0].code3.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.beaux_ids[0].code4.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.beaux_ids[0].code5.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.beaux_ids[0].code6.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(credit_bail_edi_obj.beaux_ids[0].code7.id))
            row =1
            for data in credit_bail_data_obj:
                data1=data.nature
                data2=data.lieu
                data3=data.name
                data4=data.date_loc
                data5=data.mont_annuel
                data6=data.mont_loyer
                data7=data.nature_bail
                data8=data.nature_periode    
                
                if credit_bail_edi_obj:
                    if credit_bail_edi_obj.beaux_ids:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':credit_bail_edi_obj.beaux_ids[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':credit_bail_edi_obj.beaux_ids[0].code1.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':credit_bail_edi_obj.beaux_ids[0].code2.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data4, 'ligne':row,'code_id':credit_bail_edi_obj.beaux_ids[0].code3.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data5, 'ligne':row,'code_id':credit_bail_edi_obj.beaux_ids[0].code4.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data6, 'ligne':row,'code_id':credit_bail_edi_obj.beaux_ids[0].code5.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data7, 'ligne':row,'code_id':credit_bail_edi_obj.beaux_ids[0].code6.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data8, 'ligne':row,'code_id':credit_bail_edi_obj.beaux_ids[0].code7.id})
                    else:
                        break
                row +=1
            
        return True
    
    def edi_passage(self, cr, uid, ids, context=None):
        ps_data = self.pool.get('passage.erp')
        ps_edi = self.pool.get('liasse.passage.erp')
        code_conf = self.pool.get('liasse.code.erp')
        code_conf_valeur = self.pool.get('liasse.code.line.erp')
        for rec in self.browse(cr,uid,ids):
            psrfc_data_ids = ps_data.search(cr,uid,[("type","=","0"),('balance_id','=',rec.id)])
            psrfnc_data_ids = ps_data.search(cr,uid,[("type","=","1"),('balance_id','=',rec.id)])
            psdfc_data_ids = ps_data.search(cr,uid,[("type","=","2"),('balance_id','=',rec.id)])
            psdfnc_data_ids = ps_data.search(cr,uid,[("type","=","3"),('balance_id','=',rec.id)])
            ps_edi_ids = ps_edi.search(cr,uid,[],limit=1)
            psrfc_data_obj = ps_data.browse(cr,uid,psrfc_data_ids)
            psrfnc_data_obj = ps_data.browse(cr,uid,psrfnc_data_ids)
            psdfc_data_obj = ps_data.browse(cr,uid,psdfc_data_ids)
            psdfnc_data_obj = ps_data.browse(cr,uid,psdfnc_data_ids)
            ps_edi_obj = ps_edi.browse(cr,uid,ps_edi_ids)
            if ps_edi_obj:
                code_conf.write(cr, uid,ps_edi_obj[0].code0.id,{'valeur':rec.p_benifice_p})
                code_conf.write(cr, uid,ps_edi_obj[0].code1.id,{'valeur':rec.p_benifice_m})
                code_conf.write(cr, uid,ps_edi_obj[0].code2.id,{'valeur':rec.p_perte_p})
                code_conf.write(cr, uid,ps_edi_obj[0].code3.id,{'valeur':rec.p_perte_m})
                code_conf.write(cr, uid,ps_edi_obj[0].code4.id,{'valeur':rec.p_total_montantp})
                code_conf.write(cr, uid,ps_edi_obj[0].code5.id,{'valeur':rec.p_total_montantm})
                code_conf.write(cr, uid,ps_edi_obj[0].code6.id,{'valeur':rec.p_benificebp})
                code_conf.write(cr, uid,ps_edi_obj[0].code7.id,{'valeur':rec.p_benificebm})
                code_conf.write(cr, uid,ps_edi_obj[0].code8.id,{'valeur':rec.p_deficitfp})
                code_conf.write(cr, uid,ps_edi_obj[0].code9.id,{'valeur':rec.p_deficitfm})
                code_conf.write(cr, uid,ps_edi_obj[0].code10.id,{'valeur':rec.p_exo4p})
                #code_conf.write(cr, uid,ps_edi_obj[0].code11.id,{'valeur':rec.p_exo4m})
                code_conf.write(cr, uid,ps_edi_obj[0].code12.id,{'valeur':rec.p_exo3p})
                #code_conf.write(cr, uid,ps_edi_obj[0].code13.id,{'valeur':rec.p_exo3m})
                code_conf.write(cr, uid,ps_edi_obj[0].code14.id,{'valeur':rec.p_exo2p})
                #code_conf.write(cr, uid,ps_edi_obj[0].code15.id,{'valeur':rec.p_exo2m})
                code_conf.write(cr, uid,ps_edi_obj[0].code16.id,{'valeur':rec.p_exo1p})
                #code_conf.write(cr, uid,ps_edi_obj[0].code17.id,{'valeur':rec.p_exo1m})
                code_conf.write(cr, uid,ps_edi_obj[0].code18.id,{'valeur':rec.p_benificenfp})
                code_conf.write(cr, uid,ps_edi_obj[0].code19.id,{'valeur':rec.p_benificenfm})
                code_conf.write(cr, uid,ps_edi_obj[0].code20.id,{'valeur':rec.p_deficitnfp})
                code_conf.write(cr, uid,ps_edi_obj[0].code21.id,{'valeur':rec.p_deficitnfm})
                code_conf.write(cr, uid,ps_edi_obj[0].code22.id,{'valeur':rec.p_cumulafdm})
                code_conf.write(cr, uid,ps_edi_obj[0].code23.id,{'valeur':rec.p_exo4cumulp})
                code_conf.write(cr, uid,ps_edi_obj[0].code24.id,{'valeur':rec.p_exo3cumulp})
                code_conf.write(cr, uid,ps_edi_obj[0].code25.id,{'valeur':rec.p_exo2cumulp})
                code_conf.write(cr, uid,ps_edi_obj[0].code26.id,{'valeur':rec.p_exo1cumulp})
                
                if ps_edi_obj.passages_rfc:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(ps_edi_obj.passages_rfc[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(ps_edi_obj.passages_rfc[0].code1.id))
                if ps_edi_obj.passages_rfnc:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(ps_edi_obj.passages_rfnc[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(ps_edi_obj.passages_rfnc[0].code1.id))
                if ps_edi_obj.passages_dfc:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(ps_edi_obj.passages_dfc[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(ps_edi_obj.passages_dfc[0].code2.id))
                
                if ps_edi_obj.passages_dfnc:
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(ps_edi_obj.passages_dfnc[0].code0.id))
                    cr.execute('delete from liasse_code_line_erp where code_id='+str(ps_edi_obj.passages_dfnc[0].code2.id))
            row =1
            for data in psrfc_data_obj:
                data1=data.name
                data2=data.montant1
                data3=data.montant2
                
                if ps_edi_obj:
                    if ps_edi_obj.passages_rfc:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':ps_edi_obj.passages_rfc[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':ps_edi_obj.passages_rfc[0].code1.id})
                    else:
                        break
                row +=1

            row =1
            for data in psrfnc_data_obj:
                data1=data.name
                data2=data.montant1
                data3=data.montant2
                
                if ps_edi_obj:
                    if ps_edi_obj.passages_rfnc:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':ps_edi_obj.passages_rfnc[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data2, 'ligne':row,'code_id':ps_edi_obj.passages_rfnc[0].code1.id})
                    else:
                        break
                row +=1
                
            
            row =1
            for data in psdfc_data_obj:
                data1=data.name
                data2=data.montant1
                data3=data.montant2
                
                if ps_edi_obj:
                    if ps_edi_obj.passages_dfc:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':ps_edi_obj.passages_dfc[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':ps_edi_obj.passages_dfc[0].code2.id})
                    else:
                        break
                row +=1

            for data in psdfnc_data_obj:
                data1=data.name
                data2=data.montant1
                data3=data.montant2
                
                if ps_edi_obj:
                    if ps_edi_obj.passages_dfnc:
                        code_conf_valeur.create(cr, uid,{'valeur':data1, 'ligne':row,'code_id':ps_edi_obj.passages_dfnc[0].code0.id})
                        code_conf_valeur.create(cr, uid,{'valeur':data3, 'ligne':row,'code_id':ps_edi_obj.passages_dfnc[0].code2.id})
                    else:
                        break
                row +=1
            
            
        return True
    
    def edi_affectation(self, cr, uid, ids, context=None):
        affect = self.pool.get('liasse.affect.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            affect_edi_ids = affect.search(cr,uid,[],limit=1)
            affect_edi_obj = affect.browse(cr,uid,affect_edi_ids)
            if affect_edi_obj:
                code_conf.write(cr,uid,affect_edi_obj[0].code0.id,{'valeur':rec.aff_rep_n})
                code_conf.write(cr,uid,affect_edi_obj[0].code1.id,{'valeur':rec.aff_res_n_in})
                code_conf.write(cr,uid,affect_edi_obj[0].code2.id,{'valeur':rec.aff_res_n_ex})
                code_conf.write(cr,uid,affect_edi_obj[0].code3.id,{'valeur':rec.aff_prev})
                code_conf.write(cr,uid,affect_edi_obj[0].code4.id,{'valeur':rec.aff_autre_prev}) 
                code_conf.write(cr,uid,affect_edi_obj[0].code5.id,{'valeur':rec.aff_totala})
                code_conf.write(cr,uid,affect_edi_obj[0].code6.id,{'valeur':rec.aff_rl})
                code_conf.write(cr,uid,affect_edi_obj[0].code7.id,{'valeur':rec.aff_autre_r})
                code_conf.write(cr,uid,affect_edi_obj[0].code8.id,{'valeur':rec.aff_tant})
                code_conf.write(cr,uid,affect_edi_obj[0].code9.id,{'valeur':rec.aff_div})             
                code_conf.write(cr,uid,affect_edi_obj[0].code10.id,{'valeur':rec.aff_autre_aff})
                code_conf.write(cr,uid,affect_edi_obj[0].code11.id,{'valeur':rec.aff_rep_n2})
                code_conf.write(cr,uid,affect_edi_obj[0].code12.id,{'valeur':rec.aff_totalb}) 
        return True
    
    def edi_encouragement(self, cr, uid, ids, context=None):
        affect = self.pool.get('liasse.encour.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            affect_edi_ids = affect.search(cr,uid,[],limit=1)
            affect_edi_obj = affect.browse(cr,uid,affect_edi_ids)
            if affect_edi_obj:
                code_conf.write(cr,uid,affect_edi_obj[0].code0.id,{'valeur':rec.vente_imp_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code1.id,{'valeur':rec.vente_imp_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code2.id,{'valeur':rec.vente_imp_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code3.id,{'valeur':rec.vente_ex100_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code4.id,{'valeur':rec.vente_ex100_epi}) 
                code_conf.write(cr,uid,affect_edi_obj[0].code5.id,{'valeur':rec.vente_ex100_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code6.id,{'valeur':rec.vente_ex50_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code7.id,{'valeur':rec.vente_ex50_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code8.id,{'valeur':rec.vente_ex50_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code9.id,{'valeur':rec.vente_li_ep})             
                code_conf.write(cr,uid,affect_edi_obj[0].code10.id,{'valeur':rec.vente_li_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code11.id,{'valeur':rec.vente_li_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code12.id,{'valeur':rec.vente_lex100_ep})                                  
                code_conf.write(cr,uid,affect_edi_obj[0].code13.id,{'valeur':rec.vente_lex100_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code14.id,{'valeur':rec.vente_lex100_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code15.id,{'valeur':rec.vente_lex50_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code16.id,{'valeur':rec.vente_lex50_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code17.id,{'valeur':rec.vente_lex50_ept}) 
                code_conf.write(cr,uid,affect_edi_obj[0].code18.id,{'valeur':rec.pres_imp_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code19.id,{'valeur':rec.pres_imp_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code20.id,{'valeur':rec.pres_imp_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code21.id,{'valeur':rec.pres_ex100_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code22.id,{'valeur':rec.pres_ex100_epi})             
                code_conf.write(cr,uid,affect_edi_obj[0].code23.id,{'valeur':rec.pres_ex100_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code24.id,{'valeur':rec.pres_ex50_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code25.id,{'valeur':rec.pres_ex50_epi}) 
                code_conf.write(cr,uid,affect_edi_obj[0].code26.id,{'valeur':rec.pres_ex50_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code27.id,{'valeur':rec.prod_acc_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code28.id,{'valeur':rec.prod_acc_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code29.id,{'valeur':rec.prod_acc_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code30.id,{'valeur':rec.prod_sub_ep}) 
                code_conf.write(cr,uid,affect_edi_obj[0].code31.id,{'valeur':rec.prod_sub_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code32.id,{'valeur':rec.prod_sub_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code33.id,{'valeur':rec.sub_eq_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code34.id,{'valeur':rec.sub_eq_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code35.id,{'valeur':rec.sub_eq_ept})             
                code_conf.write(cr,uid,affect_edi_obj[0].code36.id,{'valeur':rec.sub_imp_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code37.id,{'valeur':rec.sub_imp_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code38.id,{'valeur':rec.sub_imp_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code39.id,{'valeur':rec.sub_ex100_ep})             
                code_conf.write(cr,uid,affect_edi_obj[0].code40.id,{'valeur':rec.sub_ex100_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code41.id,{'valeur':rec.sub_ex100_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code42.id,{'valeur':rec.sub_ex50_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code43.id,{'valeur':rec.sub_ex50_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code44.id,{'valeur':rec.sub_ex50_ept})             
                code_conf.write(cr,uid,affect_edi_obj[0].code45.id,{'valeur':rec.taux_part_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code46.id,{'valeur':rec.taux_part_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code47.id,{'valeur':rec.taux_part_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code48.id,{'valeur':rec.profit_g_ep})             
                code_conf.write(cr,uid,affect_edi_obj[0].code49.id,{'valeur':rec.profit_g_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code50.id,{'valeur':rec.profit_g_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code51.id,{'valeur':rec.profit_ex_ep}) 
                code_conf.write(cr,uid,affect_edi_obj[0].code52.id,{'valeur':rec.profit_ex_epi})             
                code_conf.write(cr,uid,affect_edi_obj[0].code53.id,{'valeur':rec.profit_ex_ept})
                code_conf.write(cr,uid,affect_edi_obj[0].code54.id,{'valeur':rec.total_g_ep})
                code_conf.write(cr,uid,affect_edi_obj[0].code55.id,{'valeur':rec.total_g_epi})
                code_conf.write(cr,uid,affect_edi_obj[0].code56.id,{'valeur':rec.total_g_ept})  
        return True
    
    def edi_immo(self, cr, uid, ids, context=None):
        affect = self.pool.get('liasse.immo.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            affect_edi_ids = affect.search(cr,uid,[],limit=1)
            affect_edi_obj = affect.browse(cr,uid,affect_edi_ids)
            if affect_edi_obj:
                code_conf.write(cr,uid,affect_edi_obj[0].code0.id,{'valeur':rec.immonv_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].code1.id,{'valeur':rec.immonv_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].code2.id,{'valeur':rec.immonv_aug_pd})
                code_conf.write(cr,uid,affect_edi_obj[0].code3.id,{'valeur':rec.immonv_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].code4.id,{'valeur':rec.immonv_dim_cess}) 
                code_conf.write(cr,uid,affect_edi_obj[0].code5.id,{'valeur':rec.immonv_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].code6.id,{'valeur':rec.immonv_dim_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].code7.id,{'valeur':rec.immonv_mbf})
                code_conf.write(cr,uid,affect_edi_obj[0].code8.id,{'valeur':rec.immoi_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].code9.id,{'valeur':rec.immoi_aug_acq})             
                code_conf.write(cr,uid,affect_edi_obj[0].code10.id,{'valeur':rec.immoi_aug_pd})
                code_conf.write(cr,uid,affect_edi_obj[0].code11.id,{'valeur':rec.immoi_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].code12.id,{'valeur':rec.immoi_dim_cess})                                  
                code_conf.write(cr,uid,affect_edi_obj[0].code13.id,{'valeur':rec.immoi_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].code14.id,{'valeur':rec.immoi_dim_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].code15.id,{'valeur':rec.immoi_mbf})
                code_conf.write(cr,uid,affect_edi_obj[0].code16.id,{'valeur':rec.immonc_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].code17.id,{'valeur':rec.immonc_aug_acq}) 
                code_conf.write(cr,uid,affect_edi_obj[0].code18.id,{'valeur':rec.immonc_aug_pd})
                code_conf.write(cr,uid,affect_edi_obj[0].code19.id,{'valeur':rec.immonc_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].code20.id,{'valeur':rec.immonc_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].code21.id,{'valeur':rec.immonc_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].code22.id,{'valeur':rec.immonc_dim_vir})             
                code_conf.write(cr,uid,affect_edi_obj[0].code23.id,{'valeur':rec.immonc_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].fp_mb.id,{'valeur':rec.fp_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].fp_aug_acq.id,{'valeur':rec.fp_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].fp_aug_vir.id,{'valeur':rec.fp_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].fp_dim_cess.id,{'valeur':rec.fp_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].fp_dim_ret.id,{'valeur':rec.fp_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].fp_dim_vir.id,{'valeur':rec.fp_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].fp_mbf.id,{'valeur':rec.fp_mbf})

                code_conf.write(cr,uid,affect_edi_obj[0].charge_mb.id,{'valeur':rec.charge_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].charge_aug_acq.id,{'valeur':rec.charge_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].charge_aug_vir.id,{'valeur':rec.charge_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].charge_dim_cess.id,{'valeur':rec.charge_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].charge_dim_ret.id,{'valeur':rec.charge_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].charge_dim_vir.id,{'valeur':rec.charge_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].charge_mbf.id,{'valeur':rec.charge_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].prime_mb.id,{'valeur':rec.prime_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].prime_aug_acq.id,{'valeur':rec.prime_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].prime_aug_vir.id,{'valeur':rec.prime_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].prime_dim_cess.id,{'valeur':rec.prime_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].prime_dim_ret.id,{'valeur':rec.prime_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].prime_dim_vir.id,{'valeur':rec.prime_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].prime_mbf.id,{'valeur':rec.prime_mbf}) 
                
                code_conf.write(cr,uid,affect_edi_obj[0].immord_mb.id,{'valeur':rec.immord_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].immord_aug_acq.id,{'valeur':rec.immord_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].immord_aug_vir.id,{'valeur':rec.immord_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].immord_dim_cess.id,{'valeur':rec.immord_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].immord_dim_ret.id,{'valeur':rec.immord_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].immord_dim_vir.id,{'valeur':rec.immord_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].immord_mbf.id,{'valeur':rec.immord_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].brevet_mb.id,{'valeur':rec.brevet_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].brevet_aug_acq.id,{'valeur':rec.brevet_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].brevet_aug_vir.id,{'valeur':rec.brevet_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].brevet_dim_cess.id,{'valeur':rec.brevet_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].brevet_dim_ret.id,{'valeur':rec.brevet_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].brevet_dim_vir.id,{'valeur':rec.brevet_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].brevet_mbf.id,{'valeur':rec.brevet_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].fond_mb.id,{'valeur':rec.fond_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].fond_aug_acq.id,{'valeur':rec.fond_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].fond_aug_vir.id,{'valeur':rec.fond_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].fond_dim_cess.id,{'valeur':rec.fond_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].fond_dim_ret.id,{'valeur':rec.fond_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].fond_dim_vir.id,{'valeur':rec.fond_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].fond_mbf.id,{'valeur':rec.fond_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].autre_incorp_mb.id,{'valeur':rec.autre_incorp_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_incorp_aug_acq.id,{'valeur':rec.autre_incorp_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_incorp_aug_vir.id,{'valeur':rec.autre_incorp_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_incorp_dim_cess.id,{'valeur':rec.autre_incorp_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_incorp_dim_ret.id,{'valeur':rec.autre_incorp_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_incorp_dim_vir.id,{'valeur':rec.autre_incorp_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].autre_incorp_mbf.id,{'valeur':rec.autre_incorp_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].terrain_mb.id,{'valeur':rec.terrain_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].terrain_aug_acq.id,{'valeur':rec.terrain_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].terrain_aug_vir.id,{'valeur':rec.terrain_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].terrain_dim_cess.id,{'valeur':rec.terrain_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].terrain_dim_ret.id,{'valeur':rec.terrain_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].terrain_dim_vir.id,{'valeur':rec.terrain_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].terrain_mbf.id,{'valeur':rec.terrain_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].constructions_mb.id,{'valeur':rec.constructions_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].constructions_aug_acq.id,{'valeur':rec.constructions_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].constructions_aug_vir.id,{'valeur':rec.constructions_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].constructions_dim_cess.id,{'valeur':rec.constructions_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].constructions_dim_ret.id,{'valeur':rec.constructions_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].constructions_dim_vir.id,{'valeur':rec.constructions_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].constructions_mbf.id,{'valeur':rec.constructions_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].inst_mb.id,{'valeur':rec.inst_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].inst_aug_acq.id,{'valeur':rec.inst_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].inst_aug_vir.id,{'valeur':rec.inst_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].inst_dim_cess.id,{'valeur':rec.inst_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].inst_dim_ret.id,{'valeur':rec.inst_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].inst_dim_vir.id,{'valeur':rec.inst_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].inst_mbf.id,{'valeur':rec.inst_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].mat_mb.id,{'valeur':rec.mat_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].mat_aug_acq.id,{'valeur':rec.mat_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].mat_aug_vir.id,{'valeur':rec.mat_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].mat_dim_cess.id,{'valeur':rec.mat_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].mat_dim_ret.id,{'valeur':rec.mat_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].mat_dim_vir.id,{'valeur':rec.mat_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].mat_mbf.id,{'valeur':rec.mat_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].mob_mb.id,{'valeur':rec.mob_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].mob_aug_acq.id,{'valeur':rec.mob_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].mob_aug_vir.id,{'valeur':rec.mob_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].mob_dim_cess.id,{'valeur':rec.mob_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].mob_dim_ret.id,{'valeur':rec.mob_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].mob_dim_vir.id,{'valeur':rec.mob_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].mob_mbf.id,{'valeur':rec.mob_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].autre_corp_mb.id,{'valeur':rec.autre_corp_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_corp_aug_acq.id,{'valeur':rec.autre_corp_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_corp_aug_vir.id,{'valeur':rec.autre_corp_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_corp_dim_cess.id,{'valeur':rec.autre_corp_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_corp_dim_ret.id,{'valeur':rec.autre_corp_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].autre_corp_dim_vir.id,{'valeur':rec.autre_corp_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].autre_corp_mbf.id,{'valeur':rec.autre_corp_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].immocc_mb.id,{'valeur':rec.immocc_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].immocc_aug_acq.id,{'valeur':rec.immocc_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].immocc_aug_vir.id,{'valeur':rec.immocc_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].immocc_dim_cess.id,{'valeur':rec.immocc_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].immocc_dim_ret.id,{'valeur':rec.immocc_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].immocc_dim_vir.id,{'valeur':rec.immocc_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].immocc_mbf.id,{'valeur':rec.immocc_mbf})
                
                code_conf.write(cr,uid,affect_edi_obj[0].mati_mb.id,{'valeur':rec.mati_mb})
                code_conf.write(cr,uid,affect_edi_obj[0].mati_aug_acq.id,{'valeur':rec.mati_aug_acq})
                code_conf.write(cr,uid,affect_edi_obj[0].mati_aug_vir.id,{'valeur':rec.mati_aug_vir})
                code_conf.write(cr,uid,affect_edi_obj[0].mati_dim_cess.id,{'valeur':rec.mati_dim_cess})
                code_conf.write(cr,uid,affect_edi_obj[0].mati_dim_ret.id,{'valeur':rec.mati_dim_ret})
                code_conf.write(cr,uid,affect_edi_obj[0].mati_dim_vir.id,{'valeur':rec.mati_dim_vir}) 
                code_conf.write(cr,uid,affect_edi_obj[0].mati_mbf.id,{'valeur':rec.mati_mbf})
        return True
    
    def edi_tva(self, cr, uid, ids, context=None):
        affect = self.pool.get('liasse.tva.erp')
        code_conf = self.pool.get('liasse.code.erp')
        for rec in self.browse(cr,uid,ids):
            affect_edi_ids = affect.search(cr,uid,[],limit=1)
            affect_edi_obj = affect.browse(cr,uid,affect_edi_ids)
            if affect_edi_obj:
                code_conf.write(cr,uid,affect_edi_obj[0].tva_facsd.id,{'valeur':rec.tva_facsd})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_faco.id,{'valeur':rec.tva_faco})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_facd.id,{'valeur':rec.tva_facd})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_facsf.id,{'valeur':rec.tva_facsf})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_recsd.id,{'valeur':rec.tva_recsd}) 
                code_conf.write(cr,uid,affect_edi_obj[0].tva_reco.id,{'valeur':rec.tva_reco})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_recd.id,{'valeur':rec.tva_recd})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_recsf.id,{'valeur':rec.tva_recsf})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_charsd.id,{'valeur':rec.tva_charsd})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_charo.id,{'valeur':rec.tva_charo})             
                code_conf.write(cr,uid,affect_edi_obj[0].tva_chard.id,{'valeur':rec.tva_chard})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_charsf.id,{'valeur':rec.tva_charsf})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_immosd.id,{'valeur':rec.tva_immosd})                                  
                code_conf.write(cr,uid,affect_edi_obj[0].tva_immoo.id,{'valeur':rec.tva_immoo})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_immod.id,{'valeur':rec.tva_immod})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_immosf.id,{'valeur':rec.tva_immosf})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_totalsd.id,{'valeur':rec.tva_totalsd})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_totalo.id,{'valeur':rec.tva_totalo}) 
                code_conf.write(cr,uid,affect_edi_obj[0].tva_totald.id,{'valeur':rec.tva_totald})
                code_conf.write(cr,uid,affect_edi_obj[0].tva_totalsf.id,{'valeur':rec.tva_totalsf})
        return True
    
    def report_nouveau(self, cr, uid, ids, ex_prec , context=None):
        
        if ex_prec:
            start_period, stop_period, start, stop = \
            self._get_start_stop_for_filter('filter_no', ex_prec, None, None, None, None,cr,uid)
            initial_balance_mode = self._get_initial_balance_mode(cr,uid,start_period)
        
        #actiiiiif#        
        bilan_actif = self.pool.get('liasse.bilan.actif.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        code_conf = self.pool.get('liasse.code.erp')
        if ex_prec:
            for code in bilan_actif_obj:
                total1= self.count_cell(cr, uid, ids, code.code1,ex_prec,start_period, stop_period, initial_balance_mode)
                total2= self.count_cell(cr, uid, ids, code.code2,ex_prec,start_period, stop_period, initial_balance_mode)
                total3 = total1 - total2
                code_conf.write(cr,uid,code.code3.id,{'valeur':total3})
        else:
            for code in bilan_actif_obj:
                code_conf.write(cr,uid,code.code3.id,{'valeur':'0'})
            
        # passif##########################################
        bilan_passif = self.pool.get('liasse.bilan.passif.erp')
        bilan_passif_ids = bilan_passif.search(cr,uid,[],order='sequence')
        bilan_passif_obj = bilan_passif.browse(cr,uid,bilan_passif_ids)
        code_conf = self.pool.get('liasse.code.erp')
        if ex_prec:
            for code in bilan_passif_obj:
                total1= self.count_cell(cr, uid, ids, code.code1,ex_prec,start_period, stop_period, initial_balance_mode)
        else:
            for code in bilan_passif_obj:
                code_conf.write(cr,uid,code.code1.id,{'valeur':'0'})
        # cpc##########################
        bilan_actif = self.pool.get('liasse.cpc.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        code_conf = self.pool.get('liasse.code.erp')
        if ex_prec:
            for code in bilan_actif_obj:
                total1 = self.count_cell(cr, uid, ids, code.code1,ex_prec,start_period, stop_period, initial_balance_mode)
                total2 = self.count_cell(cr, uid, ids, code.code2,ex_prec,start_period, stop_period, initial_balance_mode)                    
                total3 = total1 + total2
                code_conf.write(cr,uid,code.code3.id,{'valeur':total3})
        else:
            for code in bilan_actif_obj:
                code_conf.write(cr,uid,code.code3.id,{'valeur':'0'})
                
        #### det cpc #################################
        bilan_actif = self.pool.get('liasse.det.cpc.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        if ex_prec:
            for code in bilan_actif_obj:
                total1= self.count_cell(cr, uid, ids, code.code1,ex_prec,start_period, stop_period, initial_balance_mode)
        else:
            for code in bilan_actif_obj:
                code_conf.write(cr,uid,code.code1.id,{'valeur':'0'}) 
        ### tfr################
        bilan_actif = self.pool.get('liasse.tfr.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        if ex_prec:
            self.edi_affectation(cr, uid, ids, context)
            for code in bilan_actif_obj:
                total1= self.count_cell(cr, uid, ids, code.code1,ex_prec,start_period, stop_period, initial_balance_mode)
        else:
            for code in bilan_actif_obj:
                code_conf.write(cr,uid,code.code1.id,{'valeur':'0'}) 
        # caf ##############################
        bilan_actif = self.pool.get('liasse.caf.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        if ex_prec:
            for code in bilan_actif_obj:
                total1 = self.count_cell(cr, uid, ids, code.code1,ex_prec,start_period, stop_period, initial_balance_mode)
        else:
            for code in bilan_actif_obj:
                code_conf.write(cr,uid,code.code1.id,{'valeur':'0'}) 
        ####### stock #####
        
        bilan_actif = self.pool.get('liasse.stock.erp')
        bilan_actif_ids = bilan_actif.search(cr,uid,[],order='sequence')
        bilan_actif_obj = bilan_actif.browse(cr,uid,bilan_actif_ids)
        code_conf = self.pool.get('liasse.code.erp')
        if ex_prec:
            for code in bilan_actif_obj:
                total1=self.count_cell(cr, uid, ids, code.code1,ex_prec,start_period, stop_period, initial_balance_mode)
                total2= self.count_cell(cr, uid, ids, code.code2,ex_prec,start_period, stop_period, initial_balance_mode)
                                    
                total3 = total1 + total2
                code_conf.write(cr,uid,code.code3.id,{'valeur':total3})
        else:
            for code in bilan_actif_obj:
                code_conf.write(cr,uid,code.code3.id,{'valeur':'0'})
                code_conf.write(cr,uid,code.code1.id,{'valeur':'0'})
                code_conf.write(cr,uid,code.code2.id,{'valeur':'0'})
        return True
    
    def count_cell(self, cr, uid, ids, code,fy,start, stop, initial_balance_mode,context=None):
        res = 0
        code_conf = self.pool.get('liasse.code.erp')
        balance = self.pool.get('account.account')
        liasse_conf = self.pool.get('liasse.configuration.erp')
        compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',code.id)])
        if compte_conf_ids:
            compte_conf = liasse_conf.browse(cr, uid, compte_conf_ids)
        #compte_conf_obj = liasse_conf.browse(cr, uid, compte_conf_ids)
            if compte_conf.type in ['1']:
                for rec in compte_conf.code_ids:
                    compte_conf_somme_ids = liasse_conf.search(cr, uid, [('code','=',rec.id)])
                    compte_conf_somme = liasse_conf.browse(cr, uid, compte_conf_somme_ids)
                    #for compte_conf_somme in compte_conf_somme_obj:
                    for compte_id in compte_conf_somme.compte:
                        signe = self.check_account_signe(compte_id.compte)
                        compte_bal_somme_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+'%'),('type','not in',['view'])])
                        for acc_id in compte_bal_somme_ids:
                            net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('balance',0)
                            res += net*signe
                    for compte_id in compte_conf_somme.comptem:
                        signe = self.check_account_signe(compte_id.compte)
                        compte_bal_somme_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+'%'),('type','not in',['view'])])
                        for acc_id in compte_bal_somme_ids:
                            net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('balance',0)
                            res -= net*signe
            elif compte_conf.type in ['2']:
                for rec in compte_conf.code_ids:
                    res += float(rec.valeur)
                for rec in compte_conf.code_min_ids:
                    res -= float(rec.valeur)
            elif compte_conf.type in ['3']:
                for rec in compte_conf.code_ids:
                    if float(rec.valeur)>0:
                        res += float(rec.valeur)
            elif compte_conf.type in ['4']:
                for rec in compte_conf.code_ids:
                    if float(rec.valeur)<0:
                        res += abs(float(rec.valeur))
            else:
                for compte_id in compte_conf.compte:
                    signe = self.check_account_signe(compte_id.compte)
                    compte_bal_somme_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+'%'),('type','not in',['view'])])
                    for acc_id in compte_bal_somme_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('balance',0)
                        res += net*signe
                for compte_id in compte_conf.comptem:
                    signe = self.check_account_signe(compte_id.compte)
                    compte_bal_somme_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+'%'),('type','not in',['view'])])
                    for acc_id in compte_bal_somme_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('balance',0)
                        res -= net*signe
        code_conf.write(cr,uid,code.id,{'valeur':res})
        return res
        
    def count_init(self, cr, uid, ids, code, context=None):
        fy = False
        for rec in self.browse(cr,uid,ids,context=context):
            fy = rec.name
        start_period, stop_period, start, stop = \
            self._get_start_stop_for_filter('filter_no', fy, None, None, None, None,cr,uid)
        initial_balance_mode = self._get_initial_balance_mode(cr,uid,start)
        res = 0
        code_conf = self.pool.get('liasse.code.erp')
        balance = self.pool.get('account.account')
        liasse_conf = self.pool.get('liasse.configuration.erp')
        compte_conf_ids = liasse_conf.search(cr, uid, [('code','=',code.id)])
        if compte_conf_ids:
            compte_conf = liasse_conf.browse(cr, uid, compte_conf_ids)
            if compte_conf.type in ['1']:
                for rec in compte_conf.code_ids:
                    compte_conf_somme_ids = liasse_conf.search(cr, uid, [('code','=',rec.id)])
                    compte_conf_somme = liasse_conf.browse(cr, uid, compte_conf_somme_ids)
                    for compte_id in compte_conf_somme.compte:
                        compte_bal_somme_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+'%'),('type','not in',['view'])])
                        for acc_id in compte_bal_somme_ids:
                            net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('init_balance',0)
                            res += abs(net)
                    for compte_id in compte_conf_somme.comptem:
                        compte_bal_somme_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+'%'),('type','not in',['view'])])
                        for acc_id in compte_bal_somme_ids:
                            net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('init_balance',0)
                            res -= abs(net)
            elif compte_conf.type in ['2']:
                for rec in compte_conf.code_ids:
                    res += float(rec.valeur)
                for rec in compte_conf.code_min_ids:
                    res -= float(rec.valeur)
            else:
                for compte_id in compte_conf.compte:
                    compte_bal_somme_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+'%'),('type','not in',['view'])])
                    for acc_id in compte_bal_somme_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('init_balance',0)
                        res += abs(net)
                for compte_id in compte_conf.comptem:
                    compte_bal_somme_ids = balance.search(cr, uid, [('code','=like',compte_id.compte+'%'),('type','not in',['view'])])
                    for acc_id in compte_bal_somme_ids:
                        net =self._get_account_details(cr,uid,[acc_id], 'all', fy, 'filter_no', start, stop, initial_balance_mode)[acc_id].get('init_balance',0)
                        res -= abs(net)
        code_conf.write(cr,uid,code.id,{'valeur':res})
        return res
    """
    def generate_xml(self,cr,uid,ids,context=None):
        doc = etree.Element( "openerp", nsmap={})
        data = etree.SubElement( doc, "data",noupdate="1" )
        data.append(etree.Comment("Compte"))
            
        actif_ids = self.pool.get("liasse.configuration.erp").search(cr,uid,[])
        actif_obj = self.pool.get("liasse.configuration.erp").browse(cr,uid,actif_ids)
        for line in actif_obj:
            record = etree.SubElement( data, "record",model="liasse.configuration",id='code_'+str(line.code.code) )
            field1 = etree.SubElement( record, "field",name="code",ref='code_'+str(line.code.code) )
            field1 = etree.SubElement( record, "field",name="type")
            field1.text= str(line.type)
            if line.code_ids:
                cd = []
                for td in line.code_ids:
                    cd.append('code_'+td.code)
                field1 = etree.SubElement( record, "field",name="code_ids", eval="[(6, 0, "+str(cd).replace("'", '')+")]" )
            if line.code_min_ids:
                cd = []
                for td in line.code_min_ids:
                    cd.append('code_'+td.code)
                field1 = etree.SubElement( record, "field",name="code_min_ids", eval="[(6, 0, "+str(cd).replace("'", '')+")]" )
        
        xml_data = "%s" % (
                etree.tostring(doc, pretty_print = True, xml_declaration = True, encoding='UTF-8')
                )
        self.write(cr, uid, ids, {
            'output2': base64.b64encode(xml_data),}, context=context)
        
        return True 
    """
    def check_account_signe(self,account):
        signe = 1
        if account.startswith('4') or account.startswith('1') or account.startswith('55') or account.startswith('59') or\
         account.startswith('7') or account.startswith('28') or account.startswith('29') or account.startswith('39'):
            signe = -1
        return signe
################### added #################

