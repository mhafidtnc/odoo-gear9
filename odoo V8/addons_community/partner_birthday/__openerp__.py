# -*- coding: utf-8 -*-
{
    "name": "Birthday Reminder",
    "version": "8.0.1.0.3",
    "category": "Sales",
    "author": "Odoo Tools",
    "website": "https://odootools.com/apps/8.0/birthday-reminder-201",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "auto_install": False,
    "depends": [
        "base",
        "mail"
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/res_partner_view.xml",
        "data/cron.xml"
    ],
    "qweb": [
        
    ],
    "js": [
        
    ],
    "demo": [
        
    ],
    "summary": "Keep your managers informed of the nearest birthdays",
    "description": """
    The app goal is to track your customers / suppliers (so any partners) birthdays
<ul style="font-size:18px">
    <li>
    State the birthday right on a partner form
    </li>
    <li>
    All the partners birthdays of this month are summed up in a single email
    </li>
    <li>
    Configure whom to notify about birthdays on a company form: no more harmful forgetting
    </li>
</ul>

    Group partners by birthday month and day. And it is not a standard grouping where "June 2016" is different from "June 2015". June is always June!
    The time of each email dispatch may be changed in Odoo scheduled actions (Settings &gt; Technical &gt; Automation &gt; Scheduled Actions &gt; "Birthday Notifications"). By default it is the first day of each month
    Birthdays reminder
    Birthday field
    Group by birthday month without year
    Configure birthday recipients
""",
    "images": [
        "static/description/main.png"
    ],
    "price": "0.0",
    "currency": "EUR",
}