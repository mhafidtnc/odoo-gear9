#coding: utf-8
from openerp import models, fields, api, _
from openerp import SUPERUSER_ID
from datetime import datetime, timedelta, date

class res_company(models.Model):
    _inherit = 'res.company'

    birth_users = fields.Many2many('res.users','rel_s_table','rel_s_1','rel_s_2',string='Notify birthday to')

class res_partner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    not_id = fields.Many2one('notification.birthday',string='Notification')


class notification_birthday(models.Model):
    _name = "notification.birthday"
    _inherit = 'mail.thread'

    name = fields.Char(string='Name',default=_('Reminder'))

    @api.model
    def _send_birthday_list(self,lead_notify=False,partner_ids=False,templ_id=False,subj=_('Birthdays Reminder')):
        res = True

        if templ_id and lead_notify:
            local_context = self.env.context.copy()
            company = self.env['res.users'].browse(SUPERUSER_ID).company_id.name
            local_context.update({
                'dbname': self.env.cr.dbname,
                'base_url': self.env['ir.config_parameter'].get_param('web.base.url', default='http://localhost:8069', context=self.env.context),
                'datetime' : datetime,
                'company' : company,
                'only_user_sign' : True,
                'partner_ids_s':partner_ids,
            })

            temp_obj = self.env['email.template']
            template_id = self.env['ir.model.data'].get_object('partner_birthday', templ_id)
            for notify in lead_notify:
                for user in self.env['res.users'].browse(SUPERUSER_ID).company_id.birth_users:
                    lang = 'en_EN'
                    if user.lang:
                        lang = user.lang
                    local_context.update({'lang':lang,})
                    template_id = temp_obj.with_context(local_context).browse(template_id.id)
                    body_html = temp_obj.with_context(local_context).render_template(template_id.body_html, 'notification.birthday', notify.id)             

                    res = notify.with_context(local_context).message_post(
                        body=body_html,
                        subject=subj,
                        subtype='partner_birthday.mt_birth_notify',
                        partner_ids=[user.partner_id.id],)
        return res 

    
    @api.model    
    def cron_notify(self):
        m_dict = {'01': _('January'), '02': _('February'), '03': _('March'), '04': _('April'), '05': _('May'), '06': _('June'), '07': _('July'), '08': _('August'), '09': _('September'), '10': _('October'), '11': _('November'), '12': _('December')}
        today_date = datetime.now().date().strftime('%Y-%m-%d %H:%M:%S')
        birthday_data = today_date.split('-')
        partner_birthday_month = m_dict[birthday_data[1]]

        partner_ids = self.env['res.partner'].search([('partner_birthday_month','=',partner_birthday_month)],order='partner_birthday')
        if partner_ids:
            notify_id = self.create({})
            if notify_id:           
                self._send_birthday_list(lead_notify=[notify_id],partner_ids=partner_ids,templ_id='birthday_notification',subj=_('Birthday Notifications'))
                notify_id.unlink()
       