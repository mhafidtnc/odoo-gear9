# -*- coding: utf-8 -*-


from openerp import tools, models, fields, api, _

class res_partner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    @api.one
    def _set_new_birthday_data(self):
        if self.partner_birthday:
            months = {'01': _('January'), '02': _('February'), '03': _('March'), '04': _('April'), '05': _('May'), '06': _('June'), '07': _('July'), '08': _('August'), '09': _('September'), '10': _('October'), '11': _('November'), '12': _('December')}
            birthday_data = (self.partner_birthday).split('-')
            self.partner_birthday_month = months[birthday_data[1]]
            self.partner_birthday_day = birthday_data[2] + ' ' + self.partner_birthday_month


    partner_birthday = fields.Date(string="Birthday", inverse="_set_new_birthday_data")
    partner_birthday_day = fields.Char(string="Birthday day")
    partner_birthday_month = fields.Char(string="Birthday month")